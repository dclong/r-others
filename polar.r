polariod=function(x)
{#This function changes an arbitrary p-dimensional vector x into its polar representation
#Parameter x is the p-dimensional vector
#Because of the change between the two kinds of representions is not one-to-one, there is some
#constraint for x. Generally speaking, x should have no partial value which is 0.
################################################################################################
#calculate the dimension of the space
SpaceDim=length(x)
#build up the polar vector
polar=rep(0,SpaceDim)
#calculate the length of the vector
R=sqrt(sum(x^2))
polar[1]=R
#initial Rprojection1
Rprojection1=x[SpaceDim]
#use loop to calculate theta(i)
for(i in (SpaceDim-1):2)
{
   #calculate the value of Rprojection2
   Rprojection2=sqrt(Rprojection1^2+x[i]^2)*sign(Rprojection1)
   #find the value of sin and cos
   SinValue=Rprojection1/Rprojection2
   CosValue=x[i]/Rprojection2
   #use function asincos to find the value of theta
   polar[i+1]=asincos(SinValue,CosValue)
   #give new value to Rprojection1
   Rprojection1=Rprojection2
}
#calculate the value for theta(1)
Sinvalue=Rprojection1/R
CosValue=x[1]/R
polar[2]=asincos(SinValue,CosValue)
#return value
return(polar)
}
#define the function asincos(SinValue,CosValue)
asincos=function(SinValue,CosValue)
{#We all know that triangle functions are not monotone, so sometimes we can not
#just use the value of sin or cos function to find the value of theta.
#This function use both the value of sin and cos functions to find the value of theta.
#The return value is between 0 and 2*pi
#Parameters Sinvalue and CosValue are the value of sin and cos functions respectivley
ifelse(SinValue>=0,return(acos(CosValue)),return(2*pi-acos(CosValue)))
}
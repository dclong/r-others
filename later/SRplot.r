SRplot=function(x,y)
{#This function is used to construct the plot of studentized residual against yhat
#Parameter x is the design matrix
#Parameter y is the response vector
x=as.matrix(x)
n=nrow(x)
p=qr(x)$rank
library(MASS)
H=x%*%ginv(t(x)%*%x)%*%t(x)
yhat=H%*%y
res=y-yhat
SSE=sum(res^2)
SigSqHat=SSE/(n-p)
Studentized=res/(sqrt(SigSqHat)*sqrt(1-diag(H)))
plot(yhat,Studentized)
}
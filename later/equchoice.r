sp<-function(n)
{#用于产生Sp序列
	sequence<-c(1)
	count<-1
	while(count<n)
	{
		count<-count+1
		sequence<-c(sequence,count,-rev(sequence))	
	}
	return(sequence)
}

expunction<-function(x,flag)
{#对给定的矩阵进行消去变换T(flag)X
	if(!is.matrix(x))
		return("Wrong input! A matrix is needed!")
	n<-ncol(x)
	m<-nrow(x)
	if(flag>min(m,n))
		return("Wrong input! The integer entered is too big!")
	temp<-x[flag,flag]
	for(i in (1:m)[-flag])
		for(j in (1:n)[-flag])
			x[i,j]<-x[i,j]-x[flag,j]*x[i,flag]/temp
	x[flag,]<-x[flag,]/temp
	x[,flag]<--x[,flag]/temp
	x[flag,flag]<--x[flag,flag]
	return(x)
}

simplify<-function(x,flag)
{#用于从Sp序列的部分序列中找出对应的变量
	#只与Sp同用
	if(flag>0)
		return(sort(append(x,flag)))
	return(sort(x[!(x==-flag)]))
}



equchoice<-function(x,y)
{#给出在RMSq，Cp和AIC准则下的详细信息以便于对方程进行选取
      x1=x
	n<-nrow(x)
      x=cbind(rep(1,n),x)
	p<-ncol(x)
	temp<-rep(1,n)%*%x1
	B<-rbind(c(n,temp),cbind(t(temp),t(x1)%*%x1))
	temp<-t(x)%*%y
	A<-rbind(cbind(B,temp),c(t(temp),crossprod(y,y)))
	A<-expunction(A,1)
	sequence<-sp(p-1)
	count<-length(sequence)
	vartable<-c(0)
	RSS<-rss(x,y)
	d<-list(parameter=list(list(variable=c(0),estimation=c(A[1,p+1]))),RSSq=c(A[p+1,p+1]),RMSq.order=NULL,RMSq=c(A[p+1,p+1]/(n-1)),Cp.order=NULL,
	Cp=c(A[p+1,p+1]*(n-p)/RSS-n+2),slopeCp=c(A[p+1,p+1]*(n-p)/RSS-n+2),AIC.order=NULL,AIC=n*log(A[p+1,p+1])+2)
	for(i in 1:count)
	{
		vartable<-simplify(vartable,sequence[i])
		A<-expunction(A,abs(sequence[i])+1)
		d$parameter<-c(d$parameter,list(list(variable=vartable,estimation=A[vartable+1,p+1])))
		d$RSSq<-c(d$RSSq,A[p+1,p+1])
		q<-length(vartable)
		d$RMSq<-c(d$RMSq,A[p+1,p+1]/(n-q))
		d$Cp<-c(d$Cp,A[p+1,p+1]*(n-p)/RSS-n+2*q)
		d$slopeCp<-c(d$slopeCp,(A[p+1,p+1]*(n-p)/RSS-n+2*q)/q)
		d$AIC<-c(d$AIC,n*log(A[p+1,p+1])+2*q)
	}
	d$RMSq.order<-order(d$RMSq)
	d$RMSq<-sort(d$RMSq)
	d$Cp.order<-order(d$Cp)
	d$Cp<-sort(d$Cp)
	d$slopeCp<-d$slopeCp[d$Cp.order]
	d$AIC.order<-order(d$AIC)
	d$AIC<-sort(d$AIC)
	return(d)
}

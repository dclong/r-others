


ridge.trace<-function(x,y,kmin=0,kmax=1,PointNum=1000)
{#作岭迹图，建立岭回归方程
	if(kmin<0)
		return("Wrong input for parameter kmin!")
	if(kmax<=kmin)
		return("Parameter kmax is not biger than kmin!")
	judge.response(y)
	judge.integer.positive(PointNum)
	RSS1<-rss(x,y)
	sys<-censtandardize(x)
	x<-sys$matrix
	temp<-t(x)%*%x
	seqx<-seq(from=kmin,to=kmax,length=PointNum)
	m<-nrow(temp)
	seqy<-matrix(0,ncol=m,nrow=PointNum)
	for(i in 1:PointNum)
		seqy[i,]<-ginverse(temp+seqx[i]*diag(m))%*%t(x)%*%y
	graphsheet()
	#在此作图
	matplot(seqx,seqy,lty=2:(m+1),col=2:(m+1))
	cat("      If you want to this program to deal with the data please enter the value for the ridge parameter which you figure out through the graph. If you want to deal with the data yourself please enter a value less than 0.\n")
	value<-"N"
	while(value=="N"||value=="n")
	{
		cat("Please enter the your value:\n")
		value<-scan(what=numeric(),n=1)
		if(value<0)
	   {
		     cat("Are you sure that you want to deal with the data yourself?(Y/N)\n")
		     value<-scan(what=character(),n=1)
	   }
	   else
	   {
		     estimation<-ginverse(temp+value*diag(m))%*%t(x)%*%y
		     temp1<-mean(y)
		     RSS2<-sum((y-temp1-x%*%estimation)^2)
		     temp2<-estimation/sys$variance
		     return(list(estimation.censtandard=c(temp1,estimation),estimation.original=c(temp1-crossprod(sys$average,temp2),temp2)))
		#c(RSS1,RSS2,RSS2/RSS1)为什么RSS2会比RSS1小？哪里出错了？
	   }
	}
}


ridge.estimation<-function(x,y,kvalue)
{#给定岭参数，计算岭估计
	judge.response(y)
	if(kvalue<0)
		return("Wrong input! A positive value is needed!")
	sys<-censtandardize(x)
	x<-sys$matrix
	temp<-t(x)%*%x
	m<-nrow(temp)
	estimation<-ginverse(temp+kvalue*diag(m))%*%t(x)%*%y
	temp1<-mean(y)
	temp2<-estimation/sys$variance
	return(list(estimation.censtandard=c(temp1,estimation),estimation.original=c(temp1-crossprod(sys$average,temp2),temp2)))
}
pointXY=function(distance=1,dimen=2,n=100000)
{#This function is used to simulate the probability that the distance between two
#points in a "dimen" dimension hypercube less than "distance".
#Parameter "distance" is the distance we mentioned above and has a default value 1
#Parameter "dimen" is the dimension of the hypercube and has a default value 2
#Parameter "n" is the sample size we used to simulate the result and has default value 100000
pointX=matrix(runif(n*dimen),ncol=dimen)
pointY=matrix(runif(n*dimen),ncol=dimen)
mean(apply((pointX-pointY)^2,1,sum)<distance)
}
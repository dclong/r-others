ConInt=function(estimates,CovMat,alpha=0.05)
{#calculate the seperate confidence intervals of the parameters
#Parameter estimate is the vector of estimated parameters
#Parameter CovMat is the covariance matrix of the estimations
#Parameter alpha is the significant level, which has the default value 0.05
#-----------------------------------------------------------------
n=length(estimates)
if(nrow(CovMat)!=n)
   stop("parameters conflict!")
if(ncol(CovMat)!=n)
   stop("parameters conflict!")
#alpha=repeat(alpha,n/length(alpha))
sigmas=sqrt(diag(CovMat))
Width=qnorm(alpha/2,lower.tail=F)*sigmas
UpperLimit=estimates+Width
LowerLimit=estimates-Width
result=cbind(estimates,sigmas,LowerLimit,UpperLimit)
colnames(result)=c("  Estimate","  Std","  LowerLimit","  UpperLimit")
return(result)
}

LLHRatioTest=function(LLHo,LLHa,DimDiff)
{#calculate the log likelihood ratio test
#LLHo: the maximum log likelihood under null hypothesis (reduced model)
#LLHa: the maximum log likelihood under the alternative hypothesis (full model)
#DimDiff: the difference of the dimensions of the two models
#-------------------------------------------------------------------------------
ChisqStat=2*(LLHo-LLHa)
Df=DimDiff
Pvalue=pchisq(ChisqStat,Df,lower.tail=F)
result=c(LLHo,LLHa,ChisqStat,Df,Pvalue)
result=matrix(result,nrow=1)
colnames(result)=c("LLHo","LLHa","ChisqStat","Df","Pvalue")
return(result)
}
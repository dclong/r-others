OrthogonalPolynomial=function(trt,size)
{#This function is used to obtain orthogonal polynomial contrasts
#Parameter trt is a vector containing the levels of the treatment
#Parameter size is a vector containing the number of observations for each level of treatment
#################################################################################################
Ntrt=length(trt)
Nobs=sum(size)
X=matrix(0,nrow=Ntrt,ncol=Nobs)
X[1,]=rep(1,Nobs)
x1=rep(trt,size)
for(i in 2:Ntrt)
{
   X[i,]=x1^(i-1)
   for(j in 1:(i-1))
      X[i,]=X[i,]-X[i,]%*%X[j,]/X[j,]%*%X[j,]*X[j,]
}
result=NULL
flag=c(1,size[-Ntrt])
flag=cumsum(flag)
result=X[2:Ntrt,flag]
result=t(result)*size
return(result)
}


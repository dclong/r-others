cook=function(x,y)
{#This function is used to calculate Cook statistics
#x is design matrix, y is dependent variable
#x can be either complete or incomplete
   x=as.matrix(x)
   n=nrow(x)
   x=cbind(rep(1,n),x)
   p=ncol(x)
   H=x%*%solve(t(x)%*%x)%*%t(x)
   h=diag(H)
   yhat=H%*%y
   ehat=y-yhat
   RSS=sum(ehat^2)
   sigmahat=sqrt(RSS/(n-p))
   r=ehat/(sigmahat*sqrt(1-h))
   cook=h/(1-h)*r^2/p
   return(cbind(ehat,h,r,cook))
}

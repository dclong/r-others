centralize<-function(x,flag= 2)
{#将设计矩阵中心化
	#n=1时保证第一行仍为1，反之则否
	if(is.complete(x)) 
	{
		m <- nrow(x)
		n<-ncol(x)
		temp<-rep(0,n-1)
		for(i in 2:n)
		{
			temp[i-1]<-mean(x[,i])
			x[, i] <- x[, i] - rep(temp[i-1], m)
		}
		if(flag!=1)
			x<-designmatrix.transform(x)
		return(list(matrix=x,average=temp))
	}#返回一个列表，其中Matrix为中心化后的矩阵；
	#vector为中心化向量，要注意其从第二列开始！
	x<-as.matrix(x)
	m <- nrow(x)
	n<-ncol(x)
	temp<-rep(0,n)
	for(i in 1:n)
	{
		temp[i]<-mean(x[,i])
		x[, i] <- x[, i] - rep(temp[i], m)
	}
	if(flag== 1)
		designmatrix.transform(x)
	return(list(matrix=x,average=temp))
}

standardize<-function(li)
{#将设计矩阵标准化,必须在中心化后才能使用
	#其返回类型由li决定
	x<-li$matrix
	if(is.complete(x))
	 {
		n<-ncol(x)
		temp<-rep(0,n-1)
		for(i in 2:ncol(x))
		{
			temp[i-1]<-sqrt(sum(x[,i]^2))
			if(temp[i-1])
				x[, i] <- x[, i]/temp[i-1]
		}
		return(list(matrix=x,average=li$average,variance=temp))
	}
	n<-ncol(x)
	temp<-rep(0,n)
	for(i in 1:n)
	{
		temp[i]<-sqrt(sum(x[,i]^2))
		if(temp[i])
			x[, i] <- x[, i]/temp[i]
	}
	return(list(matrix=x,average=li$average,variance=temp))
}
orthodox<-function(x)
{#将给定的矩阵正交化
	x[, 1]<-x[, 1]/sqrt(sum(x[, 1]^2))
	for(i in 2:ncol(x)) 
	{
		for(j in 1:(i - 1))
			x[, i] <- x[, i] - crossprod(x[, i], x[, j]) * x[, j]
		x[, i] <- x[, i]/sqrt(sum(x[, i]^2))
	}
	x
}

maincom<-function(x,y,ratio=0.8)
{#利用主成分法建立回归方程
	sys1<-centralize(x)
	x1<-sys1$matrix
	sys2<-eigen(t(x1)%*%x1)
	count<-0
	if(ratio>0&&ratio<=1)
	{
		if(ratio>0&&ratio<0.7)
			print("The contribution ration of the major component you give is a little small.
		It's strongly recommened that you choose a bigger one\n")
		prob<-0
		SUM<-sum(sys2$vectors)
	   while(prob<ratio)
	   {
		   count<-count+1
		   prob<-prob+sys2$values[count]/SUM
	   }
   }
   else
   {
	  print("Please enter an integer to stand for your choice:")
	  count<-scan(what=numeric(),n=1)
	  while(!(is.integer(count)&&count>0&&count<=ncol(x1)))
	  {
		 print("Wrong input for your choice!\n")
		 print("Please enter your choice again:")
		 count<-scan(what=numeric(),n=1)
	  }
   }
      if(count==1)
      {
	      part<-orthodox(sys2$vectors)[,1]
         temp1<-part%*%t(part)%*%t(x1)%*%y/sys2$values[1]
         temp2<-mean(y)-crossprod(sys1$average,temp1)
         return(list(number=count,values=sys2$values,estimation=c(temp2,temp1)))
      }
	   part<-orthodox(sys2$vectors)[,1:count]
	   temp1<-part%*%diag(1/(sys2$values[1:count]))%*%t(part)%*%t(x1)%*%y
	   temp2<-mean(y)-crossprod(sys1$average,temp1)
	   return(list(number=count,values=sys2$values,estimation=c(temp2,temp1)))
}
shade=function(constraint="locator()",type="l",spacing=200,slope=0,cp="auto",delta=10^-5)
#Copyright @ Chuanlong Du
{#This function is used to generate shaded graph
 #Parameter costraint is constraint of the area to be shaded. It can be list of functions or a list of points. 
 #The default way is to get the points from the graph.
 #Parameter col is the color of shade
 #Parameter type is the type of shade
 #Parameter spacing is the controls the number of points or lines
 #Parameter slope is the slope of the line
 #Parameter cp is the center point of the area to be shaded, the default value is "auto". It's only useful when 
 #the constraint is a list of functions or in concave case. 
############################################################################################
 #Warning: This function can only be used to shade convex areas. If a concave area is to be shaded, you must 
 #first divide it into several convex part and then shade very part. And the parameter cp should be the same 
 #for all the sub-convex area into order to get a neat graph.
###############################################################################################################
 #first get the constraint
 if(is.character(constraint) && constraint=="locator()")
 {
    cat("Please click in order at these acmes which restric the area to be shaded:\n")
    acmes=locator()
    #get the center point of the area to be shaded
    AcmeMat=cbind(acmes$x,acmes$y)
    ConNum=length(acmes$x)
 }
 else
 {#check the type of the constraint
    ConNum=length(constraint)
    if(is.vector(constraint[[1]]))
       if(length(constraint[[1]])==2)
       {#The constraints are points
          AcmeMat=NULL
          for(i in 1:ConNum)
              AcmeMat=rbind(AcmeMat,constraint[[i]])
       }
       else
          if(ConNum==2)
          {
             AcmeMat=cbind(constraint[[1]],constraint[[2]])
             ConNum=nrow(AcmeMat)
          }
    else
    {#The constraint is a list of functions
       #get the center point of the area to be shaded
       if(is.character(cp) && (cp=="auto" | cp=="locator()"))
       {
          cat("Please click at one points in the area to be shaded (last one used if more than one click):\n")
          cp=locator()
          n=length(cp$x)
          cpx=cp$x[[n]]
          cpy=cp$y[[n]]
       }
       else
       {
          cpx=cp[1]
          cpy=cp[2]
       }
       #to be continued
    }
 }
 if(is.character(cp) && cp=="auto")
 {#get the center point of the area to be shaded
    cpx=mean(AcmeMat[,1])
    cpy=mean(AcmeMat[,2])
 }
 else
    if(is.character(cp) && cp=="locator()")
    {
       cat("Please click at one points in the area to be shaded (last one used if more than one click)\n")
       cp=locator()
       n=length(cp$x)
       cpx=cp$x[[n]]
       cpy=cp$y[[n]]
    }
    else
    {
       cpx=cp[1]
       cpy=cp[2]
    }
 #information about the lines
 lineMat=matrix(0,nrow=ConNum,ncol=10)
 colnames(lineMat)=c("A","B","C","XL","XU","YL","YU","Sign","IPSign","InorOut")
 for(i in 1:(ConNum-1))
 {
    x1=AcmeMat[i,1]
    y1=AcmeMat[i,2]
    x2=AcmeMat[i+1,1]
    y2=AcmeMat[i+1,2] 
    lineMat[i,"A"]=y1-y2  
    lineMat[i,"B"]=x2-x1
    lineMat[i,"C"]=x2*y1-x1*y2
    lineMat[i,"XL"]=min(c(x1,x2))
    lineMat[i,"XU"]=max(c(x1,x2))
    lineMat[i,"YL"]=min(c(y1,y2))
    lineMat[i,"YU"]=max(c(y1,y2))
    lineMat[i,"Sign"]=sign(lineMat[i,"A"]*cpx+lineMat[i,"B"]*cpy-lineMat[i,"C"])
 }
 x1=AcmeMat[ConNum,1]
 y1=AcmeMat[ConNum,2]
 x2=AcmeMat[1,1]
 y2=AcmeMat[1,2]
 lineMat[ConNum,"A"]=y1-y2
 lineMat[ConNum,"B"]=x2-x1
 lineMat[ConNum,"C"]=y1*x2-x1*y2
 lineMat[ConNum,"XL"]=min(c(x1,x2))
 lineMat[ConNum,"XU"]=max(c(x1,x2))
 lineMat[ConNum,"YL"]=min(c(y1,y2))
 lineMat[ConNum,"YU"]=max(c(y1,y2))
 lineMat[ConNum,"Sign"]=sign(lineMat[ConNum,"A"]*cpx+lineMat[ConNum,"B"]*cpy-lineMat[ConNum,"C"])
 #information of the shade-line
 if(slope==Inf)
 {
    A0=1
    B0=0
    C0=AcmeMat[,1]
 }
 else
 {
    A0=slope
    B0=-1
    C0=slope*AcmeMat[,1]-AcmeMat[,2]
 }
 Cmin=min(C0)
 Cmax=max(C0)
 #---------------shade the area--------------------------------------------------
 #first generate a vector C0, which define a sequence a parallel lines
 C0=seq(Cmin,Cmax,length.out=spacing)
 C0=C0[2:(spacing-1)]
 for(Cline in C0)
 {
    #calculate the intersection points
    flag=0#indicate how many intersection points have been obtained
    IPMat=matrix(0,nrow=ConNum,ncol=2)#a matrix used to store the intersection points
    for(i in 1:ConNum)
    {#calculate the intersection points with ith line
       CoefMat=rbind(lineMat[i,1:2],c(A0,B0))
       deter=round(det(CoefMat),8)
       if(deter)
       {
          InterPoint=solve(CoefMat)%*%c(lineMat[i,3],Cline)
          #lineMat[,"IPSign"]=lineMat[,"A"]*InterPoint[1]+lineMat[,"B"]*InterPoint[2]-lineMat[,"C"]
          #lineMat[,"InorOut"]=lineMat[,"Sign"]*lineMat[,"IPSign"]
          #if(all(lineMat[,"InorOut"]>=0))
          #{
           #  flag=flag+1
            # IPMat[flag,]=InterPoint
          #}
          x=InterPoint[1]
          y=InterPoint[2]
          if(x<=lineMat[i,"XU"]+delta & x>=lineMat[i,"XL"]-delta & y<=lineMat[i,"YU"]+delta & y>=lineMat[i,"YL"]-delta)
          {
             flag=flag+1
             IPMat[flag,]=InterPoint
          }
       }
    }
    #find two appropriate intersection points (far from each other)
    Pflag1=0
    Pflag2=0
    Pdist=0
    for(i in 1:(flag-1))
    {
       P1=IPMat[i,]
       for(j in (i+1):flag)
       {
          P2=IPMat[j,]
          distance=sum((P1-P2)^2)
          if(distance>Pdist)
          Pflag1=i
          Pflag2=j
          Pdist=distance
       }
    }
    #draw line
    Points=IPMat[c(Pflag1,Pflag2),]
    xlim=c(min(Points[,1]),max(Points[,1]))
    ylim=c(min(Points[,2]),max(Points[,2]))
    if(slope==Inf)
    {
       x=rep(Cline,1000)
       y=seq(ylim[1],ylim[2],length.out=1000)
       points(x,y,type=type)
    }
    else
    {
       x=seq(xlim[1],xlim[2],length.out=1000)
       y=slope*x-Cline
       points(x,y,type=type)
    }
 }
}
 
 

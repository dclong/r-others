
boxcox.iterative<-function(x,y,IterativeNum=5,IntervalMin=-20,IntervalMax=20,ratio=0.5,PointNum=101)
{
	judge.interval.strong(ratio,0,1)
	judge.integer.positive(IterativeNum)
	if(IntervalMin>=IntervalMax)
		return("Wrong input of interval!")
	judge.integer.positive(PointNum)
	TempInterval<-IntervalMax-IntervalMin
	if(PointNum/TempInterval<2.5)
		return("The parameter PointNum is too small!. You are strongly suggested to change it to a biger one or shrink the interval.")
	TempMatrix<-projection1(x)
	x<-as.matrix(x)
	TempMatrix<-diag(nrow(x))-TempMatrix
	TempPara<-1
	TempMin<-Inf
	repeat
	{
		ParaVector<-seq(from=IntervalMin,to=IntervalMax,length=PointNum)
		for(i in 1:PointNum)
		{
			temp=boxcox.transform(y,ParaVector[i]) 
			RssTemp<-temp%*%TempMatrix%*%temp
			if(RssTemp<TempMin)
			{
			    TempMin<-RssTemp
			    TempPara<-ParaVector[i]
		    }
		}
		if(IterativeNum==1)
			return(c(TempPara,TempMin))
		else
		{
			IterativeNum<-IterativeNum-1
			TempInterval<-IntervalMax-IntervalMin
			IntervalMin<-TempPara-0.5*ratio*TempInterval
			intervalmax<-TempPara+0.5*ratio*TempInterval
		}
	}
}

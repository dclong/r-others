choicesummary=function(ch)
{#This function is used to summary the information calculated by equchoice
   n=length(ch$parameter)
   SpNo=1:n
   RMSq=order(ch$RMSq.order)
   Cp=order(ch$Cp.order)
   AIC=order(ch$AIC.order)
   Sum=RMSq+Cp+AIC
   VarNum=rep(0,n)
   for(i in 1:n)
      VarNum[i]=length(ch$parameter[[i]]$variable)-1
   return(cbind(SpNo,RMSq,Cp,AIC,Sum,VarNum))
}
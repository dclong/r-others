normalize=function(x)
{#This function takes in a matrix and normalize it to make sure that the sum of
#squares of each row is equal to 1.
#Paramter x is the matrix taken in
#According to its aim, there is some constraint for matrix x. There must be some
#non-zero values in each row.
sweep(x,1,sqrt(apply(x^2,1,sum)),FUN="/")
}
SampleSize=function(diff,sigma,power=0.8,contrast=1,side=2,alpha=0.05,varpro=rep(1,length(contrast)),npro=rep(1,length(contrast)))
{#This function is used to calcuate the sample size needed to achive a give power
#################################################################################
#Parameter diff is te difference to be detected
#-------------------------------------------------------------------------------
#Parameter sigma is the estimated common standard deviation
#-------------------------------------------------------------------------------
#Parameter contrast is the contrast coefficients of the treatment groups.
#If the decision is made base on only one group then contrast=1 which is
#the default value
#-------------------------------------------------------------------------------
#Parameter alpha is the significant level, which has the default value 0.05
#-------------------------------------------------------------------------------
#Parameter side indicates whether it's one-side or two-side test
#-------------------------------------------------------------------------------
#Parameter varpro is the proportion of the variances of the groups, which has
#the default value of equal proportion
#-------------------------------------------------------------------------------
#Parameter npro is the proportion of the sample sizes of each groups, which has
#the default value of equal proportion
if(diff==0)
   stop("parameter diff should not be 0!")
if(sigma<=0)
   stop("parameter sigma should have a postive value!")
if(alpha>=1|alpha<=0)
   stop("the significant level should be between 0 and 1!")
if(any(varpro<=0))
   stop("the elements of parameter varpro should be all positive!")
if(any(npro<=0))
   stop("the elements of parameter npro should be all positive!")
if(side!=1 & side!=2)
   stop("parameter side can only be 1 or 2!")
if(power>=1 | power<=0)
   stop("parameter power should be between 0 and 1!")
#one sample
ngroups=length(contrast)
if(ngroups==1)
{
   contrast=1
   varpro=1
   npro=1
}
else
{
   #standardize the proportion of npro
   npro=npro/npro
}
#calculate the sample size
constant=sum(contrast^2*varpro/npro)*sigma^2/diff^2
nstart=constant*(qnorm(alpha/side,lower.tail=F)+qnorm(power))^2
nstart=ceiling(nstart)
DF=ngroups*(nstart-1)
nfinal=constant*(qt(alpha/side,df=DF,lower.tail=F)+qt(power,df=DF))^2
nfinal=ceiling(nfinal)
while(nfinal!=nstart)
{#keep updating until the sample size doesn't change
   nstart=nfinal
   DF=ngroups*(nstart-1)
   nfinal=constant*(qt(alpha/side,df=DF,lower.tail=F)+qt(power,df=DF))^2
   nfinal=ceiling(nfinal)
}
ns=ceiling(nfinal*npro)
return(ns)
}
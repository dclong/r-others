Allocate=function(n,TrtNO)
{#This function is used to find the optimal allocation of the experiment units in one way ANOVA
#Be careful! Control group is most emphasized!
#-----------------------------------------------------------------------------------------------
#Parameter n is the total number of experiment units
#Parameter t is the number of treatments
#------------------------------------------------------------------------------------------------
TrtEU=1
SubNum=TrtNO-1
flag=1
minimum=(n-TrtNO+2)/(n-TrtNO+1)
while(SubNum<n)
{
   TrtEU=TrtEU+1
   SubNum=TrtEU*(TrtNO-1)
   variance=(n-(TrtNO-2)*TrtEU)/TrtEU/(n-(TrtNO-1)*TrtEU)
   if(variance<minimum)
   {
      flag=TrtEU
      minimum=variance
   }
   else
      if(variance==minimum)
         flag=c(flag,TrtEU)
}
AlloN=length(flag)
result=cbind(n-(TrtNO-1)*flag,flag,rep(minimum,AlloN))
colnames(result)=c("      NO. in Control Group","      NO. in Each of the Other Groups","      Minimum Variance")
rownames(result)=paste(rep("Allocation",AlloN),1:AlloN)
return(result)
}
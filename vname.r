
#' File and Directory Manipulation
#' Generates a valid file or folder name in a given directory.
#' @param file path of a file.
#' @param path path the parent directory of the file.
#' @return a valid file or directory name.
#' @author Chuanlong Benjamin Du
#' @seealso \code{tempfile}.
#' @keywords valid name
#' @examples
#' #create a temp file
#' fileName = "TEMP_TEMP.TXT"
#' file.create(fileName)
#' vname(fileName)
#'
vname <-
function(file,path=getwd())
{
  fext = file.extension(file)
  fname = file.name(file, full = FALSE)
  fcopy=dir(path=path,pattern=paste(fname,"\\([[:digit:]]*\\)",fext,sep = "",ignore.case=TRUE))
  if (length(fcopy) == 0)
    ncopy = paste(tempdir(), "/", fname, "(1)", fext,sep = "")
  else
  {
    MaxIndex = 0
    for (copy in fcopy)
    {
      copy = file.name(copy, full = FALSE)
      copy = gsub("^.*\\(", "", copy)
      copy = gsub("\\)$", "", copy)
      CurrentIndex = as.integer(copy)
      if (CurrentIndex > MaxIndex)
        MaxIndex = CurrentIndex
    }
    MaxIndex = MaxIndex + 1
    ncopy = paste(tempdir(), "/", fname, "(", MaxIndex,")", fext, sep = "")
  }
  return(ncopy)
}

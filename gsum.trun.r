#' This functions finds the group sums.
#' @param file the file (spreedsheet file) containing the students' grades.
#' @param output the file used to store the result.
#' @param score.header the header of the column containing the score to be analyzed.
#' @param group.header the header of the column cotaining group information of students.
#' @param group.only logical; if true, then only return result for each group, otherwise return result for each student.
#' @param trun the final range of the grades. All grades will be truncated to fall into this range.
#' @param score.range the range of grades, which helps to check whether there's mistake in the grades file.
#' @param header logical; whether to read  the first row of the file as headers.
#' @param sheet.index the index of the sheet to be read in.

gsum.trun=function(file,output=file,score.header="exam",group.header="Company",drop.number=4,group.only=TRUE,trun=c(0,7),score.range=c(0,100),header=TRUE,sheet.index=1)
{
	#read in grades file
  file.extension=tolower(file.ext(file))
  if(file.extension==".xls")
  {
    library(xlsReadWrite)
    read.xls(file=file,colNames=header,sheet=sheet.index)->grades
  }
  else
    if(file.extension==".xlsx")
    {
      library(xlsx)
      read.xlsx(file=file,sheetindex=sheet.index,header=header)->grades
    }
    else
      if(file.extension==".csv")
        grades=read.csv(file=file,header=header)
      else
        stop("an Excel document is required.")
	#replace missing grades with 0
	grades[,score.header][is.na(grades[,score.header])]=0
	#check whether all grades are satisfied
	grades[,score.header]>=min(score.range)&grades[,score.header]<=max(score.range)->check
	if(!all(check))
	{
		check.which=which(check)
		if(length(check.which)>1)
			stop("NO. ", check.which, "are not in the specified ranged.")
		else
			if(length(check.which)==1)
				stop("NO. ", check.which, "is not in the specified range.")
	}
	#format group
	grades[,group.header]=toupper(trim(grades[,group.header]))
  #calculate group means
	if(drop.number>0)
    group.sums=tapply(grades[,score.header],INDEX=grades[,group.header],FUN=function(x){sum(sort(x)[-(1:drop.number)])})
  else
    group.sums=tapply(grades[,score.header],INDEX=grades[,group.header],FUN=sum)
  if(!(is.vector(trun) && length(trun==2)))
    stop("trun should a vector a length 2 which specifies the final range of grades.")
  #truncate the lower tail of grades
  trun.lower=min(trun)
  ifelse(group.sums<trun.lower,trun.lower,group.sums)->group.sums
  #truncate the upper tail of grades
  trun.upper=max(trun)
  ifelse(group.sums>trun.upper,trun.upper,group.sums)->group.sums
  #decide what result should be returned
	if(group.only)
    result=group.sums
  else
  {
    #update the orginal data
    apply(outer(grades[,group.header],names(group.sums),"=="),1,which)->gsum.index
	  grades[,score.header]=group.sums[gsum.index]
	  result=grades
  }
  #write the result into the specified file
  output.extension=tolower(file.ext(output))
  if(output.extension==".csv")
      write.csv(result,file=output)
  else
      if(output.extension==".xls")
      {
        library(xlsReadWrite)
        write.xls(result,file=output)
      }
      else
        if(output.extension==".xlsx")
        {
          library(xlsx)
          write.xlsx(result,file=output)
        }
        else
          if(output.extension==".txt" || output.extension=="")
            write.table(result,file=output)
          else
            stop("the specified file type is not supported.")
}
#' Package Usage

#' Function \code{includePackages} includes (previously excluded) packages for usage survey. Function \code{excludePackages} excludes packages from usage survey. Function \code{getExcludedPackages} returns packages that are exluded from usage survey. Ususaly you know some packages are very useful for you, so you don't have survey their usage. 
#' @aliases includePackages excludePackages getExcludedPackages
#' @usage includePackages(pkgs)
#' excludePackages(pkgs)
#' getExcludedPackages()
#' @param pkgs a charactor vector containing packages. 
#' @example
#' #excludePackages("utils")
#' @export includePackages excludePackages getExcludedPackages
includePackages = function(pkgs){
    scan(file=.pkgs.exclude,what=character(),quiet=TRUE)->pkgs.exclude
    pkgs.exclude = setdiff(pkgs.exclude,pkgs)
    cat(pkgs.exclude,file=.pkgs.exclude)
}

excludePackages = function(pkgs){
    #add pkgs into file .pkgs.exclude
    scan(file=.pkgs.exclude,what=character(),quiet=TRUE)->pkgs.exclude
    pkgs.exclude = unique(c(pkgs.exclude,pkgs))
    cat(pkgs.exclude,file=.pkgs.exclude)
    #---------- drop fields from file .pkgs.freq --------------
    #read in historical packages
    pkgs.hist=readLines(con=.pkgs.freq,n=1)
    #get rid of the first column name, which is "Date"
    pkgs.hist = unlist(strsplit(pkgs.hist,split=" "))[-1]
    pkgs.hist = gsub("\"","",pkgs.hist,fixed=TRUE)
    pkgs.hist = gsub(" ","",pkgs.hist,fixed=TRUE)
    sapply(pkgs.hist,function(x)x%in%pkgs)->pkgs.hist.drop.bool
    if(any(pkgs.hist.drop.bool)){
        #read out the frequency table and create responding columns
        read.table(file=.pkgs.freq,header=TRUE)->pkgs.table
        pkgs.table = pkgs.table[,!pkgs.hist.drop.bool]
        #write the new table into the file
        write.table(pkgs.table,file=.pkgs.freq,row.names=FALSE)
    }
}

getExcludedPackages = function(){
    scan(file=.pkgs.exclude,what=character(),quiet=TRUE)
}
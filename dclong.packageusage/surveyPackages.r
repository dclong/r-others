#' Package Usage

#' Survey package usages.
#'
#' Each time function \code{surveyPackages} is called, the loaded packages and packages they depend on will be recorded once. A way to automate the survey is to add code \code{.Last=surveyPackages} in file \code{Rprofile.site}. 

#' @aliases surveyPackages 
#' @usage surveyPackages() 
#' @author Chuanlong Benjamin Du
#' @examples
#' #surveyPackages()
#' @export surveyPackages
surveyPackages = function(){
  #record current time
  current.time = Sys.time()
  #get all packages
  search() -> pkgs
  pkgs = grep(pattern="^package:",x=pkgs,value=TRUE)
  pkgs = sub(pattern="^package:",replacement="",x=pkgs)
  if(!"dclong.PkgsFreq"%in%pkgs){
    #try to load package dclong.PkgsFreq
    try(library(dclong.PkgsFreq),silent=TRUE)   
  }
  #find packages that are required by the loaded packages
  for(pkg in pkgs){
    pkgs = c(pkgs,packageDependency(pkg=pkg,lazy.input=FALSE))
  }
  pkgs = unique(pkgs)
  #packages excluded
  pkgs.exclude = scan(.pkgs.exclude,what=character(),quiet=TRUE)
  #useful packages
  pkgs = setdiff(pkgs,.pkgs.exclude)
  if(length(pkgs)==0){
    return()
  }
  #read in historical packages
  pkgs.hist=readLines(con=.pkgs.freq,n=1)
  #get rid of the first column name, which is "Date"
  pkgs.hist = unlist(strsplit(pkgs.hist,split=" "))[-1]
  pkgs.hist = gsub("\"","",pkgs.hist,fixed=TRUE)
  pkgs.hist = gsub(" ","",pkgs.hist,fixed=TRUE)
  #check whether there are new packages or not
  sapply(pkgs,function(x)x%in%pkgs.hist)->is.old
  if(!all(is.old)){
    if(length(pkgs.hist)==0){
      #write package names and record into the file
      pkgs.table=rep(1,length(pkgs))
      pkgs.table = t(pkgs.table)
      colnames(pkgs.table)=pkgs
      pkgs.table = as.data.frame(pkgs.table)
      pkgs.table = cbind(Date=current.time,pkgs.table)
      write.table(pkgs.table,file=.pkgs.freq,row.names=FALSE)
      return()
    }
    #read out the frequency table and create responding columns
    read.table(file=.pkgs.freq,header=TRUE)->pkgs.table
    #create new fields
    pkgs.table.new = matrix(0,nrow=nrow(pkgs.table),ncol=sum(!is.old))
    pkgs.new = pkgs[!is.old]
    colnames(pkgs.table.new) = pkgs.new
    #update records
    pkgs.table = cbind(pkgs.table,pkgs.table.new)
    #create a new record
    new.record = as.integer(sapply(pkgs.hist,function(x)x%in%pkgs))
    new.record = c(new.record,rep(1,length(pkgs.new)))
    new.record = data.frame(current.time,t(new.record))
    colnames(new.record) = colnames(pkgs.table)
    pkgs.table = rbind(pkgs.table,new.record)
    #write the new table into the file
    write.table(pkgs.table,file=.pkgs.freq,row.names=FALSE)
    return()
  }
  #no new packages, just append record to the file
  as.integer(sapply(pkgs.hist,function(x)x%in%pkgs))->new.record
  new.record = data.frame(current.time,t(new.record))
  write.table(new.record,file=.pkgs.freq,row.names=FALSE,col.names=FALSE,append=TRUE)
}


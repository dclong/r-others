#' Package Usage

#' Find packages that a specifed package depend on. 
#' @aliases packageDependency
#' @usage packageDependency(pkg, lazy.input=TRUE)
#' @param pkg character; a package.
#' @param lazy.input logical; If \code{TRUE}, then simple string (without white space and other special characters) can be used without double or single quotation.
#' @author Chuanlong Benjamin Du
#' @examples
#' packageDependency("dclong.FileSystem")
#' @importFrom dclong.String symbolToString trim
#' @export packageDependency
packageDependency = function(pkg,lazy.input=TRUE){
    if(lazy.input){
        pkg = dclong.String::symbolToString(substitute(pkg))
    }
    #read in the description file 
    path.description = system.file("DESCRIPTION",package=pkg)
    if(length(path.description)==0||path.description==""||is.null(path.description)){
        return(NULL)
    }
    readLines(con=path.description)->description
    description = dclong.String::trim(description)
    #check the Denpends field
    field.depends = extractField(description=description,"^Depends: ")
    #check the Imports field
    field.imports = extractField(description=description,"^Imports: ")
    #check the Suggests field
    field.suggests = extractField(description=description,"^Suggests: ")
    #check the LinkingTo field
    field.linkingto = extractField(description=description,"^LinkingTo: ")
    dependency = setdiff(unique(c(field.depends,field.imports,field.suggests,field.linkingto)),c("R",""))
    #check whether the dependency further depends on other packages recursively
    for(pkg in dependency){
        dependency = c(dependency,packageDependency(pkg=pkg,lazy.input=FALSE))
    }
    unique(dependency)
}

extractField = function(description,field.pattern){#extract field from description
    grep(pattern=field.pattern,x=description)->field.startIndex
    field.number = length(field.startIndex)
    if(field.number>1){
        stop("the field specified appears more than once in the description file. Something must go wrong.")
    }
    if(field.number<1){
        return(NULL)
    }
    #find end index 
    description.length = length(description)
    field.endIndex = field.startIndex + 1
    bool = description.length>=field.endIndex && !grepl(pattern="^[[:upper:]]{1}[[:alpha:]]+: ",x=description[field.endIndex])
    while(bool){
        field.endIndex = field.endIndex + 1
        bool = description.length>=field.endIndex && !grepl(pattern="^[[:upper:]]{1}[[:alpha:]]+: ",x=description[field.endIndex])    
    }
    field.endIndex = field.endIndex - 1
    #extract field content
    field.content = paste(description[field.startIndex:field.endIndex],collpase="")
    field.content = sub(pattern=field.pattern,replacement="",x=field.content)
    #get rid of (..)
    field.content = gsub(pattern="\\(.+\\)",replacement="",x=field.content)
    #split the field content by comma
    dclong.String::trim(unlist(strsplit(x=field.content,split=",",fixed=TRUE)))
}


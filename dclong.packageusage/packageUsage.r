#' Package Usage

#' Function \code{getUsages} returns a data frame containing packages used by users before. Function \code{dropRecords} drops some records from the package usage table. 
#' @aliases getUsages dropRecords 
#' @usage getUsages()
#' dropRecords(index,after,before)
#' param drop.index an integer vector which specifies the record to be dropped.
#' @param after either a date or a character specifying date in the format '%Y-%m-%d %H:%M:%S'.
#' @param before similar to argument \code{after}.
#' @export getUsages dropRecords

getUsages = function(){
    #read out the frequency table and create responding columns
    read.table(file=.pkgs.freq,header=TRUE)->pkgs.table
    #coerce the first column to date
    pkgs.table$Date = strptime(pkgs.table,'%Y-%m-%d %H:%M:%S')
    return(pkgs.table)
}
    
updateUsages = function(table){
    write.table(table,file=.pkgs.freq,row.names=FALSE)
}

dropRecords = function(index,after,before){
    pkgs.table = getPackagesTable()
    if(is.missing(after)){
        if(is.miss(before)){
            pkgs.table = pkgs.table[-index,]
            updateUsages(pkgs.table)
            return()
        }
        if(is.character(before)){
            before = strptime(before,'%Y-%m-%d %H:%M:%S')
        }
        index = pkgs.table$Date>after
        pkgs.table = pkgs.table[!index,]
        updateUsages(pkgs.table)
        return()
    }
    if(is.character(after)){
        after = strptime(after,'%Y-%m-%d %H:%M:%S')
    }
    index = pkgs.table$Date>after
    if(is.missing(before)){
        pkgs.table = pkgs.table[!index]
        updateUsages(pkgs.table)
    }
    if(is.character(before)){
        before = strptime(before,'%Y-%m-%d %H:%M:%S')
    }
    index = index&pkgs.table$Date>before
    pkgs.table = pkgs.table[!index,]
    updateUsages(pkgs.table)
}
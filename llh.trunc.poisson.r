llh.trunc.poisson=function(lambda,x)
{#This is the likelyhood function of truncated poisson distribution with
#parameter lambda
#Parameter lambda is the parameter for the poisson distribution
#x is the observation vector
return(-length(x)*(lambda+log(1-exp(-lambda)))+log(lambda)*sum(x))
}
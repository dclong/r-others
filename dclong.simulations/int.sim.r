

#' Monte Carlo
#' calculates integral of a function using monte carlo method. %% ~~ A concise
#' (1-5 lines) description of what the function does. ~~
#' 
#' %% ~~ If necessary, more details than the description above ~~
#' 
#' @param fun %% ~~Describe \code{fun} here~~
#' @param distribution %% ~~Describe \code{distribution} here~~
#' @param fpar1 %% ~~Describe \code{fpar1} here~~
#' @param fpar2 %% ~~Describe \code{fpar2} here~~
#' @param N %% ~~Describe \code{N} here~~
#' @param \dots %% ~~Describe \code{\dots} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords simulation integral
#' @examples
#' 
#' 
int.sim=function(fun,distribution="norm",fpar1="",fpar2="",N=10000,...)
{#This function simulates integrals using Monte Carlo method
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Parameter fun is the objective function to be integrated
#----------------------------------------------------------
#Parameter den specifies which distribution to use.
#A default distribution of normal will be used.
#-----------------------------------------------------------
#Parameter fpar1 and fpar2 are parameters for the specified distribuiton.
#Default values in R will be used if they are not specified.
#------------------------------------------------------------
#Parameter N is the sample size of simulation, which has a default value 10000
#-------------------------------------------------------------
#... are additional parameters for fun
#------------------------------------------------------------------
den=tolower(distribution)
objsam=NULL
if(fpar1!="")
  if(fpar2!="")
  {
    ransam=get(paste("r",distribution,sep=""))(N,fpar1,fpar2)
    for(eachsam in ransam)
      objsam=c(objsam,fun(eachsam,...)/(get(paste("d",distribution,sep=""))(eachsam,fpar1,fpar2)))
  }
  else
  {
    ransam=get(paste("r",distribution,sep=""))(N,fpar1)
    for(eachsam in ransam)
      objsam=c(objsam,fun(eachsam,...)/(get(paste("d",distribution,sep=""))(eachsam,fpar1)))
  }
else
  if(fpar2!="")
  {
    ransam=get(paste("r",distribution,sep=""))(N,,fpar2)
    for(eachsam in ransam)
      objsam=c(objsam,fun(eachsam,...)/(get(paste("d",distribution,sep=""))(eachsam,,fpar2)))
  }
  else
  {
    ransam=get(paste("r",distribution,sep=""))(N)
    for(eachsam in ransam)
      objsam=c(objsam,fun(eachsam,...)/(get(paste("d",distribution,sep=""))(eachsam)))
  }
return(mean(objsam))
}

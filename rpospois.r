rpospois=function(n,lambda)
{#This function is used to generate truncated poisson random variables
#Parameter n is the sample size
#parameter lambda is the parameter for poisson distribution
sam=NULL
for(i in 1:n)
{
   observation=rpois(1,lambda)
   while(!observation)
   {
        observation=rpois(1,lambda)
   }
   sam=c(sam,observation)
}
return(sam)
}

#' @title Character and String Manipulation.
#' @description Search for pair matches in a given string. In may texts, there're symbos appear in pair. 
#' For example, in code written in C/C++, Java and R symbols "{" and "}" appear in pair. 
#' Functions listed here help to extract/replace substring in which given symbols appear in pair.
#' @aliases pairMatch pairMatchOnce pairMatchSub
#' @usage pairMatch(left, right, x, start=1, value=TRUE, fixed=FALSE)
#' pairMatchOnce(left, right, x, start=1, value=TRUE, fixed=FALSE)
#' pairMatchSub(left, right, replacement, x, start=1, all=TRUE, fixed=FALSE)
#' jpairMatch(left, right, x, start=1, value=TRUE, fixed=FALSE)

#' @param left a string which indicate the left delimiter (doesn't have to be a single character).
#' @param right a string which indicate the right delimiter (doesn't have to be a single character).
#' @param x the string to be searched in.
#' @param start If it's a character or string or a regular expression pattern, 
#' then start serach from this character or string or the string that matches the pattern; 
#' if it's an integer, then start search from index \code{start}.
#' @param replacement a replacement for the substrings which match the specified pattern. 
#' It can be either a string or a function that can be applied on the the substring to be replaced and return a new string.
#' @param all logical; If TRUE, then all occurrences that match the specified pattern will be replaced, 
#' o.w. only the first occurrence will be replaced.
#' @param value logical; If TRUE then string matches the pattern is return, o.w. the index of the string is returned.
#' @param fixed logical. If TRUE and start is string or regular expression pattern, 
#' start is a string to be matched as is. Overrides all conflicting arguments.
#' @return A string (or index of it, which is a vector of length 3 with the form (start, leftIndex, rightIndex)) 
#' that matches the specified pattern. 
#' If no string that matches the specified pattern is found, 
#' then an empty string (or a index of one of the forms (start, leftIndex, 0), 
#' (start, 0, rightIndex) and (start, 0, 0)) is returned. 
#' (start, leftIndex, 0) means that starting from index \code{start}, 
#' the right delimiter is missing (this doesn't mean that the right delimiter never appears staring from index |code{start}, 
#' it might appears, but not enough to match the left delimiter); 
#' (start, 0, rightIndex) means that start from index \code{start}, 
#' the left delimiter doesn't appears but the right delimiter appears at index \code{rightIndex}; 
#' (start, 0, 0) menas that start from index \code{start}, neither the left nor the right delimiter appears.
#' @author Chuanlong Benjamin Du
#' @seealso \code{\link[base]{sub}} and \code{\link[base]{gsub}}.
#' @note the which is common in code written in all kinds of programming languages. 
#' For example, "{" and "}" appear pairly in code written in C/C++, R and Java. However, the left and the right delimiter can be the same.
#' @keywords match pair
#' @examples
#' \dontrun{
#' #create a string
#' str="abd;{ad{cl}{}}lll"
#' #want to extract "{...}" out
#' pairMatchOnce("{","}",str)
#' pairMatch(left="{",right="}",x=str)
#' }

#' @export
#' @importFrom rJava .jevalArray .jcall
#' @rdname pair-match
pairMatchSub <-
function(left,right, replacement, x, start=1, all=TRUE,fixed=FALSE){
  if(all){
    subStrIndex = pairMatch(left=left,right=right,x=x,
                  start=start,value=FALSE,fixed=fixed)
    if(!is.null(subStrIndex)){
      subStrNumber = nrow(subStrIndex)
      if(class(replacement)=="character"){
        for(i in subStrNumber:1){
          x = strReplace(x=x,first=subStrIndex[i,2],
                  last=subStrIndex[i,3],replacement=replacement)
        }
        return(x)
      }
      if(class(replacement)=="function"){
        for(i in subStrNumber:1){
          tempStr = substr(x,subStrIndex[i,2],subStrIndex[i,3])
          x = strReplace(x=x,first=subStrIndex[i,2],
                  last=subStrIndex[i,3],replacement=replacement(tempStr))
        }
        return(x)
      }
    }
    return(x)
  }else{
    subStrIndex = pairMatchOnce(left=left,right=right,x=x,
                start=start,value=FALSE,fixed=fixed)
    if(subStrIndex[2]>0 && subStrIndex[3]>0){
      if(class(replacement)=="character"){
        x = strReplace(x=x,first=subStrIndex[i,2],
              last=subStrIndex[i,3],replacement=replacement)
        return(x)
      }
      if(class(replacement)=="function"){
        tempStr = substr(x,subStrIndex[i,2],subStrIndex[i,3])
        x = strReplace(x=x,first=subStrIndex[i,2],
              last=subStrIndex[i,3],replacement=replacement(tempStr))
        return(x)
      }
    }
    return(x)
  }
}
#' @export
#' @rdname pair-match
pairMatch <-
function(left, right, x, start=1, value=TRUE, all=TRUE, fixed=FALSE){
  if(!all){
	return(pairMatchOnce(left,right,x,start,value,fixed))
  }
  result = NULL
  rightDelimiterLength = nchar(right)
  if(value){
    repeat{
      temp = pairMatchOnce(left=left,right=right,
              start=start,x=x,value=FALSE,fixed=fixed)
      if(temp[2]>0 && temp[3]>0){

        result = c(result,substr(x,temp[2],temp[3]))
        start = temp[3] + 1
      }
      else{
        break
      }
    }
    return(result)
  }
  repeat{
    temp = pairMatchOnce(left=left,right=right,start=start,x=x,value=FALSE,fixed=fixed)
    if(temp[2]>0 && temp[3]>0){
      result = rbind(result,temp)
      start = temp[3] + 1
    }else{
        break
    }
  }
  return(result)
}

pairMatchOnce <-
function(left, right, x, start=1, value=TRUE,fixed=FALSE)
{
  #check return type
  if(value){
    index=pairMatchOnce(left=left,right=right,x=x,start=start,value=FALSE,fixed=fixed)
    if(index[2]>0 && index[3]>0){
      return(substr(x,index[2],index[3]))
    }
    return("")
  }
  #check parameters
  if(!is.character(left))
    stop("argument left must be a single character.")
  if(length(left)>1)
    stop("argument left must be a single character.")
  if(!is.character(right))
    stop("argument right must be a single character.")
  if(length(right)>1)
    stop("arugment right must be a single character.")
  #intial return result
  result = t(c(start,0,0))
  colnames(result) = c("startIndex","leftIndex","rightIndex")
  #start index
  if(is.character(start)){
    start = regexpr(pattern = start, x, fixed = fixed)
    if(start<=0){
      return(result)
    }
  }
  #the length of the left and right delimiter for future use
  leftDelimiterLength = nchar(left)
  rightDelimiterLength = nchar(right)
  xLength = nchar(x)
  #when the left and the right delimiters are the same
  if(left==right){
    i = start
    while(i<=xLength){
      if(substr(x,i,i+leftDelimiterLength-1) == left){
        result[2] = i
        break
      }
      i = i + 1
    }
    if(result[2]>0){
      i = result[2] + 1
      while(i<=xLength){
        if(substr(x,i,i+rightDelimiterLength-1)==right){
          result[3] = i
          break
        }
        i = i + 1
      }
    }
    if(result[3]>0){
      result[3] = result[3] + rightDelimiterLength -1;
    }
    return(result)
  }
  #when the left and the right delimters are not the same
  i = start
  while(i<=xLength){
    if(substr(x,i,i+leftDelimiterLength-1)==left){
      result[2] = i
      break;
    }
    i = i + 1
  }
  if(result[2]>0){
    toBeMatched = 1
    start = result[2] + leftDelimiterLength
    i = start
    while(i<=xLength){
      if(substr(x,i,i+leftDelimiterLength-1)==left){
        toBeMatched = toBeMatched + 1
        start = i + leftDelimiterLength
      }else if(substr(x,i,i+rightDelimiterLength-1)==right){
        toBeMatched = toBeMatched - 1
        start = i + rightDelimiterLength
        if(toBeMatched==0){#the pair match is found
          result[3] = i
          break;
        }
      }
      i = i + 1
    }
    if(result[3]>0){
      result[3] = result[3] + rightDelimiterLength-1;
    }
    return(result)
  }
  i = start
  while(i<=xLength){
    tempStr = substr(x,i,i)
    if(substr(x,i,i+rightDelimiterLength-1)==right){
        result[3] = i
        break;
    }
    i = i + 1
  }
  if(result[3]>0){
    result[3] = result[3] + rightDelimiterLength-1;
  }
  return(result)
}
#' @export
#' rdname pair-match
jpairMatch <-
function(left, right, x, start=1, value=TRUE, fixed=FALSE){
  rJava::.jcall(obj=.objPairMatch,returnSig="[[I",method="pairMatch",left,right,x,as.integer(start-1))->index
  index = t(sapply(index,rJava::.jevalArray))
  if(length(index)==0){
    index = NULL
  }else{
    index = index + 1
  }
  if(value){
    if(is.null(index)){
      return("")
    }
    numberFound = nrow(index)
    result = character(numberFound)
    for(i in 1:numberFound){
      result[i] = substr(x,index[i,2],index[i,3])
    }
  }
}

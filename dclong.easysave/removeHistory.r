
#' R Workspace and History.

#' Remove history file associatd with the current workspace or history files that have no R image associated with it.
#'
#' Function \code{clearHistory} removes history files that have no R image associated with it.

#' @aliases clearHistory
#' @usage clearHistory()
#' @author Chuanlong Benjamin Du
#' @seealso \code{\link{savewh}}, \code{\link{clearHistory}}.
#' @keywords remove history command clear
#' @examples
#' #rmhist()
#' @export clearHistory
clearHistory = function(){
  file.list = dir(path=getHistFolder(),pattern = "\\.rhistory$",
                  full.names=FALSE,ignore.case=TRUE)
  ids = gsub(pattern="\\.rhistory$","",x=file.list,ignore.case=TRUE)
  ids = as.numeric(ids)
  ids = setdiff(ids,.histID)
  for(id in ids){
    if(is.null(getHistImage(id))){#how about these history files whose image has been deleted?
      removeHistoryByID(id)
    }
  }
  file.list = dir(path=getHistFolder(),pattern = "_image$",
                      full.names=FALSE,ignore.case=TRUE)
  ids = gsub(pattern="_image$","",x=file.list,ignore.case=TRUE)
  ids = as.numeric(ids)
  ids = setdiff(ids,.histID)
  for(id in ids){
    if(!file.exists(getHistFile(id))){
      removeHistoryByID(id)
    }
  }
}

removeHistoryByID = function(hist.id){
  getHistFile(hist.id)->hist.file
  if(file.exists(hist.file)){
    file.remove(hist.file)
  }
  hist.image.host = getHistImageHost(hist.id)
  if(file.exists(hist.image.host)){
    file.remove(hist.image.host)
  }
}
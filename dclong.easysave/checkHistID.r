#' R Workspace and History.
#' Some help methods.
#'
#' Function \code{checkHistID} get a valid ID (positive integer) for saving R history; function \code{checkHistTitle} get a valid title for saving R workspace. Function \code{readInTitle} read in a title for saving R workspace.
#' @param title a string.
#' @note Functions here are intended to be used in this package only. Don't use it anywhere else unless you know how it works.
#' @author Chuanlong Benjamin Du
#' @examples
#' #getHistID()
#' @importFrom stats rbinom

readInTitle = function(){
  cat("No legal title is specified! Wanna specify a title for saving the workspace?\n")
  cat("Y/y: Yes, I want to specify a title.\n")
  cat("N/n: No, let the program handle it.\n")
  cat("C/c: Cancel the operation\n.")
  flush.console()
  userIntent=scan(what=character(0),n=1)
  userIntent=tolower(userIntent)
  if(userIntent=="y"){
    cat("Please enter a title:\n")
    flush.console()
    title=scan(what=character(0),n=1)
    title=gsub("[[:space:]]+$","",title)
    title=gsub("^[[:space:]]+","",title)
    if(title==""){
      stop("the title cannot be an empty string!")
    }
    return(title)
  }
  if(userIntent=="n"){
    title=gsub(".*[/|\\\\]","",getwd())
    return(title)
  }
  stop("Operation canceled by User!")
}

ftemp = function(){
  cat("The value of histID is: ",histID,"\n")
}

checkHistID = function(){
  if(exists(".histID")&&.histID>0){
    return(.histID)
  }
  #first get the path of the directory for saving history files
  #get all file in the folder
  file.list = dir(path=getHistFolder(),pattern = "\\.rhistory$",ignore.case=TRUE)
  ids = gsub(pattern="\\.rhistory$","",x=file.list,ignore.case=TRUE)
  ids = as.numeric(ids)
  #random decide where to start searching, this is done to ensure both speed and possibility to cover available small IDs
  hist.id = 1
  if(stats::rbinom(n=1,size=1,prob=0.9)){
      hist.id = length(ids) + 1
  }
  while(hist.id %in% ids){
    hist.id = hist.id + 1
  }
  .histID <<- hist.id
  return(.histID)
}

checkTitle = function(title){
  if(title!=""){
    .wsName <<- title
  }
  if(exists(".wsName")&&is.character(.wsName)&&length(.wsName)==1){
    return(.wsName)
  }
  .wsName <<- readInTitle()
  if(grepl('\\.rdata{1}$',.wsName,ignore.case=TRUE)){
      .wsName<<-gsub('\\.rdata{1}$',"",.wsName,ignore.case=TRUE)
  }
  return(.wsName)
}
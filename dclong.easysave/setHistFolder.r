
#' R Workspace and History.
#'
#' Set the folder for saving history files.
#'
#' Function \code{\link{setHistFolder}} set a folder for saving history files. Function \code{\link{resetHistFolder}} set a default folder for saving history files.
#' @aliases setHistFolder resetHistFolder getHistFolder
#' @usage resetHistFolder() 
#' setHistFolder(path)
#' getHistFolder()
#' @param path the path of the folder for saving history files.
#' @author Chuanlong Bejamin Du
#' @keywords set history folder directory
#' @examples
#' #resetHistFolder()
#' @export setHistFolder resetHistFolder getHistFolder

setHistFolder <- function(path){
  if(missing(path)){
    newHistFolder = choose.dir(default=getPackageHome())
    if(!is.na(newHistFolder)){
      path = newHistFolder
    }else{
      return()
    }
  }
  old.path = getHistFolder()
  if(path!=old.path){
    #print copying information
    cat("Copying history files to the new directory ...\n")  
    #copy files
    file.copy(from=dir(old.path,full.names=TRUE),to=path)
    #write the new path into the configuration file
    writeLines(path,con=getConfFile())    
  }
}

getHistFolder = function(){
  hist.folder=dclong.String::trim(readLines(getConfFile()))
  if(is.na(hist.folder)||length(hist.folder)==0){
    hist.folder = ""
  }
  return(hist.folder)
}

getConfFile <-function(){
  paste(getPackageHome(),"conf.txt",sep="/")
}

getPackageHome <- function(){
  system.file(package="dclong.EasySave")
}

resetHistFolder = function(){
  setHistFolder(paste(getPackageHome(),"history",sep="/"))
}


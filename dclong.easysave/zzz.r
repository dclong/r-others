
.onLoad = function(lib,pkg){
  #load library
  library(track)
  #create .histID
  checkHistID()
  #auto track history command
  if(interactive()){
    track.history.start(file=getHistFile(),style="full")    
  }
}
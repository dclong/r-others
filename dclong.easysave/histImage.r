
#' R Workspace and History.

#' Get the path of the history folder or R image which is associated with a history file.
#'
#' Function \code{getHistImage} returns the path of the R image which is associated with a history ID.
#' @aliases getHistImage
#' @usage getHistImage(hist.id=.histID)
#' @param hist.id a positive integer which is the ID for some history file.

#' @author Chuanlong Bejamin Du

#' @keywords image workspace path

#' @examples
#' #getHistImage()
#' @export getHistImage
getHistImage = function(hist.id=.histID){
  getHistImageHost(hist.id)->hist.image
  if(file.exists(hist.image)){
    paste(readLines(hist.image),collapse="")->path
    path = dclong.String::trim(path)
    return(path)
  }
  return(NULL)
}

getHistImageHost = function(hist.id=.histID){
  paste(getHistFolder(),"/",hist.id,"_image",sep="")
}

getHistFile = function(hist.id=.histID){
  paste(getHistFolder(),"/",hist.id,".rhistory",sep="")
}




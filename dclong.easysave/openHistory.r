
#' R Workspace and History.
#' Save/open R history commands and R workspace.
#'
#'The history commands will be save to be a file "n.history" in directory "history" under the home directory of this package, where "n" is a positive integer decide by function \code{getHistID}. The value of "n" is also saved to variable \code{.histID} in the workspace. The title for saving R workspace is saved in variable \code{.wsName} in the workspace. Function \code{saveWorkspace} saves R workspaces. Function \code{openHistory} (or \code{openh}) opens R history. Function \code{track.history.load} is a modified version of the \code{\link[track]{track.history.load}} in package \code{track}, so that it allows saving history in any folder instead of the same folder with the R image.
#' @aliases saveHistory saveh saveWorkspace savew savewh savehw openHistory openh track.history.load
#' @usage saveWorkspace(title="",path=getwd(),lazy.input=TRUE)
#' savew(title="",path=getwd(),lazy.input=TRUE)
#' openHistory(show=25, reverse=FALSE, pattern,...)
#' openh(show=25, reverse=FALSE, pattern,...)
#' track.history.load(times=FALSE)
#' @param title a title for saving R workspace.
#' @param path the path of the directory where R workspace is to be saved in. By default, the current working directory will be used.
#' @param lazy.input logical; If true, then simple string (without white space and other special characters) can be used without double or single quotation.
#' @param show integer; the number of command lines to show. Inf will give all of the currently available history.
#' @param reverse logical. If TRUE, the lines are shown in reverse order. Note: this is not useful when there are continuation lines.
#' @param pattern a character string to be matched against the lines of the history.
#' @param ...	 arguments to be passed to grep when doing the matching.
#' @param times whether to include time stamps. 
#' @author Chuanlong Benjamin Du
#' @note You can modify the opend history file as you want, but the changes won't be saved in the history file.
#' @keywords history command workspace image save open
#' @examples
#' #openh()
#' @export saveWorkspace savew openHistory openh track.history.load
#' @importFrom dclong.String symbolToString trim
#' @importFrom dclong.FileSystem fileName

track.history.load = function (times = FALSE) 
{
    file <- dclong.FileSystem::fileName(getOption("incr.hist.file"))
    if (is.null(file) || nchar(file) == 0) 
        file <- Sys.getenv("R_INCR_HIST_FILE")
    if (is.null(file) || nchar(file) == 0) 
        file <- ".Rincr_history"
    if (file.exists(file)) {
        cat("Loading incremental history file", file, "\n")
        if (times) {
            utils:::loadhistory(file)
        }
        else {
            file2 <- tempfile(file)
            on.exit(unlink(file2))
            writeLines(grep("^##------ .* ------##$", readLines(file, 
                -1), invert = TRUE, value = TRUE), con = file2)
            utils:::loadhistory(file2)
        }
    }
    else {
        cat("Incremental history file", file, "does not exist\n")
    }
}

openHistory <-
function(show=25,reverse=FALSE,pattern,...){
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  file.show <- tempfile(fileName(getHistFile()))
  on.exit(unlink(file.show))
  commands = dclong.String::trim(grep("^##------ .* ------##$", readLines(file.show, -1), invert = TRUE, value = TRUE))
  commands = commands[commands!=""]
  if(!missing(pattern)){
    commands = unique(grep(pattern=pattern,x=commands,value=TRUE,...))
  }
  if(reverse){#the commands were stored in reversed order
    commands = rev(commands)
  }
  commands.length = length(commands)
  commands = commands[(commands.length-show+1):commands.length]
  writeLines(text=commands,con=file.show,sep="\n")
  file.edit(file.show)
}

openh = openHistory

saveWorkspace <-
function(title="",path=getwd(),lazy.input=TRUE)
{#This fucntion saves the image of R
#+++++++++++++++++++++++++++++++++++++++++++++++++++
#Argument title is the name to be used
#+++++++++++++++++++++++++++++++++++++++++++++++++++
#Argument path is the path of the
  if(lazy.input){
    title = dclong.String::symbolToString(substitute(title))
    path = dclong.String::symbolToString(substitute(path))
  }
  title = dclong.String::trim(title)
  path = dclong.String::trim(path)
  checkTitle(title)
  file=paste(path,"/",.wsName,".rdata",sep="")
  save.image(file)
  getHistImageHost()->hist.image.host
  cat(file,file=hist.image.host,append=FALSE)
}

savew = saveWorkspace

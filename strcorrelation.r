
#' Character and String Manipulation.

#' Calculate a fuzzy correlation between two string, which meausres how close these two strings are. It can be used to do fuzzy match, for example many students make mistakes when putting down their IDs or even names. Function(s) listed here can help to check which string in a database is closest to a miss-spelled one.   

#' @param str.mistake a string with mistakes. 
#' @param str.right a string without mistakes. 
#' @param set a vector of strings which acts as the database for possible missing characters, words and so on for the string with mistakes.

#' @author Chuanlong Benjamin Du

#' @note Don't make argument \code{set} too long, o.w. it might take too much time to enumerate all possible cases. The elements of set don't have to be single characters, they can be a whole word, e.g. "the". We know that combinations "hte", "het" and so seldom happens, so putting word "the" into argument \code{set} base can much more efficient than putting "t", "h" and "e" into the arugment \code{set}. 
 
fuzzyCorrelation = function(str.mistake, str.right,set=strRight){
    
}
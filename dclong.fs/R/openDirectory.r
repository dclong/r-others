
#' File/Direcotry Manipulation.
#' Opens a specified directory.
#' @aliases openDirectory opend
#' @usage openDirectory(path=getwd(),dd=NULL,lazy.input=TRUE)
#' opend(path=getwd(),dd=NULL,lazy.input=TRUE)
#' @param path the path the directory to be opened.
#' @param dd a positive integer which is the index of some directory defined. You can use function \code{\link{showDirDef}} to check the indeces of all defined directories.
#' @param lazy.input logical; If true, then simple string (without white space and other special characters) can be used without double or single quotation.
#' @note There's no guarantee that this function can always handle windows relative paths.
#' @author Chuanlong Benjamin Du
#' @seealso \code{\link{openf}}.
#' @keywords directory manipulation open
#' @examples
#' opend()
#' @export openDirectory opend
openDirectory <-
function(path=getwd(),dd=NULL,lazy.input=TRUE)
{
  if(lazy.input){
    path=dclong.String::symbolToString(substitute(path))
  }
  if(!is.null(dd)){
    path=getDirDef(dd)
  }
  shell.exec(path)
}

opend=openDirectory
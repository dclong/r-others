
#' File and Directory Manipulation.
#' Removes specified files, which is a refined version of \code{\link[base]{file.remove}}.
#' @aliases removeFile rmf
#' @usage removeFile(...,flist=NULL,lazy.input=TRUE)
#' rmf(...,flist=NULL,lazy.input=TRUE)
#' @param \dots files to be removed.
#' @param flist a character vector which specifies a list of files to be removed.
#' @param lazy.input logical; If true, then simple string (without white space and other special characters) can be used without double or single quotation.
#' @note This function will remove specified files, so be careful when you use it.
#' @author Chuanlong Benjamin Du
#' @seealso \code{\link[base]{file.remove}}.
#' @keywords file remove
#' @examples
#' #create a temp file
#' fileName = "TEMP_TEMP.TXT"
#' file.create(fileName)
#' #remove the temp file
#' removeFile(fileName)
#' @export removeFile rmf
removeFile <-
function(...,flist=NULL,lazy.input=TRUE)
{#argument path only affect argument ext
#+++++++++++++++++++++++++++++++++++++++++
  dots=match.call(expand.dots=FALSE)$...
  if(lazy.input){
    #dots=sapply(dots,substitute)
    dots=sapply(dots,dclong.String::symbolToString)
    flist=dclong.String::symbolToString(substitute(flist))
  }
  if(!length(dots))
    dots=character(0)
  else
    dots=file.path(dots)
  if(!is.null(flist))
    dots=.Primitive("c")(dots,flist)
  .Internal(file.remove(dots))
}

rmf=removeFile



#' File and Directory Manipulation.
#' Opens a file using with a given extension.
#' @aliases openf.csv openf.doc openf.docx openf.dvi openf.eps openf.pdf openf.r openf.rdata openf.rhistory openf.txt openf.log openFile openf
#' @usage openf.csv(title,path=getwd(),dd=NULL,lazy.input=TRUE)
#' openf.doc(title,path=getwd(),dd=NULL,lazy.input=TRUE)
#' openf.docx(title,path=getwd(),dd=NULL,lazy.input=TRUE)
#' openf.dvi(title,path=getwd(),dd=NULL,lazy.input=TRUE)
#' openf.eps(title,path=getwd(),dd=NULL,lazy.input=TRUE)
#' openf.pdf(title,path=getwd(),dd=NULL,lazy.input=TRUE)
#' openf.r(title,path=getwd(),dd=NULL,lazy.input=TRUE)
#' openf.rdata(title,path=getwd(),dd=NULL,lazy.input=TRUE)
#' openf.rhistory(title,path=getwd(),dd=NULL,lazy.input=TRUE)
#' openf.txt(title,path=getwd(),dd=NULL,lazy.input=TRUE)
#' openf.log(title,path=getwd(),dd=NULL,lazy.input=TRUE)
#' openFile(title="",extension="",path=getwd(),dd=NULL,lazy.input=TRUE)
#' openf(title="",extension="",path=getwd(),dd=NULL,lazy.input=TRUE)
#' @param title title of the file.
#' @param extension extension of the file.
#' @param path the path of the file to be opened.
#' @param dd index of a default path (refer to \code{\link{showDirDef}}).
#' @param lazy.input logical; If true, then simple string (without white space
#' and other special characters) can be used without double or single quotation.
#' @note There's not no guarantee that this functions can always handle windows
#'   relative paths.
#' @author Chuanlong Benjamin Du
#' @seealso \code{\link{opend}}.
#' @keywords file manipulation
#' @examples
#' #create a temp file
#' fileName="TEMP_TEMP.txt"
#' #open the file
#' openFile(fileName)
#'
#' @export openf.csv openf.doc openf.docx openf.dvi openf.eps openf.log openf.pdf openf.r openf.rdata openf.rhistory openf.txt openFile openf
openFile <-
function(title="",extension="",path=getwd(),dd=NULL,lazy.input=TRUE)
{#This function opens a given file
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Argument path is the path the file to be opened
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++
  if(lazy.input){
    title=dclong.String::symbolToString(substitute(title))
    extension=dclong.String::symbolToString(substitute(extension))
    path=dclong.String::symbolToString(substitute(path))
  }
  if(!is.null(dd)){
    path=getDirDef(dd)
  }
  if(title==""){
    file.edit("")
  }else{
    filename=paste(path,title,sep="/")
    if(file.exists(filename)){
      if(extension!=""){
        newfilename=paste(filename,extension,sep="")
        if(file.exists(newfilename)){
          shell.exec(newfilename)
        }
        else{
          shell.exec(filename)
          warning("Please make sure the opened file is the one you meant.")
        }
      }
      else{
        shell.exec(filename)
      }
    }
    else{
      if(extension==""){
        warning("The specified file does not exist. Operation is canceled.")
      }
      else{
        newfilename=paste(filename,extension,sep="")
        if(file.exists(newfilename)){
          shell.exec(newfilename)
        }
        else{
          warning("The specified file does not exist. Operation is canceled.")
        }
      }
    }
  }
}

openf=openFile

openf.txt <-
function(title,path=getwd(),dd=NULL,lazy.input=TRUE)
{
  if(lazy.input){
    title=dclong.String::symbolToString(substitute(title))
    path=dclong.String::symbolToString(substitute(path))
  }
  if(!is.null(dd)){
    path=getDirDef(dd)
  }
  openf(title=title,extension='.txt',path=path,lazy.input=FALSE)
}

openf.rhistory <-
function(title,path=getwd(),dd=NULL,lazy.input=TRUE)
{
  if(lazy.input){
    title=dclong.String::symbolToString(substitute(title))
    path=dclong.String::symbolToString(substitute(path))
  }
  if(!is.null(dd)){
    path=getDirDef(dd)
  }
  openf(title=title,extension='.rhistory',path=path,lazy.input=FALSE)
}

openf.rdata <-
function(title,path=getwd(),dd=NULL,lazy.input=TRUE)
{
  if(lazy.input){
    title=dclong.String::symbolToString(substitute(title))
    path=dclong.String::symbolToString(substitute(path))
  }
  if(!is.null(dd))
    path=getDirDef(dd)
  openf(title=title,extension='.rdata',path=path,lazy.input=FALSE)
}

openf.r <-
function(title,path=getwd(),dd=NULL,lazy.input=TRUE)
{
  if(lazy.input){
    title=dclong.String::symbolToString(substitute(title))
    path=dclong.String::symbolToString(substitute(path))
  }
  if(!is.null(dd)){
    path=getDirDef(dd)
  }
  openf(title=title,extension='.r',path=path,lazy.input=FALSE)
}

openf.pdf <-
function(title,path=getwd(),dd=NULL,lazy.input=TRUE)
{
  if(lazy.input){
    title=dclong.String::symbolToString(substitute(title))
    path=dclong.String::symbolToString(substitute(path))
  }
  if(!is.null(dd)){
    path=getDirDef(dd)
  }
  openf(title=title,extension='.pdf',path=path,lazy.input=FALSE)
}

openf.log <-
function(title,path=getwd(),dd=NULL,lazy.input=TRUE)
{#open a document
#++++++++++++++++++++++++++++++++++++
  if(lazy.input){
    title=dclong.String::symbolToString(substitute(title))
    path=dclong.String::symbolToString(substitute(path))
  }
  if(!is.null(dd)){
    path=getDirDef(dd)
  }
  openf(title=title,extension='.log',path=path,lazy.input=FALSE)
}

openf.eps <-
function(title,path=getwd(),dd=NULL,lazy.input=TRUE)
{
  if(lazy.input){
    title=dclong.String::symbolToString(substitute(title))
    path=dclong.String::symbolToString(substitute(path))
  }
  if(!is.null(dd)){
    path=getDirDef(dd)
  }
  openf(title=title,extension='.eps',path=path,lazy.input=FALSE)
}

openf.dvi <-
function(title,path=getwd(),dd=NULL,lazy.input=TRUE)
{
  if(lazy.input){
    title=dclong.String::symbolToString(substitute(title))
    path=dclong.String::symbolToString(substitute(path))
  }
  if(!is.null(dd)){
    path=getDirDef(dd)
  }
  openf(title=title,extension='.dvi',path=path,lazy.input=FALSE)
}

openf.docx <-
function(title,path=getwd(),dd=NULL,lazy.input=TRUE)
{
  if(lazy.input){
    title=dclong.String::symbolToString(substitute(title))
    path=dclong.String::symbolToString(substitute(path))
  }
  if(!is.null(dd)){
    path=getDirDef(dd)
  }
  openf(title=title,extension='.docx',path=path,lazy.input=FALSE)
}

openf.doc <-
function(title,path=getwd(),dd=NULL,lazy.input=TRUE)
{
  if(lazy.input){
    title=dclong.String::symbolToString(substitute(title))
    path=dclong.String::symbolToString(substitute(path))
  }
  if(!is.null(dd)){
    path=getDirDef(dd)
  }
  openf(title=title,extension='.doc',path=path,lazy.input=FALSE)
}

openf.csv <-
function(title,path=getwd(),dd=NULL,lazy.input=TRUE)
{
  if(lazy.input){
    title=dclong.String::symbolToString(substitute(title))
    path=dclong.String::symbolToString(substitute(path))
  }
  if(!is.null(dd)){
    path=getDirDef(dd)
  }
  openf(title=title,extension='.csv',path=path,lazy.input=FALSE)
}



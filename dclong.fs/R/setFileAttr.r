#' File and Directory Manipulation.
#' Set attributes (read-only, hidden and etc) of files.
#' @param title a character vector specifies the names of files.
#' @param file full path of files.
#' @param path path of a directory.
#' @param hide logical; should hide or not.
#' @param read.only logical; should be read only or not.
#' @param archive logical; should be archive or not.
#' @param system logical; should be system file or not.
#' @param index.not logical; should be not content indexed or not.
#' @param lazy.input logical; If true, then simple string (without white space and other special characters) can be used without double or single quotation.
#' @author Chuanlong Benjamin Du
#' @note This functions only works in Windows system currently.
#' @keywords set file attribute
#' @examples
#' #create a temp file
#' fileName = "TEMP_TEMP.TXT"
#' file.create(fileName)
#' #set attributes for the temp file
#' setFileAttr(fileName,hide=TRUE)
#' @export setFileAttr
setFileAttr <-
function(title,path=getwd(),file=NULL,hide=NA,read.only=NA,archive=NA,system=NA,index.not=NA,lazy.input=FALSE)
{
  if(lazy.input){
    if(is.null(file)){
      title=dclong.String::symbolToString(substitute(title))
      path=dclong.String::symbolToString(substitute(path))
      file=dclong.String::path.comb(path,title)
    }
    else{
      file=dclong.String::symbolToString(substitute(file))
    }
  }
  if(.Platform$OS.type=="windows"){
    command='attrib '
    if(!is.na(hide))
      if(hide)
        command=paste(command,'+h ',sep="")
      else
        command=paste(command,'-h ',sep="")
    if(!is.na(read.only))
      if(read.only)
        command=paste(command,'+r ',sep="")
      else
        command=paste(command,'-r ',sep="")
    if(!is.na(archive))
      if(archive)
        command=paste(command,'+a ',sep="")
      else
        command=paste(command,'-a ',sep="")
    if(!is.na(system))
      if(system)
        command=paste(command,'+s ',sep="")
      else
        command=paste(command,'-s ',sep="")
    if(!is.na(index.not))
      if(index.not)
        command=paste(command,'+i ',sep="")
      else
        command=paste(command,'-i ',sep="")
    for(i in file)
    {
      i=gsub(pattern="/$",replacement="",x=i)
      i=gsub(pattern="\\\\$",replacement="",x=i)
      shell(paste(command,'\"',i,'\"',sep=""))
    }
  }
  else{#linux operating system
  
  }
}

#' File and Directory Manipulation.
#' Search for files whose content match the specified pattern. It's useful in many cases, for example it can help you to check which functions in a package calls a help method.
#' @aliases searchFileContent searchfc
#' @usage searchFileContent(cpattern,fpattern=NULL,flist=NULL,file.names=TRUE,cfixed=FALSE,cignore.case=FALSE,fignore.case=TRUE,path=getwd(),dd=NULL,lazy.input=TRUE)
#' searchfc(cpattern,fpattern=NULL,flist=NULL,file.names=TRUE,cfixed=FALSE,cignore.case=FALSE,fignore.case=TRUE,path=getwd(),dd=NULL,lazy.input=TRUE)
#' @param cpattern the pattern of the content to be find
#' @param fpattern the pattern of the files to be searched in.
#' @param flist a character vector of files to be searched in.
#' @param file.names logical. If TRUE then the name of the files containing the specified pattern is returned, otherwise a logical vector is returned.
#' @param cfixed logical. If TRUE, cpattern is a string to be matched as is. Overrides all conflicting arguments.
#' @param cignore.case logical; If FALSE, the pattern matching is case sensitive and if TRUE, case is ignored during matching.
#' @param fignore.case logical; If FALSE, the pattern matching is case sensitive and if TRUE, case is ignored during matching.
#' @param path the path of a directory.
#' @param dd index of a default path (refer to \code{\link{showDirDef}}).
#' @param lazy.input logical; If true, then simple string (without white space and other special characters) can be used without double or single quotation.
#' @return the path (either short or long) of the files match the given pattern.
#' @author Chuanlong Benjamin Du
#' @seealso \code{\link{changefc}}, \code{\link{changedn}}.
#' @keywords search file content
#' @examples
#' #create a temp file
#' fileName = "TEMP_TEMP.TXT"
#' file.create(fileName)
#' cat("PATTERN_MATCH DFJAJ;F\n",file=fileName)
#' #search the given pattern in the temp file
#' searchfc(cpattern="^PATTERN_MATCH",flist=dir.txt())
#' @export searchFileContent searchfc

searchFileContent=function(cpattern,fpattern=NULL,flist=NULL,file.names=TRUE,
          cfixed=FALSE,cignore.case=FALSE,fignore.case=TRUE,path=getwd(),
                                                      dd=NULL,lazy.input=TRUE){
  if(lazy.input){
    cpattern=dclong.String::symbolToString(substitute(cpattern))
    fpattern=dclong.String::symbolToString(substitute(fpattern))
    flist=dclong.String::symbolToString(substitute(flist))
  }
  if(!is.null(dd)){
    path=getDirDef(dd)
  }
  #get the files to be changed
  if(!is.null(fpattern) && length(fpattern)){
    files=dir(path=path,pattern=fpattern,full.names=TRUE,ignore.case=fignore.case)
  }
  else{
    files=NULL
  }
  files=c(files,flist)
  files=files[is.file(files,FALSE)]
  result=NULL
  for(file in files){
    fcontent=readText(file=file,lazy.input=FALSE)
    result=c(result,grepl(pattern=cpattern,x=fcontent,
                        ignore.case=cignore.case,fixed=cfixed))
  }
  if(file.names){
    result=files[result]
  }
  return(result)
}

searchfc = searchFileContent

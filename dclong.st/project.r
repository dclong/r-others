

#' Matrices
#' returns projection matrix of a design matrix. %% ~~ A concise (1-5 lines)
#' description of what the function does. ~~
#'
#' %% ~~ If necessary, more details than the description above ~~
#'
#' @param x %% ~~Describe \code{x} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords projection matrix
#' @examples
#'
#' #generate a matrix
#' A=matrix(rnorm(12),nrow=3)
#' #calculate the projection matrix
#' project(A)
#'
project <-
function(x)
{#This function is used to calculate the projection matrix.
#Paramter x is the designmatrix.
#Be careful with the form of the matrix when you use this function!
  x=as.matrix(x)
  xtran=t(x)
  temp=xtran%*%x
  n=nrow(temp)
  if(qr(temp)$rank==n)
    return(x%*%solve(temp)%*%xtran)
  library(MASS)
    return(x%*%ginv(temp)%*%xtran)
}

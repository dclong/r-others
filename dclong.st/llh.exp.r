
#' Log Likelihood Function
#' log likelihood function of exponential distribution. %% ~~ A concise (1-5
#' lines) description of what the function does. ~~
#'
#' %% ~~ If necessary, more details than the description above ~~
#'
#' @param lambda %% ~~Describe \code{lambda} here~~
#' @param data %% ~~Describe \code{data} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords exponential likelihood
#' @examples
#'
#'
llh.exp=function(lambda,data)
{
  n=length(data)
  n*log(lambda)-lambda*sum(data)
}


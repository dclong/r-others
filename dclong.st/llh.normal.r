
#' Log Likelihood Function
#' log likelihood function of normal distribution. %% ~~ A concise (1-5 lines)
#' description of what the function does. ~~
#'
#' %% ~~ If necessary, more details than the description above ~~
#'
#' @param mu %% ~~Describe \code{mu} here~~
#' @param sigma %% ~~Describe \code{sigma} here~~
#' @param data %% ~~Describe \code{data} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords normal likelihood
#' @examples
#'
#'
llh.normal=function(mu,sigma,data=numeric(0))
{#This function calculate the likelihood function of normal sampling data model
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Parameter v is the variance of the normal distribution
#-------------------------------------------------------
#Parameter mu is the mean of the normal distribution
#--------------------------------------------------------
#Parameter y is the observation vector
#---------------------------------------------------------
  n=length(data)
  nmu=length(mu)
  nsigma=length(sigma)
  if(nmu>nsigma)
    if(nmu%%nsigma)
      stop("long parameter length is not multiple of the shorter parameter length.")
    else
      sigma=rep(sigma,times=nmu/nsigma)
  else
    if(nsigma>nmu)
      if(nsigma%%nmu)
        stop("long parameter length is not multiple of the shorter parameter length.")
      else
        mu=rep(mu,nsigma/nmu)
  #-----------------------------------------------------------
  -n*log(sqrt(2*pi)*sigma)-rowSums(outer(mu,data,'-')^2)/2/sigma^2
}

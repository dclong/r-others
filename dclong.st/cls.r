
#' Fitting Linear Models
#' fit linear models with constraint. %% ~~ A concise (1-5 lines) description
#' of what the function does. ~~
#'
#' %% ~~ If necessary, more details than the description above ~~
#'
#' @param X design matrix. %% ~~Describe \code{X} here~~
#' @param Y observation vector. %% ~~Describe \code{Y} here~~
#' @param ConMat contant matrix (vector) %% ~~Describe \code{ConMat} here~~
#' @param d the value of CBeta to be tested. %% ~~Describe \code{d} here~~
#' @param alpha significant level. %% ~~Describe \code{alpha} here~~
#' @param PreExtra extra component of variance. %% ~~Describe \code{PreExtra}
#'   here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords linear model
#' @examples
#'
#'
cls=function(X,Y,ConMat=0,d=0,alpha=0.05,PreExtra=0)
{#This program is used to calculate almost all the things that we are interested in constraint least square models.
#Parameter X is the design matrix
#Parameter Y is the response variable
#Parameter ConMat is the constant matrix or vector
#Parameter is in the expression H0: Cbeta=d, where C is the constant matrix
#parameter alpha is significant level
#Parameter PreExtra is the extra part of variance when we do the prediction
#                     (PreExtra+a%*%t(a))*MSE
###############################################################################################################
#improvement: consider using the output of linear model or formular
####################################################################
  #number of observations:
  n=nrow(X)
  #number of variables:
  p=ncol(X)
  #degrees of freedom of MSE:
  degree=n-qr(X)$rank
  #Projection matrix:
  library(MASS)
  Px=X%*%ginv(t(X)%*%X)%*%t(X)
  Yhat=Px%*%Y
  Ehat=Y-Yhat
  RSS=t(Ehat)%*%Ehat
  Bhat=ginv(t(X)%*%X)%*%t(X)%*%Y
  #--------------------------------------------------------------------------------
  MSE=RSS/degree
  Sigsqlimit.upper=RSS/qchisq(alpha/2,degree)
  Sigsqlimit.lower=RSS/qchisq(alpha/2,degree,lower.tail=FALSE)
  Sigmalimit.upper=sqrt(Sigsqlimit.upper)
  Sigmalimit.lower=sqrt(Sigsqlimit.lower)
  result=list(SigsqHat=MSE,SigsqL=Sigsqlimit.lower,SigsqU=Sigsqlimit.upper,SigmaHat=sqrt(MSE),SigmaL=Sigmalimit.lower,SigmaU=Sigmalimit.upper)
  #if ConMat is given, then calculate CBeta and its confidence interval
  if(any(as.logical(ConMat)))
  {
    CBhat=ConMat%*%Bhat
################The following code deal with the case when ConMat is vector####################
    if(is.vector(ConMat)|nrow(as.matrix(ConMat))==1)
    {
        #construct confidence interval for CBhat
        a=ConMat%*%ginv(t(X)%*%X)%*%t(X)
        #calculate the standard error of the estimation
        se=sqrt(a%*%t(a)*MSE)
        IntWid=qt(alpha/2,degree,lower.tail=FALSE)*se
        CBlimit.upper=CBhat+IntWid
        CBlimit.lower=CBhat-IntWid
        #calculate the P-value for the null hypothesis CBeta=d:
        StdCBhat=(CBhat-d)/se
        Pvalue=pt(abs(StdCBhat),degree,lower.tail=FALSE)*2
        #--------if PreExtra is given a non-zero value then do the prediction-------------------
        if(PreExtra)
        {
            pse=sqrt((PreExtra+a%*%t(a))*MSE)
            PreIntWid=qt(alpha/2,degree,lower.tail=FALSE)*pse
            Prelimit.upper=CBhat+PreIntWid
            Prelimit.lower=CBhat-PreIntWid
            result=list(result,CBhat=CBhat,se=se,CBL=CBlimit.lower,CBU=CBlimit.upper,StdCBhat=StdCBhat,Pvalue=Pvalue,pse,PreL=Prelimit.lower,PreU=Prelimit.upper)
            return(result)
        }
        result=list(result,CBhat=CBhat,se=se,CBL=CBlimit.lower,CBU=CBlimit.upper,StdCBhat=StdCBhat,Pvalue=Pvalue)
        return(result)
    }
############The following code deal with the case when ConMat is not a vector but a matrix#########
    cova=as.vector(MSE)*(ConMat%*%ginv(t(X)%*%X)%*%t(ConMat))
    #degrees of freedom of the numerator:
    m=nrow(ConMat)
    #value of F statistics:
    Fstat=t(CBhat-d)%*%solve(ConMat%*%ginv(t(X)%*%X)%*%t(ConMat))%*%(CBhat-d)/m/MSE
    #p-value:
    Pvalue=pf(Fstat,m,degree,lower.tail=FALSE)
    result=list(result,CBhat=CBhat,cov=cova,Fstat=Fstat,Pvalue=Pvalue)
    return(result)
  }
  return(result)
}


#' Log Likelihood Function
#' log likelihood function of Cauchy distribution. %% ~~ A concise (1-5 lines)
#' description of what the function does. ~~
#'
#' %% ~~ If necessary, more details than the description above ~~
#'
#' @param location %% ~~Describe \code{location} here~~
#' @param scale %% ~~Describe \code{scale} here~~
#' @param data %% ~~Describe \code{data} here~~
#' @param gr %% ~~Describe \code{gr} here~~
#' @param hessian %% ~~Describe \code{hessian} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords cauchy likelihood
#' @examples
#'
#'
llh.cauchy=function(location,scale,data=numeric(0),gr=FALSE,hessian=FALSE)
{#log likelihood function of cauchy distribution
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#argument location and scale are location and scale parameters for Cauchy distribution
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#argument data is the observation vector
#-----------------------------------------------------------------------
  n=length(data)
  nloc=length(location)
  nscale=length(scale)
  if(nloc>nscale)
  {
    reminder=nloc%%nscale
    if(reminder)
      stop("longer parameter length is not a multiple of shorter parameter length.")
    else
      scale=rep(scale,times=nloc/nscale)
  }
  else
    if(nscale>nloc)
    {
      reminder=nscale%%nloc
      if(reminder)
        stop("longer paramter length is not a multiple of shorter parameter length.")
      else
        nloc=rep(nloc,times=nscale/nloc)
    }
  if(gr)
  {
    tmp=outer(location,data,'-')/scale
    gr1=2*tmp/(1+tmp^2)
    gr2=(rowSums(gr1*tmp)-n)/scale
    gr1=rowSums(gr1)
    return(cbind(gr1,gr2))
  }
  if(hessian)
  {

  }
  -n*log(pi*scale)-rowSums(log((outer(location,data,'-')/scale)^2+1))
}



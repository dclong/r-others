

#' Statistics Distributions
#' empirical distribution %% ~~ A concise (1-5 lines) description of what the
#' function does. ~~
#'
#' %% ~~ If necessary, more details than the description above ~~
#'
#' @param x values of vector at which the empirical distribution are to be
#'   calculated %% ~~Describe \code{x} here~~
#' @param dat observed data %% ~~Describe \code{dat} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords empirical distribution
#' @examples
#'
#' #empirical distribution
#' empir(rnorm(1),rnorm(5))
#'
empir <-
function(x,dat)
{#This function calcualte the empirical distribution
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Arguemnt x is the independent variable
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Arguemnt dat is the data vector
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  colMeans(outer(dat,x,'<='))
}



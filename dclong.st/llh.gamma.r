

#' Log Likelihood Function
#' log likelihood function of gamma distribution. %% ~~ A concise (1-5 lines)
#' description of what the function does. ~~
#'
#' %% ~~ If necessary, more details than the description above ~~
#'
#' @param shape %% ~~Describe \code{shape} here~~
#' @param scale %% ~~Describe \code{scale} here~~
#' @param dat %% ~~Describe \code{dat} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords gamma likelihood
#' @examples
#'
#'
llh.gamma=function(shape,scale,dat)
{#This is the log likelihood function of Gamma distribution
#Parameter a (alpha) and b (beta) are the parameter of Gamma distribution
#Parameter x is the data vector
#-------------------------------------------------------------------------------
  n=length(data)
  nshape=length(shape)
  nscale=length(scale)
  if(nshape>nscale)
    if(nshape%%nscale)
      stop("longer parameter length is not a multiple of the shorter parameter length.")
    else
      scale=rep(scale,times=nshape/nscale)
  else
    if(nscale>nshape)
      if(nscale%%nshape)
        stop("longer parameter length is not a multiple of the shorter parameter length.")
      else
        shape=rep(shape,nscale/nshape)
  -n*log(gamma(shape))-n*shape*log(scale)+(shape-1)*sum(log(data))-sum(data)/scale
}





#' Linear Models
#' check whether a linear combination of parameters is testable. %% ~~ A
#' concise (1-5 lines) description of what the function does. ~~
#'
#' %% ~~ If necessary, more details than the description above ~~
#'
#' @param x %% ~~Describe \code{x} here~~
#' @param conmat %% ~~Describe \code{conmat} here~~
#' @param roundn %% ~~Describe \code{roundn} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords linear model test
#' @examples
#'
#'
#'
is.testable <-
function(x,conmat,roundn=10)
{#This function is used to check whether t(a)%*%beta is estimable under the design matrix x
#Parameter x is the design matrix
#Parameter a is the coefficient of beta
#Parameter roundn is the number of digits kept when we round the value
#The result will be consisted of three part.
#(1)
#The first part is a vector which is based on to check whether the combination is estimable
#If it's zero vector then the combination is estimable, otherwise the combination is unestimable
#(2)
#The second part is a vector giving the value of t(a)%*%ginv(t(x)%*%x)%*%t(x), which mutiplied
#by Y produces the ordinary least squares estimate of t(a)%*%beta
#(3)
#The third part is a logic value which tell whether the combination is estimable.
  if(is.matrix(conmat))
  {
    if(qr(conmat)$rank<nrow(conmat))
      stop("the rows of conmat must be independent.")
  }
  library(MASS)#!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  Ptx=project(t(x))
  Iden=diag(rep(1,ncol(x)))
  CPtx=conmat%*%(Iden-Ptx)
  coefy=conmat%*%ginv(t(x)%*%x)%*%t(x)
  testable=all(round(CPtx,roundn)==0)
  return(list(CPtx=CPtx,coefy=coefy,testable=testable))
}#need to be modified to work better!



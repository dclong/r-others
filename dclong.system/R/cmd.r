
#' R System and Operating System Related
#' Open the command prompt in Windows system.
#' @param path the default path of cmd prompt.
#' @author Chuanlong Benjamin Du
#' @seealso \code{shell.exec} and \code{system}.
#' @keywords command cmd prompt
#' @examples
#' cmd()
#' 
cmd=function(path=getwd())
{
  Rhome=Sys.getenv('R_HOME')
  Sys.setenv('R_HOME' = path)
  shell.exec('cmd.exe')
  Sys.setenv('R_HOME' = Rhome)
}


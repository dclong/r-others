#' A simple wrap of the shutdown command. For a more powerful use, you can DOS/Linux command by yourself using function \code{\link[base]{shell}}.

#' @param hour waiting time before shutdown in hours.
#' @param minute waiting time before shutdown in minutes.
#' @param second waiting time before shutdown in seconds.
shutdown = function(hour,minute,second){
    if(.Platform$OS.type=="windows"){
        sec = 0
        if(!missing(hour)){
            sec = sec + hour*3600
        }
        if(!missing(minute)){
            sec = sec + minute*60
        }
        if(!missing(second)){
            sec = sec + second
        }
        command = paste("shutdown/s /t",sec)
    }else{
        minu = 0
        if(!missing(hour)){
            minu = minu + hour*60
        }
        if(!missing(minute)){
            minu = minu + minute
        }
        if(!missing(second)){
            minu = minu + second/60
        }
        command = paste("shutdown -h", minu)
    }
    shell(command)
}


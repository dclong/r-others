
#' R System and Operating System Related
#' Check whether current operating system is Windows or not.
#' @return logical; If the current operating system is Windows
#' then TRUE, vice versa.
#' @author Chuanlong Benjamin Du
#' @seealso \code{\link{is.Linux}}.
#' @keywords system OS Windows
#' @examples
#' is.Windows()
#'
is.Windows=function()
{
  .Platform$OS.type=="windows"
}


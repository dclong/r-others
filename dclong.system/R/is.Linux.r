
#' R System and Operating System Related
#' Check whether current Operating System is Linux or not.
#' @return logical; If the current operating system is Linux then TRUE,
#' vice versa.
#' @author Chuanlong Benjamin Du
#' @seealso \code{\link{is.Windows}}.
#' @keywords system OS Linux
#' @examples
#' is.Linux()
#'
is.Linux=function()
{
  .Platform$OS.type=="linux"
}
RandTest1way<-function(dat,trtns,M){
#compute randomization test for one-way anova
#layout
#
#trtns is vector of treatment group sizes
#(i.e., n_1, n_2, . . ., n_k) for k trts
#dat is vector of data in actual order
#so that 1:n_1 are treatment 1, etc.
#M is number of random permutations to use
#
n<-length(dat)
k=length(trtns)
#Check whether the parameters consist with each other
if(n!=sum(trtns))
   stop("number of observations does not match trt sizes")
#statistics vector
Tstat=NULL
for(i in 1:M)
{
   #do a permutation
   permutation=sample(dat,n,replace=F)
   Tstat=c(Tstat,StatCompute(permutation,trtns,k))
}
Treal=StatCompute(dat,trtns,k)
return(mean(Tstat>=Treal))
}

StatCompute=function(permutation,trtns,k)
{#This function is used to calculate the statistics (4.2) in the text book
#Parameter dat is the data group followed by group
#Parameter trtns is the number of units assigned to the treatments
#Parameter k is the number of treatments, i.e. the length of vector trtns
#-------------------------------------------------------------------------------
#index of the first observation of current group
BeginIndex=1
#initial the statistics
Tstar=0
for(j in 1:k)
{
   #number of units assigned to the jth treatment
   TrtjNum=trtns[j]
   #index of the first observation of the next group
   NextBeginIndex=BeginIndex+TrtjNum
   #update the statistics
   Tstar=Tstar+sum(permutation[BeginIndex:(NextBeginIndex-1)])^2/TrtjNum
   #update the index
   BeginIndex=NextBeginIndex
}
return(Tstar)
}

Evaluate=function(dat,trtns,M,Width=0.001,N=100000)
{#This function uses monte carlo method to decide an apropriate permutation sample size
#
#trtns is vector of treatment group sizes
#(i.e., n_1, n_2, . . ., n_k) for k trts
#dat is vector of data in actual order
#so that 1:n_1 are treatment 1, etc.
#M is number of random permutations to use
#
Pvalue=NULL
for(i in 1:N)
   Pvalue=c(Pvalue,RandTest1way(dat,trtns,M))
PvalEstimate=as.vector(quantile(Pvalue,prob=0.5))
delta=Width/2
Prob=mean(abs(Pvalue-PvalEstimate)<=delta)
return(data.frame(PermutationSize=M,PvalEstimate=PvalEstimate,IntervalWidth=Width,Prob))
}


Evaluate2=function(dat,trtns,M,Width=0.001,N=10000)
{#This function use monte carlo method to find the variance to the p-values, thus to decide the appropriate permutation sample size
#
Pvalue=NULL
for(i in 1:N)
   Pvalue=c(Pvalue,RandTest1way(dat,trtns,M))
Pval.sd=sd(Pvalue)
return(data.frame(PermutationSize=M,IntervalWidth=Width,PvalueSd=Pval.sd))
}
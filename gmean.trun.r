#' This functions finds the truncated group mean of students.
#' @param file the file (spreedsheet file) containing the students' grades.
#' @param output the file used to store the result.
#' @param score.header the header of the column containing the score to be analyzed.
#' @param group.header the header of the column cotaining group information of students.
#' @param drop.number the number of lowest scores to be dropped for each group.
#' @param group.only logical; if true, then only return result for each group, otherwise return result for each student.
#' @param score.range the range of grades, which helps to check whether there's mistake in the grades file.
#' @param header logical; whether to read  the first row of the file as headers.
#' @param sheet.index the index of the sheet to be read in.

gmean.trun=function(file,output=file,score.header="exam",group.header="Company",drop.number=3,group.only=TRUE,score.range=c(0,100),header=TRUE,sheet.index=1)
{
	#read in grades file
  file.extension=tolower(file.ext(file))
  if(file.extension==".xls")
  {
    library(xlsReadWrite)
    read.xls(file=file,colNames=header,sheet=sheet.index)->grades 
  }
  else
    if(file.extension==".xlsx")
    {
      library(xlsx)
      read.xlsx(file=file,sheetindex=sheet.index,header=header)->grades
    }
    else
      if(file.extension==".csv")
        grades=read.csv(file=file,header=header)
      else
        stop("an Excel document is required.")
	#replace missing grades with 0
	grades[,score.header][is.na(grades[,score.header])]=0
	#check whether all grades are satisfied
	grades[,score.header]>=min(score.range)&grades[,score.header]<=max(score.range)->check
	if(!all(check))
	{
		check.which=which(check)
		if(length(check.which)>1)
			stop("NO. ", check.which, "are not in the specified ranged.")
		else
			if(length(check.which)==1)
				stop("NO. ", check.which, "is not in the specified range.")
	}
	#format group
	grades[,group.header]=toupper(trim(grades[,group.header]))
	#calculate truncated mean
	if(drop.number>0)
    mean.trun=tapply(grades[,score.header],INDEX=grades[,group.header],FUN=function(x){mean(sort(x)[-(1:drop.number)])})
  else
    mean.trun=tapply(grades[,score.header],INDEX=grades[,group.header],FUN=mean)
	if(group.only)
    result=mean.trun
  else
  {
    #update the orginal data
    apply(outer(grades[,group.header],names(mean.trun),"=="),1,which)->mean.index
	  grades[,score.header]=mean.trun[mean.index]
	  result=grades
  }
  #write the result into the specified file
  output.extension=tolower(file.ext(output))
  if(output.extension==".csv")
      write.csv(result,file=output)
  else
      if(output.extension==".xls")
      {
        library(xlsReadWrite)
        write.xls(result,file=output)
      }
      else
        if(output.extension==".xlsx")
        {
          library(xlsx)
          write.xlsx(result,file=output)
        }
        else
          if(output.extension==".txt" || output.extension=="")
            write.table(result,file=output)
          else
            stop("the specified file type is not supported.")
}
RandTestSysPval=function(dat,trtns)
{#This funcitons is used to calcualte the systematic pvalue of random permutation test
#Parameter dat is the data in order
#Parameter trtns is the number of experiment units in each treatment gourp
n=length(dat)
if(sum(trtns)!=n)
   stop("parameters conflict!")
Tstat=StatCompute(dat,trtns)
combs=combsng(n,trtns,0)
pers=matrix(dat[combs],ncol=n)
Tstar=apply(pers,1,StatCompute,trtns=trtns)
return(mean(Tstar>=Tstat))
}


StatCompute=function(permutation,trtns)
{#This function is used to calculate the statistics (4.2) in the text book
#Parameter dat is the data group followed by group
#Parameter trtns is the number of units assigned to the treatments
#Parameter k is the number of treatments, i.e. the length of vector trtns
#-------------------------------------------------------------------------------
#index of the first observation of current group
k=length(trtns)
BeginIndex=1
#initial the statistics
Tstar=0
for(j in 1:k)
{
   #number of units assigned to the jth treatment
   TrtjNum=trtns[j]
   #index of the first observation of the next group
   NextBeginIndex=BeginIndex+TrtjNum
   #update the statistics
   Tstar=Tstar+sum(permutation[BeginIndex:(NextBeginIndex-1)])^2/TrtjNum
   #update the index
   BeginIndex=NextBeginIndex
}
return(Tstar)
}


combsng=function(n,gns,k)
{#This function is use to find the combinations of n choose n1, n2, ..., nt
#Parameter gns (group numbers) is a non-negative verctor (n1,n2,..., nt), the sum of which is less or equal to n
#Parameter k decides the result returned
#         if k is betwwen 1 and n choose n1, n2, ..., nt then return the kth nature combination
#         else return all the combinations in order
#-----------------------------------------------------------------------------------------------------
if(any(gns<0))
   stop("positve elements are required for parameter gns")
GnsSum=sum(gns)
if(n<GnsSum)
   stop("the sum of parameter gns shouldn't exceed parameter n")
if(n>GnsSum)
   gns=c(gns,n-GnsSum)
IndiZero=(gns==0)
if(any(IndiZero))
   return(n,gns[!IndiZero],k)
CombsNum=factorial(n)/prod(factorial(gns))
if(k>0&&k<=CombsNum)
{#find the kth nature combination
   if(length(gns)==2)
   {
      result=combs2g(n,gns[1],k)
      result=c(result,(1:n)[-result])
      return(result)
   }
   NewN=n-gns[1]
   NewGns=gns[-1]
   SubCombsNum=factorial(NewN)/prod(factorial(NewGns))
   quotient=floor(k/SubCombsNum)
   remainder=k-quotient*SubCombsNum
   if(remainder==0)
   {
      result=combs2g(n,gns[1],quotient)
      result=c(result,((1:n)[-result])[combsng(NewN,NewGns,SubCombsNum)])
      return(result)
   }
   result=combs2g(n,gns[1],quotient+1)
   result=c(result,((1:n)[-result])[combsng(NewN,NewGns,remainder)])
   return(result)
}
#find all the combinations in order
count=choose(n,gns[1])
result=NULL
if(length(gns)==2)
{
   for(i in 1:count)
   {
      TempResult=combs2g(n,gns[1],i)
      result=rbind(result,c(TempResult,(1:n)[-TempResult]))
   }
   return(result)
}
for(i in 1:count)
{
   NewN=n-gns[1]
   NewGns=gns[-1]
   SubCombsNum=factorial(NewN)/prod(factorial(NewGns))
   TempResult=combs2g(n,gns[1],i)
   TempMatrix=matrix(TempResult,byrow=T,ncol=gns[1],nrow=SubCombsNum)
   TempResult=matrix(((1:n)[-TempResult])[combsng(NewN,NewGns,0)],nrow=SubCombsNum)
   TempMatrix=cbind(TempMatrix,TempResult)
   result=rbind(result,TempMatrix)
}
return(result)
}


combs2g=function(n,m,k)
{#This function is use to find the combinations of n choose m
#Parameter m should be a positive integer, which is less or equal to n
#Parameter k decides the result return
#          if k is between 1 and n choose m then return the kth nature combination
#          else return all combinations in order
if(n<m)
   stop("parameter n should not be less than m")
if(m<=0)
   stop("positive value is required for parameter m")
Total=choose(n,m)
if(k>0&&k<=Total)
{#find the kth nature combination
   if(m==1)
      return(k)
   summation=0
   flag=0
   while(summation<k)
   {
      flag=flag+1
      summation=summation+choose(n-flag,m-1)
   }
   return(c(flag,combs2g(n-flag,m-1,k-summation+choose(n-flag,m-1))+flag))
}
#find all the combinations in order
result=NULL
for(i in 1:Total)
   result=rbind(result,combs2g(n,m,i))
return(result)
}


#' Convert an object to LaTeX and Related Utilities.
#' Convert an matrix in R to an array in latex code.
#' @param mat matrix to be converted
#' @param bracket bracket to be used
#' @param alignment alignment of the array in latex
#' @param append logical; If true, then the latex code will be appended to the specified file, otherwise the file will rewritten.
#' @param output name of the output tex file. If NULL then the latex code  is printed to R console.
#' @param lazy.input logical; If true, then simple string (without white space and other special characters) can be used without double or single quotation.
#' @author Chuanlong Benjamin Du
#' @seealso \code{\link[Hmisc]{latex}}
#' @keywords tex array matrix
#' @examples
#' #create a matrix
#' mat=matrix(rnorm(9),nrow=3)
#' #get the latex code for matrix mat
#' #matrixToTex(mat,file=NULL)
#' @export matrixToTex
matrixToTex <-
function(mat,bracket="[]",alignment="r",output=NULL,append=FALSE,lazy.input=TRUE)
{
  if(lazy.input){
    bracket=dclong.String::symbolToString(substitute(bracket))
    alignment=dclong.String::symbolToString(substitute(alignment))
    output=dclong.String::symbolToString(substitute(output))
    if(length(output)==0){
      output=NULL
    }
  }
  if(is.vector(mat))
    mat=t(mat)
  RowNum=nrow(mat)
  ColNum=ncol(mat)
  string=paste("\\left",substr(bracket,1,1),sep="")
  string=paste(string,"\n\\begin{array}{",sep="")
  string=paste(string,paste(rep(alignment,ColNum),collapse=""),sep="")
  string=paste(string,"}\n",sep="")
  for(i in 1:RowNum)
  {
    string=paste(string,mat[i,1],sep="")
    if(ColNum>1)
    {
      for(j in 2:ColNum)
      {
        string=paste(string,"&",sep=" ")
        string=paste(string,mat[i,j],sep=" ")
      }
    }
    string=paste(string,"\n",sep="\\\\")
  }
  string=paste(string,"\\end{array}",sep="")
  string=paste(string,"\n",sep="")
  string=paste(string,"\\right",sep="")
  string=paste(string,substr(bracket,2,2),sep="")
  #write the string into a file
  if(is.null(output))
    #print the code to standard output
    cat(paste(string,"\n",sep=""))
  else
    cat(string, file = output,append =FALSE)
}
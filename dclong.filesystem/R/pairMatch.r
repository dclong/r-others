#' File and Directory Manipulation.

#' Search for and replace a given pair match pattern in files.
#'
#' Function pairMatchSeFile can search for a given pair match pattern in a bunch of files. Function pairMatchChFile can search for a given pair match pattern and replace the matches with a strings. To replace the matches, you can either specify strings for replacement or a function which take in a string as argument and returns a string.
#' @aliases pairMatchSeFile pairMatchChFile
#' @usage pairMatchSeFile(left,right,title,path=getwd(),file=NULL,start=1,fixed=FALSE,lazy.input=TRUE)
#' pairMatchChFile(left,right,replacement,title,path=getwd(),file=NULL,start=1,all=TRUE,fixed=FALSE,interact=TRUE,lazy.input=TRUE)
#' @param left a single character which indicate the left delimiter.
#' @param right a single character which indicate the right delimiter.
#' @param replacement a replacement for the substrings which match the specified pattern. It can be either a string or a function that can be applied on the the substring to be replaced and return a new string.
#' @param title the title of the file to be read in.
#' @param path the parent directory of the file.
#' @param file path (either short or long) of the file.
#' @param start If it's a character or string or a regular expression pattern, then start serach from this character or string or the string that matches the pattern; if it's an integer, then start search from index \code{start}.
#' @param all logical; If TRUE, then all occurrences that match the specified pattern will be replaced, o.w. only the first occurrence will be replaced.
#' @param fixed logical. If TRUE and start is string or regular expression pattern, start is a string to be matched as is. Overrides all conflicting arguments.
#' @param interact logical; Indicate whether allow user interaction while changing file content, file names or directory names. If TRUE, then user's interaction is required.
#' @param lazy.input logical; If true, then simple string (without white spaceand other special characters) can be used without double or single quotation.
#' @author Chuanlong Benjamin Du
#' @seealso \code{\link[dclong.String]{pairMatch}}.
#' @examples
#' #create a file
#' fileName = "Temp_temp.txt"
#' file.create(fileName)
#' cat("abcdjfa",file=fileName)
#' pairMatchSeFile('a','a',title=fileName,lazy.input=FALSE)
#' @export pairMatchSeFile pairMatchChFile
#' @importFrom dclong.String pairMatch pairMatchSub strReplace
pairMatchSeFile = function(left,right,title,path=getwd(),
              file=NULL,start=1,fixed=FALSE,lazy.input=TRUE){
  if(lazy.input){
    if(is.null(file)){
      title=dclong.String::symbolToString(substitute(title))
      path=dclong.String::symbolToString(substitute(path))
      file=combinePath(path,title)
    }
    else{
      file=dclong.String::symbolToString(substitute(file))
    }
  }else{
    if(is.null(file)){
      file = combinePath(path,title)
    }
  }
  readText(file=file,lazy.input=FALSE)->fileContent
  dclong.String::pairMatch(left=left,right=right,x=fileContent,start=start,value=TRUE,fixed=fixed)
}

pairMatchChFile = function(left,right,replacement,title,path=getwd(),file=NULL,
              start=1,all=TRUE,fixed=FALSE,interact=TRUE,lazy.input=TRUE){
  if(lazy.input){
    if(is.null(file)){
      title=dclong.String::symbolToString(substitute(title))
      path=dclong.String::symbolToString(substitute(path))
      file=combinePath(path,title)
    }
    else{
      file=dclong.String::symbolToString(substitute(file))
    }
  }else{
    if(is.null(file)){
      file = combinePath(path,title)
    }
  }
  for(f in file){
    #read content of file 
    fileContent = readText(file=f,lazy.input=FALSE)
    if(interact){
      cat(paste("File ",f," in processing ...\n",sep=""))
      oldContentIndex=dclong.String::pairMatch(left=left,right=right,
                    x=fileContent,start=start,value=FALSE,fixed=FALSE)
      if(!is.null(oldContentIndex)){
        repNumber = nrow(oldContentIndex)
        replaceTable = matrix("",nrow=repNumber,ncol=2)
        colnames(replaceTable) = c("Old","New")
        if(class(replacement)=="function"){
          for(i in 1:repNumber){
            replaceTable[i,1] = substr(fileContent,oldContentIndex[i,2],
                            oldContentIndex[i,3])
            replaceTable[i,2] = replacement(replaceTable[i,1])
          }
        }else{
          for(i in 1:repNumber){
            replaceTable[i,1] = substr(fileContent,oldContentIndex[i,2],
                            oldContentIndex[i,3])
          }
          replaceTable[,2] = replacement
        }
        print(replaceTable)
        cat("The above replacement(s) will be done.\n")
        askUserIntent()->userIntent
        if(userIntent=='o'){
          fileContent=pairMatchReplaceOneByOne(fileContent,
              as.vector(t(oldContentIndex)),as.vector(t(replaceTable)),repNumber)
        }else{
          if(userIntent=='a'){
            fileContent=pairMatchReplaceMultiple(fileContent,
              as.vector(t(oldContentIndex)),as.vector(t(replaceTable)),repNumber)
          }else{
            cancelOperation()
          }
        }
      }else{
        cat("No replacement is needed.\n")
      }
    }else{
      fileContent=dclong.String::pairMatchSub(left=left,right=right,replacement=replacement,
              x=fileContent,start=start,all=all,fixed=fixed)
    }
    #overwrite the file
    cat(fileContent,file=f,append=FALSE)
  }
}

pairMatchReplaceOneByOne = function(fileContent,index,replacement,flag){
  if(flag>0){
    cat(paste('Relace "',replacement[1],'" with "',replacement[2],'"?\n',sep=""))
    userIntent = askUserIntent2()
    if(userIntent=='y'){
      pairMatchReplaceOnce(fileContent,index,replacement,flag)->fileContent
      index = index[-(1:3)] + nchar(replacement[2]) - nchar(replacement[1])
      replacement = replacement[-(1:2)]
      flag = flag -1
      return(pairMatchReplaceOneByOne(fileContent,index,replacement,flag))
    }else{
      if(userIntent=='a'){
        return(pairMatchReplaceMultiple(fileContent,index,replacement,flag))
      }else{
        if(userIntent=='s'){
          index = index[-c(1:3)]
          replacement = replacement[-(1:2)]
          flag = flag -1
          return(pairMatchReplaceOneByOne(fileContent,index,replacement,flag))
        }else{
          if(userIntent=='p'){
            return(fileContent)
          }else{
            cancelOperation()
          }
        }
      }
    }
  }
  pairMatchReplaceOnce(fileContent,index,replacement,flag)
}

pairMatchReplaceMultiple = function(fileContent,index,replacement,flag){
  if(flag>0){
    for(i in flag:1){
      temp3 = 3*i
      temp2= 2*i
      fileContent = dclong.String::strReplace(x=fileContent,first=index[temp3-1],
                          last=index[temp3],replacement=replacement[temp2])
    }
  }
  return(fileContent)
}

pairMatchReplaceOnce = function(fileContent,index,replacement,flag){
  if(flag>0){
    fileContent = dclong.String::strReplace(x=fileContent,first=index[2],
                          last=index[3],replacement=replacement[2])
  }
  return(fileContent)
}
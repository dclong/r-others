fibo=function(n,ini,coef=rep(1,length(ini)),start=1,irev=FALSE,crev=FALSE)
{#
#argument n is the subscript of the array to be calculated
#argument ini is the initial value vector starting from the first term in the recursion formula
#argument coef is the coefficients vector in the linear recursion equation
  order=length(ini)
  if(order<2)
    stop("the order of the difference equation must be at least 2.")
  if(length(coef)!=order)
    stop("the lengths of the coefficents vector and the initial values vector must be the same.")
  n=n-start+1
  nmax=max(n)
  if(nmax<=order)
    return(ini[n])
  if(irev)
    ini=rev(ini)
  if(crev)
    coef=rev(coef)
  result=rep(0,nmax)
  result[1:order]=ini
  for(i in (order+1):nmax)
    result[i]=result[(i-order):(i-1)]%*%coef
  return(result[n])
}
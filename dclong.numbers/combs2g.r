
#' Special Functions of Mathematics
#' returns a nature combination of n choose m. %% ~~ A concise (1-5 lines)
#' description of what the function does. ~~
#'
#' %% ~~ If necessary, more details than the description above ~~
#'
#' @param n %% ~~Describe \code{n} here~~
#' @param m %% ~~Describe \code{m} here~~
#' @param k %% ~~Describe \code{k} here~~
#' @param full %% ~~Describe \code{full} here~~
#' @param showWarnings %% ~~Describe \code{showWarnings} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords combination math
#' @examples
#'
#' #get the 6th nature combination of {5 choose 3}
#' combs2g(5,3,6)
#'
combs2g <-
function(n,m,k=0,full=FALSE,showWarnings=TRUE)
{#This function is use to find the combinations of n choose m
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Parameter m should be a positive integer, which is less or equal to n
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Parameter k decides the result return
#     if k is between 1 and n choose m then return the kth nature combination
#        else return all combinations in order
  if(n<m)
    stop("parameter n should not be less than m")
  if(m<=0)
    stop("positive value is required for parameter m")
  if(full)
  {
    result=combs2g(n,m,k,full=FALSE,showWarnings=showWarnings)
    result=apply(result,1,function(x) c(x,(1:n)[-x]))

    return(combsng(n,m,k))
  }
  Total=choose(n,m)
  if(k>0&&k<=Total)
  {#find the kth nature combination
    if(m==1)
    {
      result=k
      result=as.matrix(result)
      colnames(result)="G1-1"
      return(result)
    }
    summation=0
    flag=0
    while(summation<k)
    {
      flag=flag+1
      summation=summation+choose(n-flag,m-1)
    }
    result=c(flag,combs2g(n-flag,m-1,k-summation+choose(n-flag,m-1))+flag)
    result=t(result)
    colnames(result)=paste("G1",1:m,sep='-')
    return(result)
  }
  #find all the combinations in order
  if(showWarnings)
    warning("k specified is out-of-bounds. All possible combinations will be returned!")
  result=NULL
  for(i in 1:Total)
    result=rbind(result,combs2g(n,m,i))
  return(result)
}




#' SP Sequence
#' generates SP sequence. %% ~~ A concise (1-5 lines) description of what the
#' function does. ~~
#'
#' %% ~~ If necessary, more details than the description above ~~
#'
#' @param n an positve integer. %% ~~Describe \code{n} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords SP sequence
#' @examples
#'
#'
sp <-
function(n)
{#generate sp sereis
  as.integer(n)
	sequence<-c(1)
	count<-1
	while(count<n)
	{
		count<-count+1
		sequence<-c(sequence,count,-rev(sequence))
	}
	return(sequence)
}




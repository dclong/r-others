

#' Numbers Theory
#' greatest common factor.
#' @param \dots
#' @return
#' @note
#' @author Chuanlong Benjamin Du
#' @seealso
#' @references
#' @keywords greatest factor
#' @examples
#' 
#' 
gcd=function(...)
{#This function is used to find the greatest common divisor of two positive intger
#m and n
    nums = list(...)
    len = length(nums)
    if (len < 1)
        stop("at least one integer is required.")
    if (len < 2)
        return(nums[[1]])
    #--------------------------------------------
    if(len<3)
    {
      m=nums[[1]]
      n=nums[[2]]
      dividend=ifelse(m>n,m,n)
      divisor=ifelse(m<n,m,n)
      quotient=dividend%/%divisor
      remainder=dividend%%divisor
      while(remainder)
      {
        dividend=divisor
        divisor=remainder
        quotient=dividend%/%divisor
        remainder=dividend%%divisor
      }
      return(divisor)
    }
    
    gcd(nums[[1]],do.call(gcd,nums[-1]))
}

opt=function(fn,interval,dy=10^-8,dx=10^-8)
{#This function is used to find the maximum of a function in a given interval
#--------------------------------------------------------------------------------
#Parameter fn is the function to be maximized
#Parameter interval is a vector containing the end points of the given interval
#Parameter dy is the stopping rule for the function
#Parameter dx is the stopping rule for x
#--------------------------------------------------------------------------------
if(!is.vector(interval))
   stop("parameter interval should be a vector")
if(length(interval)<2)
   stop("the parameter interval should contain two elements at least")
if(length(interval)>2)
   warning("the length of interval is greater than 2.")
xlow=min(interval)
xhigh=max(interval)
IntWidth=xhigh-xlow
while(!(fn(xhigh)-fn(xlow)<dy & IntWidth<dx))
{
   IntWidth=IntWidth/3
   x1=xlow+IntWidth
   x2=xhigh-IntWidth
   y1=fn(x1)
   y2=fn(x2)
   if(y1<y2)
      xlow=x1
   else
      if(y1>y2)
         xhigh=x2
      else
      {
         xlow=x1
         xhigh=x2
      }
   IntWidth=2*IntWidth
}
x=(xlow+xhigh)/2
return(list(x=x,maximum=fn(x)))
}

fn1=function(x)
{
   return(x^2)
}

fn2=function(x)
{
   return(x*sin(x))
}


#' Block Bootstrap
#' sample mean sqrt(m)(xbar*-Exbar*) of circular block bootstrap. %% ~~ A
#' concise (1-5 lines) description of what the function does. ~~
#' 
#' %% ~~ If necessary, more details than the description above ~~
#' 
#' @param x %% ~~Describe \code{x} here~~
#' @param b %% ~~Describe \code{b} here~~
#' @param size %% ~~Describe \code{size} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords block bootstrap
#' @examples
#' 
#' 
sm.cbb=function(x,b,size)
{#This function randomly generate (standardized) CBB sample means
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Argument x is a time series
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Argument b is the block size
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Argument size is the number of sample means to be generated
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  n=length(x)
  Nb=n-b+1
  K=floor(n/b)
  m=K*b
  muhat=mean(x)
  x=c(x,x[1:b])
  result=NULL
  for(j in 1:size)
  {
    sam.index=sample(x=1:n,size=K,replace=TRUE,prob=rep(1,n))
    sam.bm=NULL
    for(i in sam.index)
    {
      sam.bm=c(sam.bm,mean(x[i:(i+b-1)]))
    }
    result=c(result,sqrt(m)*(mean(sam.bm)-muhat))
  }
  return(result)
}



#' Block Bootstrap
#' sample mean sqrt(m)(xbar*-Exbar*) of moving block bootstrap. %% ~~ A concise
#' (1-5 lines) description of what the function does. ~~
#' 
#' %% ~~ If necessary, more details than the description above ~~
#' 
#' @param x %% ~~Describe \code{x} here~~
#' @param b %% ~~Describe \code{b} here~~
#' @param size %% ~~Describe \code{size} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords bootstrap block
#' @examples
#' 
#' 
sm.mbb=function(x,b,size)
{#This function randomly generate MBB sample means
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Argument x is a time series
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Argument b is the block size
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Argument size is the number of sample means to be generated
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  n=length(x)
  Nb=n-b+1
  K=floor(n/b)
  m=K*b
  muhat=mean(x)*n/Nb
  temp=1:b
  muhat=muhat-(x[temp]+x[n:Nb])%*%(1-temp/b)/Nb
  result=NULL
  for(j in 1:size)
  {
    sam.index=sample(x=1:Nb,size=K,replace=TRUE,prob=rep(1,Nb))
    sam.bm=NULL
    for(i in sam.index)
    {
      sam.bm=c(sam.bm,mean(x[i:(i+b-1)]))
    }
    result=c(result,sqrt(m)*(mean(sam.bm)-muhat))
  }
  return(result)
}



#' Block Bootstrap
#' variance estimator of moving block bootstrap. %% ~~ A concise (1-5 lines)
#' description of what the function does. ~~
#' 
#' %% ~~ If necessary, more details than the description above ~~
#' 
#' @param b %% ~~Describe \code{b} here~~
#' @param x %% ~~Describe \code{x} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords block bootstrap
#' @examples
#' 
#' 
var.mbb=function(b,x)
{#This function calculate the bootstrap variance estimator for a given length of block
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Argument b is the length of the block
#Argument x is the observed time series
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  x=as.vector(x)
  n=length(x)
  if(n<=b)
    stop("the sample size is too small compared to the block size")
  m=n-b+1#number of blocks
  mean.block=NULL
  for(i in 1:m)
    mean.block=c(mean.block,mean(x[i:(b+i-1)]))
  return(b*(n-b)/(n-b+1)*var(mean.block))
}

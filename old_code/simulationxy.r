SimulationXY=function(t,n=10000)
{#This function simulate the distribution of X/Y, where X and Y are all standard uniform random variables
   sam=matrix(runif(2*n),nco=2)
   ratio=sam[,1]/sam[,2]
   sum(ratio<=t)/n
}
#=============================================================== 
# Tinn-R: necessary packages and functions 
# Tinn-R: >= 2.0.0.1 
#=============================================================== 
library(utils) 
 
# check necessary packages 
necessary = c('TinnR', 'svSocket') 
installed = necessary %in% installed.packages()[, 'Package'] 
if (length(necessary[!installed]) >=1) 
install.packages(necessary[!installed], dep=T) 
 
# set options 
options(use.DDE=T) 
# uncoment the line below if you want Tinn-R starts 
# always R starts 
#options(IDE='C:/Tinn-R/bin/Tinn-R.exe') 
 
# load packages 
library(TinnR) 
library(svSocket) 
 
# start DDE 
trDDEInstall() 
 
.trPaths = c( 
'C:/Users/adu/AppData/Roaming/Tinn-R/tmp/', 
'C:/Users/adu/AppData/Roaming/Tinn-R/tmp/search.txt', 
'C:/Users/adu/AppData/Roaming/Tinn-R/tmp/objects.txt', 
'C:/Users/adu/AppData/Roaming/Tinn-R/tmp/file.r', 
'C:/Users/adu/AppData/Roaming/Tinn-R/tmp/selection.r', 
'C:/Users/adu/AppData/Roaming/Tinn-R/tmp/block.r', 
'C:/Users/adu/AppData/Roaming/Tinn-R/tmp/lines.r') 
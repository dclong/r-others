#' @param beta.init a vector of initial values for coefficient beta.
#' @param var.init a vector of initial values for tao^2, sigma1^2, sigma2^2 and sigma3^2.
mlm.em = function(beta.init,var.init,data.sim,iter=Inf,criteria=1e-6){
    #data
    X1 = data.sim$X1
    X2 = data.sim$X2
    X3 = data.sim$X3
    X = data.sim$X
    y1 = data.sim$y1
    y2 = data.sim$y2
    y3 = data.sim$y3
    y = data.sim$y
    Z1 = data.sim$Z1
    Z2 = data.sim$Z1
    Z3 = data.sim$Z3
    Z = data.sim$Z
    n1 = data.sim$n1#a vector
    n2 = data.sim$n2#a vector
    n3 = data.sim$n3#a vector
    ns = data.sim$ns
    n = data.sim$n
    #current interation value
    beta.current = beta.init
    tao.sq.current = var.init[1]
    sigma.sq.current = var.init[2:4]
    #new estimates
    b1.hat = bHat(ns[1],sigma.sq.current[1],y1,X1,beta.current,tao.sq.current)
    b2.hat = bHat(ns[2],sigma.sq.current[2],y2,X2,beta.current,tao.sq.current)
    b3.hat = bHat(ns[3],sigma.sq.current[3],y3,X3,beta.current,tao.sq.current)
    b.hat = c(b1.hat,b2.hat,b3.hat)
    b12.hat = b2Hat(ns[1],sigma.sq.current[1],y1,X1,beta.current,tao.sq.current)
    b22.hat = b2Hat(ns[2],sigma.sq.current[2],y2,X2,beta.current,tao.sq.current)
    b32.hat = b2Hat(ns[3],sigma.sq.current[3],y3,X3,beta.current,tao.sq.current)
    tao.sq.new = mean(c(b12.hat,b22.hat,b32.hat))
    betaSigmasqHat(y-Z%*%b.hat,X,ns,sigma.sq.current,beta.current,criteria,iter)->bs
    beta.new = bs$beta
    sigma.sq.new = bs$sigma.sq
    #check convergency, interate if necessary
    delta = sum((beta.new-beta.current)^2)+(tao.sq.new-tao.sq.current)^2+sum((sigma.sq.new-sigma.sq.current)^2)
    while(delta>criteria){
        beta.current = beta.new
        tao.sq.current = tao.sq.new
        sigma.sq.current = sigma.sq.new
        b1.hat = bHat(ns[1],sigma.sq.current[1],y1,X1,beta.current,tao.sq.current)
        b2.hat = bHat(ns[2],sigma.sq.current[2],y2,X2,beta.current,tao.sq.current)
        b3.hat = bHat(ns[3],sigma.sq.current[3],y3,X3,beta.current,tao.sq.current)
        b.hat = c(b1.hat,b2.hat,b3.hat)
        b12.hat = b2Hat(ns[1],sigma.sq.current[1],y1,X1,beta.current,tao.sq.current)
        b22.hat = b2Hat(ns[2],sigma.sq.current[2],y2,X2,beta.current,tao.sq.current)
        b32.hat = b2Hat(ns[3],sigma.sq.current[3],y3,X3,beta.current,tao.sq.current)
        tao.sq.new = mean(c(b12.hat,b22.hat,b32.hat))
        #get estimate of sigma.sq and beta, we need interation again here
        betaSigmasqHat(y-Z%*%b.hat,X,ns,sigma.sq.current,beta.current,criteria,iter)->bs
        beta.new = bs$beta
        sigma.sq.new = bs$sigma.sq
        delta = sum((beta.new-beta.current)^2)+(tao.sq.new-tao.sq.current)^2+sum((sigma.sq.new-sigma.sq.current)^2)
  }
    list(beta.hat=beta.new,tao.sq.hat=tao.sq.new,sigma.sq.hat=sigma.sq.new,b.hat=b.hat,delta=delta)
}

betaSigmasqHat = function(U,X,ns,sigma.sq.current,beta.current,criteria,iter){
    #a robust way to estimate beta
    #beta.new = (betaHat(U,X,ns,sigma.sq.current) + beta.current)*0.5
    beta.new = betaHat(U,X,ns,sigma.sq.current)
    #sigma.sq.new = (sigmasqHat(U,X,ns,beta.new) + sigma.sq.current)*0.5
    sigma.sq.new = sigmasqHat(U,X,ns,beta.current)
    delta = sum((beta.new-beta.current)^2) + sum((sigma.sq.new-sigma.sq.current)^2)
    count=1
    while(delta>criteria&&count<iter){
        beta.current = beta.new
        sigma.sq.current = sigma.sq.new
        #beta.new = (betaHat(U,X,ns,sigma.sq.current) + beta.current)*0.5
        beta.new = betaHat(U,X,ns,sigma.sq.current)
        #sigma.sq.new = (sigmasqHat(U,X,ns,beta.new) + sigma.sq.current)*0.5
        sigma.sq.new = sigmasqHat(U,X,ns,beta.current)
        delta = sum((beta.new-beta.current)^2) + sum((sigma.sq.new-sigma.sq.current)^2)
        count = count + 1
    }
    list(beta=beta.new,sigma.sq=sigma.sq.new)
}

betaHat = function(U,X,ns,sigma.sq.current){
    Vinv = diag(1/rep(sigma.sq.current,ns))
    solve(t(X)%*%Vinv%*%X)%*%t(X)%*%Vinv%*%U
}

sigmasqHat = function(U,X,ns,beta.current){
    U-X%*%beta.current->errors
    n1 = ns[1]
    n2 = ns[2]
    n = sum(ns)
    sigma1.sq = mean(errors[1:n1]^2)
    sigma2.sq = mean(errors[(n1+1):(n1+n2)]^2)
    sigma3.sq = mean(errors[(n1+n2+1):n]^2)
    c(sigma1.sq,sigma2.sq,sigma3.sq)
}

bHat = function(nj,sigmaj.sq.current,Yj,Xj,beta.current,tao.sq.current){
        covmat = matrix(tao.sq.current,nrow=nj,ncol=nj)+diag(sigmaj.sq.current,nj)
        tao.sq.current*sum(solve(covmat)%*%(Yj-Xj%*%beta.current))
}

b2Hat = function(nj,sigmaj.sq.current,Yj,Xj,beta.current,tao.sq.current){
  bhat = bHat(nj,sigmaj.sq.current,Yj,Xj,beta.current,tao.sq.current)
  covmat = matrix(tao.sq.current,nrow=nj,ncol=nj)+diag(sigmaj.sq.current,nj)
  result=bhat^2+tao.sq.current-tao.sq.current^2*sum(solve(covmat))
  result
}




dinvg=function(x,alpha,beta)
{#This function calculates the density of inverse gamma distribution
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Parameters alpha and beta are the parameters for inverse gammma distribution
#-----------------------------------------------------------------------------
result=beta^alpha/gamma(alpha)
result=result*((1/x)^(alpha+1)*exp(-beta/x))
return(result)
}
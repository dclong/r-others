
genExpTrun = function(lambda,cut,n){
  rexp(n,rate=lambda)->x
  delta = x<=cut
  ifelse(delta,x,cut)->y
  delta = as.integer(delta)
  list(y=y,delta=delta)
}

data.exp.10000 = genExpTrun(1,2,10000)

exp.em = function(lambda.init,cut,delta.bar,criteria=1e-8){
  lambda.current = lambda.init
  lambda.new = 1/lambda.current+cut*(1-delta.bar)-cut*delta.bar/(exp(cut*lambda.current)-1)
  diff = abs(lambda.new-lambda.current)
  while(diff>criteria){
    lambda.current = lambda.new
    lambda.new = 1/lambda.current+cut*(1-delta.bar)-cut*delta.bar/(exp(cut*lambda.current)-1)
    diff = abs(lambda.new-lambda.current)
  }
  return(lambda.new)
}



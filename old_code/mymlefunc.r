mymlefunc=function(x)
{#This function takes in the observation vector of truncated poisson distribution
#and returns the maximum likelyhood estimate
#x is the observation vector
return(optimize(f=TPLLH,interval=c(0,mean(x)),x=x,maximum=T)$maximum)
}
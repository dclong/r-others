rep(c(150,170,190,220),each=10)->temp
v = 1000/(temp+273.2)
rep(c(8064,5448,1680,528),each=10)->cut
as.vector(matrix(scan("motorette.txt"),ncol=4,byrow=TRUE))->y
motorette = data.frame(v,cut,y)

life.em = function(beta.init,motorette,criteria=1e-8){
  #data
  y = motorette$y
  v = motorette$v
  cut = motorette$cut
  ones = rep(1,length(v))
  x = cbind(ones,v)
  #current values of parameters
  beta.current = beta.init
  #estimate latent variables
  z.hat = zhat(x,beta.current,cut)
  #update parameters
  beta.new = bhat(x,z.hat)
  #check covergency
  delta = sum((beta.new - beta.current)^2)
  while(delta>criteria){
    beta.current = beta.new
    z.hat = zhat(x,beta.current,cut)
    beta.new = bhat(x,z.hat)
    delta = sum((beta.new-beta.current)^2)
  }
  beta.new
}

zhat = function(x,beta.current,cut){
  mu = x%*%beta.current
  trun = log10(cut) - mu
  trunMean = sapply(trun,trunMean.norm) + mu
  ifelse(y<cut,log10(y),trunMean)
}
bhat = function(x,z.hat){
  solve(t(x)%*%x)%*%t(x)%*%z.hat
}
#' Mean of left-truncated standard normal random variable.
#' @param trun lower bunder.
trunMean.norm = function(trun){
  dnorm(trun)/(1-pnorm(trun))
}
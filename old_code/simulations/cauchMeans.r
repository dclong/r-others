
cau.sim = function(x,simSize=10000,sampleSize=100){
  matrix(rcauchy(n=simSize*sampleSize),nrow=simSize,ncol=sampleSize)->sam
  rowMeans(sam)->cauch.means
  colMeans(outer(cauch.means,x,"<="))
}
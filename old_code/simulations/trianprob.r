trianprob=function(nsim=10000)
{
  pdist=function(points)
  {
    p1=points[,1]
    p2=points[,2]
    sum((p1-p2)^2)
  }
  count=0
  for(i in 1:nsim)
  {
    points=matrix(runif(6),nrow=2)
    d1=pdist(points[,-1])
    d2=pdist(points[,-2])
    d3=pdist(points[,-3])
    if(d1+d2>d3&& d1+d3>d2 && d2+d3>d1)
      count=count+1
  }
  count/nsim
}
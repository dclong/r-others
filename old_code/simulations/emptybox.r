emptybox=function(nbox,nball,nsim=100000)
{
  sam=matrix(sample(1:nbox,nball*nsim,replace=TRUE),nrow=nsim)
  nbox-mean(apply(sam,1,function(x) length(unique(x))))
}
rantrian2=function(nsim=1000)
{
  sam=matrix(runif(2*nsim,0,1),ncol=2)
  mins=apply(sam,1,min)
  maxs=apply(sam,1,max)
  mean(mins<0.5 & maxs>0.5 & (maxs-mins)<0.5)
}
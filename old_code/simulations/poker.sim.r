poker.sim=function(nselected, ndistinct, Ndistinct,Ncopies, nsim=10000)
{
  SamSpace=1:(Ndistinct*Ncopies)
  count=0
  for(i in 1:nsim)
  {
    sam=sample(SamSpace,nselected)
    sam=sam%%Ndistinct
    sam=unique(sam)
    if(length(sam)==ndistinct)
      count=count+1
  }
  count/nsim
}
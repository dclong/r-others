Buffon=function(l=1,d=1.5,n=10000)
{#This function is used to simulate Buffon Experiment
#Parameter l is the length of the needle, which has a default value 1.
#Parameter d is the distance of the parellel lines, which has a default value 1.5.
#It is require that l<=d, which is for the convenience of integral.
#Parameter is the sample size we use to do the simulation and it has a default value of 10000.
if(l>d)
   d=1.5*l;
rho=runif(n,max=d/2);
theta=runif(n,max=pi/2);
mean(rho<=l/2*sin(theta))->p
2*l/(p*d)
}
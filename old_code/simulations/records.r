records=function(dist,nsim=1000)
{
  mode(dist)="call"
  result=NULL
  for(count in 1:nsim)
  {
    sam=eval(dist)
    total=0
    mini=sam[1]
    for(i in sam)
      if(i<mini)
      {
        total=total+1
        mini=i
      }
    total=total+1
    result=c(result,total)
  }
  mean(result)
}
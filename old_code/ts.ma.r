ts.ma=function(coef,n)
{#This function generate moving average time series data
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Argument coef is the coeficients vector of moving average model
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Arguemnt n is the length of the time series to be generated
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  len=length(coef)
  sam.err=rnorm(n+len-1)
  sam.ma=NULL
  for(i in 1:n)
  {
    temp=coef%*%sam.err[i:(i+len-1)]
    sam.ma=c(temp,sam.ma)
  }
  return(sam.ma)
}
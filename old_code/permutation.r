permutation=function(x,n)
{#This function is used to do the permutation test
 #refstat is the vector used to store these statistics
 #x is the sample vector
 #n is the number of replication
    len=length(x)
    stat=vector()
    for(i in 1:n)
    {
       newsample=sample(x,len)
       stat=c(stat,abs(median(newsample[1:10])-median(newsample[11:20])))
    }
    return(stat)
}
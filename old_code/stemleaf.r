stemleaf=function(x,n=2,scale=2)
{#This function is used to construct stem-leaf plot from the vector/matrix x
#The parameter x should be vector or matrix
#The parameter n is number of digit of the leaf
#The parameter scale can be used to expand the scale of the plot
   divide=10^n;
   quotient=trunc(x/divide);
   stem=seq(min(quotient),max(quotient));
   #get the format form of the stem
   stemformat=format(stem);
   stemlen=length(stem);
   #get the maximum length of the leaves
   #print the format
   cat("  stems  leaves\n");
   for(i in 1:stemlen)
   {#construct the plot for the responding stem
       #print the format
       cat("  ",stemformat[i],"     | ");
       #get the leaf for the responding stem
       leaf=sort(x[x>=divide*stem[i]&x<divide*(stem[i]+1)])-divide*stem[i];
       #format the leaf
       leaf=format(leaf);
       leaf=chartr(" ","0",leaf);
       #get the smaller part of the leaf
       partleaf=leaf[leaf<"50"];
       cat(partleaf,sep="");
       cat("      ");
       cat(length(partleaf))
       cat("\n");
       #-----------------------------------
       #print the format
       cat("  ",stemformat[i],"     | ");
       #get the bigger part of the leaf
       partleaf=leaf[leaf>="50"];
       cat(partleaf,sep="");
       cat("      ");
       cat(length(partleaf))
       cat("\n");
   }
}
boot=function(lambda,ON=100,BN=10000)
{#This function uses bootstrap method to get bias correted estimate of exponential distribution
#---------------------------------------------------------------------------------------------
#Parameter lambda is the parameter of exponential distribution
#Parameter ON is the size of the original sample
#Parameter BN is the size of the bootstrap sample
#---------------------------------------------------------------------------------------------
   osam=rexp(ON,lambda)
   lambdahat=ON/sum(osam)
   bsam=matrix(rexp(ON*BN,lambdahat),nrow=BN)
   lambdahat.bc=2*lambdahat-mean(ON/apply(bsam,1,sum))
   result=c(lambda,lambdahat,lambdahat.bc)
   names(result)=c("lambda","LambdaHat","LambdaHat.BC")
   return(result)
}                                                                     
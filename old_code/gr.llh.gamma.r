gr.llh.gamma=function(pars,x)
{#gradient of the log likelihood function of Gamma distribution
a=pars[1]
b=pars[2]
n=length(x)
result=-n*digamma(a)-n*log(b)+sum(log(x))
result=c(result,-n*a/b+sum(x)/b^2)
return(result)
}
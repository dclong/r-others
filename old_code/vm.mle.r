#' Cacluate the MLE of von Misese distribution. 
#' Note that the angle data must be in 0 and 2pi.
vM.mle = function(theta,lower,upper){
  y = sum(sin(theta))
  x = sum(cos(theta))
  atan2(y,x)->muhat
  if(muhat<0){
    muhat = muhat + 2*pi
  }
  #solve equation using binary search to find MLE for k
  f=function(x){
    i0 = besselI(x,0,TRUE)
    i1 = besselI(x,1,TRUE)
    i1/i0 - mean(cos(theta-muhat))
  }
  khat = uniroot(f,c(lower,upper))
  vcov = matrix(0,nrow=2,ncol=2)
  r = besselI(khat$root,1,TRUE)/besselI(khat$root,0,TRUE)
  vcov[1,1] = 0.5*(1+r) - r^2
  vcov[1,2] = vcov[2,1] = 0
  vcov[2,2] = khat$root*r
  vcov = solve(length(theta)*vcov)
  names = c("kappa","mu")
  rownames(vcov) = colnames(vcov) = names
  return(list(muhat=muhat,khat=khat,vcov=vcov))
}


vM.mle2 = function(data,kinit,muinit,lower=1e-4,upper=1e4,criteria=1e-6){
  #solve equation using binary search to find MLE for k
  f=function(x,goal){
    i0 = besselI(x,0,TRUE)
    i1 = besselI(x,1,TRUE)
    i1/i0 - goal
  }
  ks.current = kinit
  mu.current = muinit
  #mu
  theta1 = data[,1]
  s1 = sum(sin(theta1))
  c1 = sum(cos(theta1))
  theta2 = data[,2]
  s2 = sum(sin(theta2))
  c2 = sum(cos(theta2))
  theta3 = data[,3]
  s3 = sum(sin(theta3))
  c3 = sum(cos(theta3))
  theta4 = data[,4]
  s4 = sum(sin(theta4))
  c4 = sum(cos(theta4))
  ss = c(s1,s2,s3,s4)
  cs = c(c1,c2,c3,c4)
  mu.new = atan2(sum(ks.current*ss),sum(ks.current*cs))
  if(mu.new<0){
    mu.new = mu.new + 2*pi
  }
  #ks
  ks.new = rep(0,4)
  uniroot(f,c(lower,upper),goal=mean(cos(theta1-mu.new)))$root->ks.new[1]
  uniroot(f,c(lower,upper),goal=mean(cos(theta2-mu.new)))$root->ks.new[2]
  uniroot(f,c(lower,upper),goal=mean(cos(theta3-mu.new)))$root->ks.new[3]
  uniroot(f,c(lower,upper),goal=mean(cos(theta4-mu.new)))$root->ks.new[4]
  #check convergency
  delta = sum((ks.new - ks.current)^2) + (mu.new - mu.current)^2
  while(delta>criteria){
    ks.current = ks.new
    mu.current = mu.new
    #mu
    mu.new = atan2(sum(ks.current*ss),sum(ks.current*cs))
    if(mu.new<0){
      mu.new = mu.new + 2*pi
    }
    uniroot(f,c(lower,upper),goal=mean(cos(theta1-mu.new)))$root->ks.new[1]
    uniroot(f,c(lower,upper),goal=mean(cos(theta2-mu.new)))$root->ks.new[2]
    uniroot(f,c(lower,upper),goal=mean(cos(theta3-mu.new)))$root->ks.new[3]
    uniroot(f,c(lower,upper),goal=mean(cos(theta4-mu.new)))$root->ks.new[4]
    delta = sum((ks.new-ks.current)^2) + (mu.new - mu.current)^2
  }
  
  return(list(mu=mu.new,k=ks.new))
}

llh = function(data,mu,k){
  n = nrow(data)
  s = 0
  for(i in 1:ncol(data)){
    temp = -n*log(besselI(k[i],0)) + k[i]*sum(cos(data[,i]-mu[i]))
    s = s+ temp
  }
  s
}






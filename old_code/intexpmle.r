IntExpMLE=function(theta=1,n=100000)
{#This function is used the check the MLE of the Integer Exponentional distribution
#Integer Exponential is defined as that we only keep the integer part of the observations
#Here the exponential distribution has the mean theta, i.e. pdf=theta*exp(-theta*X)
#Parameter theta is the parameter for the exponential distribution and has a default value 1
#Paramter n is the sample size we used to simulate the results and has a default value 100000
MLE=log(mean(floor(rexp(n,rate=theta)))^-1+1)
result=matrix(c(theta,MLE,abs(MLE-theta)/theta),nrow=1)
colnames(result)=c("Theta","MLE","Error")
result
}
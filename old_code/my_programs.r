
is.designmatrix<-function(x)
{#判断X是否为设计矩阵，单个向量也认为是设计矩阵
	is.vector(x)||is.matrix(x)
}

judge.designmatrix<-function(x)
{#判断X是否为设计矩阵
	#如果不是则打印出错信息并结束所有程序
	if(!(is.vector(x)||is.matrix(x)))
	{
		print("Problem: Wrong input for the design matrix!")
		stop()
	}
}

is.response<-function(y)
{#判断Y是不是因变量
	is.vector(y)||(is.matrix(y)&&ncol(y)==1)
}

judge.response<-function(y)
{#判断Y是否为因变量
	#如果不是则打印出错信息并结束所有程序
	if(!(is.vector(y)||(is.matrix(y)&&ncol(y)==1)))
	{
		print("Problem: Wrong input for the explanatory variable!")
		stop()
	}
}

judge.interval.strong<-function(x,IntervalMin,IntervalMax)
{#判断是否有：x属于(IntervalMin,IntrvalMax)
	#如果不是则打印出错信息并结束所有程序
	if(!(x>IntervalMin&&x<IntervalMax))
	{
		cat("Problem: ")
		cat(x)
		cat(" is not in the interval of (")
		cat(IntervalMin)
		cat(",")
		cat(IntervalMax)
		cat(")!\n")
		stop()
	}
}

judge.interval.weak<-function(x,IntervalMin,IntervalMax)
{#判断是否有：x属于[IntervalMin,IntervalMax]
	#如果不是则打印出错信息并结束所有程序
	if(!(x>=IntervalMin&&x<=IntervalMax))
	{
		cat("Problem: ")
		cat(x)
		cat(" is not on the interval of (")
		cat(IntervalMin)
		cat(",")
		cat(IntervalMax)
		cat(")!\n")
		stop()
	}
}

judge.integer<-function(x)
{#判断X是否为整数
	#如果不是则打印出错信息并结束所有程序
	if(!is.integer(x))
	{
		cat("Problem: ")
		cat(x)
		cat(" is not an integer!\n")
		stop()
	}
}

judge.integer.positive<-function(x)
{#判断X是否为正整数
	#其中X可心是一个向量，这样就判断是否其所有分量满足条件
	#如果不是则打印出错信息并结束所有程序
	if(!(is.integer(x)&&all(x>0)))
	{
		cat("Problem: ")
		cat(x)
		cat(" is not a positive integer!\n")
		stop()
	}
}

is.phalanx<-function(x)
{#判断X是否为方阵
	is.matrix(x) && ncol(x) == nrow(x)
}

judge.phalanx<-function(x)
{#判断X是否为方阵
	#如果不是则打印出错信息并结束所有程序
	if(!(is.matrix(x) && ncol(x) == nrow(y))) {
		cat("Wrong input! A phalanx is needed!")
		stop()
	}
}

is.reversible<-function(x)
{#判断X是否可逆
	is.matrix(x) && ncol(x) == nrow(x) && det(x)
}

judge.reversible<-function(x)
{#判断X是否可逆
	#如果不是则打印出错信息并结束所有程序
	if(!(is.matrix(x) && ncol(x) == nrow(x) && det(x))) {
		cat("Wrong input! A reversible phalanx is needed!\n")
		stop()
	}
}

judge.integer.negative<-function(x)
{#判断X是否为负整数
	#如果不是则打印出错信息并结束所有程序
	if(!(is.integer(x)&&all(x<0)))
	{
		cat("Problem: ")
		cat(x)
		cat(" is not a negative integer!\n")
		stop()
	}
}

rss<-function(x, y, n = 1)
{#对给定的x,y计算给定类型的残差
	#默认求完整形式的残差
	judge.response(y)
	temp = projection(x, n)
	x <- as.matrix(x)
	t(y) %*% (diag(nrow(x)) - temp) %*% y
}


boxcox.transform<-function(y,para)
{#对给定的向量y和参数para进行Box-Cox变换
	judge.response(y)
	n=length(y)
	if(para)
		(y^para-1)/para/cumprod(y)^((para-1)/n)
	else
		log(y)*cumprod(y)^(1/n)
}

is.complete<-function(x)
{#判断矩阵X是否为完整的设计矩阵，即其第一列是否为1
	judge.designmatrix(x)
	x<-as.matrix(x)
	all(x[,1]==rep(1,nrow(x)))
}

designmatrix.transform<-function(x)
{#将矩阵X在其完整形式和非完整形式之间进行轮换
	if(is.complete(x))
		return(x[,2:ncol(x)])
	x<-as.matrix(x)
	cbind(rep(1,nrow(x)),x)
}

unit<-function(x)
{#将给定的非零向量单位化
	if(!is.vector(x)) return("Wrong input! A vector is needed!")
	if(!sum(abs(x)))
		return("Wrong input! The vector should be non-zero!")
	x/sum(x^2)
}

centralize<-function(x,flag= 2)
{#将设计矩阵中心化
	#n=1时保证第一行仍为1，反之则否
	if(is.complete(x)) 
	{
		m <- nrow(x)
		n<-ncol(x)
		temp<-rep(0,n-1)
		for(i in 2:n)
		{
			temp[i-1]<-mean(x[,i])
			x[, i] <- x[, i] - rep(temp[i-1], m)
		}
		if(flag!=1)
			x<-designmatrix.transform(x)
		return(list(matrix=x,average=temp))
	}#返回一个列表，其中Matrix为中心化后的矩阵；
	#vector为中心化向量，要注意其从第二列开始！
	x<-as.matrix(x)
	m <- nrow(x)
	n<-ncol(x)
	temp<-rep(0,n)
	for(i in 1:n)
	{
		temp[i]<-mean(x[,i])
		x[, i] <- x[, i] - rep(temp[i], m)
	}
	if(flag== 1)
		designmatrix.transform(x)
	return(list(matrix=x,average=temp))
}

standardize<-function(li)
{#将设计矩阵标准化,必须在中心化后才能使用
	#其返回类型由li决定
	x<-li$matrix
	if(is.complete(x))
	 {
		n<-ncol(x)
		temp<-rep(0,n-1)
		for(i in 2:ncol(x))
		{
			temp[i-1]<-sqrt(sum(x[,i]^2))
			if(temp[i-1])
				x[, i] <- x[, i]/temp[i-1]
		}
		return(list(matrix=x,average=li$average,variance=temp))
	}
	n<-ncol(x)
	temp<-rep(0,n)
	for(i in 1:n)
	{
		temp[i]<-sqrt(sum(x[,i]^2))
		if(temp[i])
			x[, i] <- x[, i]/temp[i]
	}
	return(list(matrix=x,average=li$average,variance=temp))
}

censtandardize<-function(x,flag= 2)
{#将设计矩阵中心化和标准化
	#n=1时保证第一行仍为1，反之则否
	if(is.complete(x))
    {
		m <- nrow(x)
		n<-ncol(x)
		temp1<-rep(0,n-1)
		temp2<-temp1
		for(i in 2:n)
	   {
		   temp1[i-1]<-mean(x[,i])
			x[, i] <- x[, i] - rep(temp1[i-1], m)
			temp2[i-1]<-sqrt(sum(x[,i]^2))
			if(temp2[i-1])
				x[, i] <- x[, i]/temp2[i-1]
		}
		if(flag!= 1)
			x<-designmatrix.transform(x)
		return(list(matrix=x,average=temp1,variance=temp2))
	}
	x<-as.matrix(x)
	m <- nrow(x)
	n<-ncol(x)
	temp1<-rep(0,n)
	temp2<-rep(0,n)
	for(i in 1:n)
    {
	   temp1[i]<-mean(x[,i])
		x[, i] <- x[, i] - rep(temp1[i], m)
		temp2[i]<-sqrt(sum(x[,i]^2))
		if(temp2[i])
			x[, i] <- x[, i]/temp2[i]
	}
	if(flag== 1)
		x<-designmatrix.transform(x)
	return(list(matrix=x,average=temp1,variance=temp2))
}

orthodox<-function(x)
{#将给定的矩阵正交化
	judge.reversible(x)
	x[, 1]<-x[, 1]/sqrt(sum(x[, 1]^2))
	for(i in 2:ncol(x)) 
	{
		for(j in 1:(i - 1))
			x[, i] <- x[, i] - crossprod(x[, i], x[, j]) * x[, j]
		x[, i] <- x[, i]/sqrt(sum(x[, i]^2))
	}
	x
}

ridge.HK<-function(x,y)
{#用Hoerl-Kennard公式计算岭参数K
	judge.response(y)
	n<-nrow(x)
	p<-ncol(x)
	if(!is.complete(x))
		p<-p+1
	if(n<=p)
		return("The data can't be handled by the Hoerl-Kennard fomular!")
	RSS<-rss(x,y)
	x<-censtandarize(x)
	sys<-eigen(t(x)%*%x)
	alpha<-ginverse(diag(sys$value))%*%t(orthodox(sys$vectors))%*%t(x)%*%y
	RSS/(n-p)/max(alpha^2)
}

expunction<-function(x,flag)
{#对给定的矩阵进行消去变换T(flag)X
	if(!is.matrix(x))
		return("Wrong input! A matrix is needed!")
	judge.integer.positive(flag)
	n<-ncol(x)
	m<-nrow(x)
	if(flag>min(m,n))
		return("Wrong input! The integer entered is too big!")
	temp<-x[flag,flag]
	for(i in (1:m)[-flag])
		for(j in (1:n)[-flag])
			x[i,j]<-x[i,j]-x[flag,j]*x[i,flag]/temp
	x[flag,]<-x[flag,]/temp
	x[,flag]<--x[,flag]/temp
	x[flag,flag]<--x[flag,flag]
	return(x)
}

simplify<-function(x,flag)
{#用于从Sp序列的部分序列中找出对应的变量
	#只与Sp同用
	if(flag>0)
		return(sort(append(x,flag)))
	return(sort(x[!(x==-flag)]))
}

equchoice<-function(x,y)
{#给出在RMSq，Cp和AIC准则下的详细信息以便于对方程进行选取
	judge.response(y)
	if(is.complete(x))
		x1<-designmatrix.transform(x)
	else
	{
		x1<-x
		x<-designmatrix.transform(x1)
	}
	n<-nrow(x)
	p<-ncol(x)
	temp<-rep(1,n)%*%x1
	B<-rbind(c(n,temp),cbind(t(temp),t(x1)%*%x1))
	temp<-t(x)%*%y
	A<-rbind(cbind(B,temp),c(t(temp),crossprod(y,y)))
	A<-expunction(A,1)
	sequence<-sp(p-1)
	count<-length(sequence)
	vartable<-c(0)
	RSS<-rss(x,y)
	d<-list(parameter=list(list(variable=c(0),estimation=c(A[1,p+1]))),RSSq=c(A[p+1,p+1]),RMSq.order=NULL,RMSq=c(A[p+1,p+1]/(n-1)),Cp.order=NULL,
	Cp=c(A[p+1,p+1]*(n-p)/RSS-n+2),slopeCp=c(A[p+1,p+1]*(n-p)/RSS-n+2),AIC.order=NULL,AIC=n*log(A[p+1,p+1])+2)
	for(i in 1:count)
	{
		vartable<-simplify(vartable,sequence[i])
		A<-expunction(A,abs(sequence[i])+1)
		d$parameter<-c(d$parameter,list(list(variable=vartable,estimation=A[vartable+1,p+1])))
		d$RSSq<-c(d$RSSq,A[p+1,p+1])
		q<-length(vartable)
		d$RMSq<-c(d$RMSq,A[p+1,p+1]/(n-q))
		d$Cp<-c(d$Cp,A[p+1,p+1]*(n-p)/RSS-n+2*q)
		d$slopeCp<-c(d$slopeCp,(A[p+1,p+1]*(n-p)/RSS-n+2*q)/q)
		d$AIC<-c(d$AIC,n*log(A[p+1,p+1])+2*q)
	}
	d$RMSq.order<-order(d$RMSq)
	d$RMSq<-sort(d$RMSq)
	d$Cp.order<-order(d$Cp)
	d$Cp<-sort(d$Cp)
	d$slopeCp<-d$slopeCp[d$Cp.order]
	d$AIC.order<-order(d$AIC)
	d$AIC<-sort(d$AIC)
	return(d)
}




relative<-function(x)
{#判断是否有复共线性关系
	if(!is.matrix(x))
		return("Wrong input! A matrix is needed!")
	if(is.complete(x))
		x<-designmatrix.transform(x)
	x<-censtandardize(x)$matrix
	temp<-t(x)%*%x
	sys<-eigen(temp)
	len<-length(sys$values)
	temp<-sys$values[1]/sys$values[len]
	if(temp>1000)
		return(c(temp,"Serious!"))
	else
		if(temp>=100)
			return(c(temp,"Somewhat!"))
		else
			return(c(temp,"Slight!"))
}





rg.inv = function(n,prob){
    #generate u ~ U(0,1)
    u = runif(n)
    sapply(u,.search.up,p=prob)
}

rg.inv.mod = function(n,prob){
    #generate u
    u = runif(n)
    sapply(u,.search.mod,p=prob)
}

rg.trial = function(n,prob){
    sam = rep(1,n)
    for(i in 1:n){
        while(rbinom(1,1,prob=prob)==0){
            sam[i] = sam[i] + 1 
        }
        next
    }
    return(sam)
}

.search.mod = function(u,p){
    start = floor(1/p)
    cdf = 1 - (1-p)^start
    if(cdf<u){
        return(.search.up(u=u,p=p,start=start+1))
    }else{
        return(.search.down(u=u,p=p,start=start-1))
    }
}

.search.up = function(u,p,start=1){
    q = 1 - p
    temp = q^(start-1)
    pmf = temp*p
    cdf = 1 - temp*q
    while(cdf<u){
        start = start + 1
        pmf = pmf*q
        cdf = cdf + pmf
    }
    return(start)
}

.search.down = function(u,p,start=1){
    q = 1 - p
    temp = q^(start-1)
    pmf = temp*p
    cdf = 1 - temp*q
    while(cdf>=u){
        start = start -1
        pmf = pmf/q
        cdf = cdf - pmf
    }
    return(start+1)
}



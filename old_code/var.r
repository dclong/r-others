#' Variance and Standard Deviation

#' Functions \code{var} and \code{sd} here extend \code{stats::var} and \code{stats::sd} to allow one to calculate variance and standard deviation of an finite discrete distribution, i.e. they add an option to use \code{n} as the denominator when calculating the variance and standard deviation. 

#' @param x a numerical vector.
#' @param exact logica; if true, then sample x is the exact distribution, i.e. \code{n} (the length of the vector) is used as the denominator for calculating variance and standard deviation. 

sd = function(x,exact=FALSE,na.rm=FALSE){
    if(exact){
        return(sqrt(var(x=x,exact=TRUE)))
    }
    stats::sd(x=x,na.rm=na.rm)
}

var = function(x, y=NULL, exact=FALSE, na.rm=FALSE,use){
    if(exact){
        return(mean((x - mean(x))^2))
    }
    stats::var(x=x,y=y,na.rm=na.rm,use=use)
}

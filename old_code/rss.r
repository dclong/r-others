designmatrix.transform<-function(x)
{#将矩阵X在其完整形式和非完整形式之间进行轮换
	if(is.complete(x))
		return(x[,2:ncol(x)])
	x<-as.matrix(x)
	cbind(rep(1,nrow(x)),x)
}


projection<-function(x,n=0)
{#求给定矩阵的给定类型的投影阵，默认求一般形式的投影阵
	if(n==1)
	{#求完整设计矩阵形式的投影阵
		if(!is.complete(x))
			designmatrix.transform(x)
		y<-t(x)
		return(x%*%solve(y%*%x)%*%y)
	}
	if(n==2)
	{#求非完整设计矩阵形式的投影阵
		if(is.complete(x))
			designmatrix.transform(x)
		y<-t(x)
			return(x%*%solve(y%*%x)%*%y)
	}
#求一般投影阵
	judge.designmatrix(x)
	y<-t(x)
	x%*%solve(y%*%x)%*%y
}

rss<-function(x, y, n = 1)
{#对给定的x,y计算给定类型的残差
	#默认求完整形式的残差
	temp = projection(x, n)
	x <- as.matrix(x)
	t(y) %*% (diag(nrow(x)) - temp) %*% y
}
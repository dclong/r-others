ts.sim=function(n,ar=0.2,error="exp",loc=-1,scale=1,drop=400,...)
{#This is a powerful function to generate time series
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Argument n is the length of the time series to be generated
#------------------------------------------------------------
#Argument error specifies the distribution of the iid errors
#-------------------------------------------------------------
#Argument drop specifies the length of burn-in
#-------------------------------------------------------------
#Argument ... are additional parameters for error distribution
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  N=n+drop#actual length of time series generated
  error=tolower(error)
  sam.error=get(paste("r",error,sep=""))(N,...)
  sam.error=scale*(sam.error+loc)
  sam.x=sam.error
  for(i in 2:N)
    sam.x[i]=ar*sam.x[i-1]+sam.error[i]
  return(sam.x[(drop+1):N])
}
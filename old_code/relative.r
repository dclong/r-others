censtandardize<-function(x,flag= 2)
{#??����?????��?DD??��o��������??��
	#n=1��������?��̨���?DD��??a1��?����???����?
	if(is.complete(x))
    {
		m <- nrow(x)
		n<-ncol(x)
		temp1<-rep(0,n-1)
		temp2<-temp1
		for(i in 2:n)
	   {
		   temp1[i-1]<-mean(x[,i])
			x[, i] <- x[, i] - rep(temp1[i-1], m)
			temp2[i-1]<-sqrt(sum(x[,i]^2))
			if(temp2[i-1])
				x[, i] <- x[, i]/temp2[i-1]
		}
		if(flag!= 1)
			x<-designmatrix.transform(x)
		return(list(matrix=x,average=temp1,variance=temp2))
	}
	x<-as.matrix(x)
	m <- nrow(x)
	n<-ncol(x)
	temp1<-rep(0,n)
	temp2<-rep(0,n)
	for(i in 1:n)
    {
	   temp1[i]<-mean(x[,i])
		x[, i] <- x[, i] - rep(temp1[i], m)
		temp2[i]<-sqrt(sum(x[,i]^2))
		if(temp2[i])
			x[, i] <- x[, i]/temp2[i]
	}
	if(flag== 1)
		x<-designmatrix.transform(x)
	return(list(matrix=x,average=temp1,variance=temp2))
}


relative<-function(x)
{#?D??��?��?��D?��12??D?1??��
	if(!is.matrix(x))
		return("Wrong input! A matrix is needed!")
	if(is.complete(x))
		x<-designmatrix.transform(x)
	x<-censtandardize(x)$matrix
	temp<-t(x)%*%x
	sys<-eigen(temp)
      print(sys$values)
	len<-length(sys$values)
	temp<-sys$values[1]/sys$values[len]
	if(temp>1000)
		return(c(temp,"Serious!"))
	else
		if(temp>=100)
			return(c(temp,"Somewhat!"))
		else
			return(c(temp,"Slight!"))
}

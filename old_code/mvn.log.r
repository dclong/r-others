mvn.log=function(x,par)
{#This function calculate the logarithm of a multivariate normal distribution
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Argument x
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#Argument par is a list that constains the mean vector and covariance matrix
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  mu=par[[1]]
  sigma=par[[2]]
  return(log(dmnorm(x,mean=mu,varcov=sigma)))
}
#Problem: consider that there are 2k+1 people participate in a competition. Each of them win k times and lose
# k times. If A wins B and B wins C and C wins A we call it a Oriented Triangle. Similarly we define Oriented
#quadrangle ans so on.
#-------------------------------------------------------------------------------------------------------------
#This function is used to solve the problem above. It will helps us to find how many oriented triangle or quadrangle
#and so on there are if all the 2k+1 people win and lose k times.
OrientedTriangle=function(k,side)
{#Parameter k means that there are 2k+1 people.
# Parameter side is the number of sides of the multiangle.
#--------------------------------------------------------------------------------------------------------------
######First build up a directed matrix#########################################################################
people=2*k+1
DM=matrix(0,nrow=people,ncol=people)
for(i in 1:(people-1))
{
    #search how many 1's there are
    ones=sum(DM[i,1:i]==1)
    #assign -1 to all the element after the diagnal element
    DM[i,(i+1):people]=-1
    #assign appropriate number of ones to the elements after the diagnal element
    DM[i,(i+1):(i+k-ones)]=1
}
#####Find the number of directed
    
}
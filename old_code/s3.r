Simulation3=function(t,n=10000)
{
   sam=matrix(runif(3*n),nco=3)
   ratio=sam[,1]*sam[,2]/sam[,3]
   sum(ratio<=t)/n
}
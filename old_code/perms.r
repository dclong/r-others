perms=function(n,m,k)
{#This function is use to find the kth natural permutation (n permutation m)
#Parameter k should not be bigger than n permutation m
#Not finished!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if(n<m)
   stop("parameter n should not be less than m")
if(k>factorial(n)/factorial(n-m))
   stop("parameter k exceeds the number of permutations")
if(k<=0)
   stop("parameter k should be positive")
if(m==1)
   return(k)
 result=NULL
 summation=0
 flag=0
 while(summation<k)
 {
    flag=flag+1
    summation=summation+factorial(n-flag,m-1)
 }
 result=c(result,flag)
 return(c(result,combinations(n-flag,m-1,k-summation+choose(n-flag,m-1))+flag))
}
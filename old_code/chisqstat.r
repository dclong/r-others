ChisqTest=function(x)
{#This function use approximate chisqare statistic to test independence
 apply(x,1,sum)->RowSum
 apply(x,2,sum)->ColSum
 OverallSum=sum(ColSum)
 xEstimate=outer(RowSum,ColSum,FUN="*")/OverallSum
 ChisqStat=sum((x-xEstimate)^2/xEstimate)
 df=(nrow(x)-1)*(ncol(x)-1)
 Pvalue=pchisq(ChisqStat,df,lower.tail=F)
 result=c(ChisqStat,df,Pvalue)
 result=t(result)
 colnames(result)=c("ChisqStat","df","Pvalue")
 return(result)
}
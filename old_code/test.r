test=function(mout,cc,b,ddf,v,d=0)
{
  if(missing(mout)||is.null(mout)){
    if(is.vector(cc)){
      cc = t(cc)
    }
    #check whether cc is row-full-rank
    ndf = nrow(cc)
    if(qr(cc)$rank<ndf){
      stop("coefficient matrix cc must be row-full-rank.")
    }
    cb_d=cc%*%b-d
    cb_d.var = cc%*%v%*%t(cc)
    if(ndf==1){
      #cc has only 1 row, use a t test
      tstat = cb_d/sqrt(cb_d.var)
      pvalue = pt(abs(tstat),df=ddf,lower.tail=FALSE)*2
      result = c(tstat,ddf,pvalue)
      names(result) = c("Tstat","DF","Pvalue")
      return(result)
    }
    Fstat=drop(t(cb_d)%*%solve(cb_d.var)%*%cb_d/ndf)
    pvalue=1-pf(Fstat,ndf,ddf)
    result = c(Fstat,ndf,ddf,pvalue)
    names(result) = c("Fstat","numDF","denDF","Pvalue")
    return(result)
  }
  mclass = class(mout)
  if(mclass=="lm"){
    #output of function lm
    if(missing(ddf)){
      ddf = mout$df
    }
    if(missing(b)){
      b = mout$coefficient
    }
    if(missing(v)){
      v = vcov(mout)
    }
    return(test(cc=cc,b=b,ddf=ddf,v=v,d=d))
  }
  if(mclass=="lme"){
    #output of function lme
    if(missing(b)){
      b = mout$coefficient$fixed
    }
    if(missing(ddf)){
      temp = unique(mout$fixDF$X[cc!=0])
      if(length(temp)==1){
        ddf = temp
      }
    }
    if(missing(v)){
      v = vcov(mout)
    }
    return(test(cc=cc,b=b,ddf=ddf,v=v,d=d))
  }
  stop(paste("model output of type ",mclass," is not support.",sep=""))
}

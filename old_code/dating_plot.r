
par(col="black", lty="solid")
plot(c(-5,65)/60, c(-5,65)/60, type="n", axes=F, xlab="x", ylab="y", 
     main="Probability to Meet Your Friend")
rect(0, 0, 1, 1)
abline(1/6, 1)
abline(-1/6, 1)
at = seq(-1/3, 4/3, 1/3)
lab = c("-1/3", "0", "1/3", "2/3", "1", "4/3")
axis(1, pos=0, at=at, labels=lab)
axis(2, pos=0, at=at, labels=lab)

p1 = c(0, 0)
p2 = c(1/6, 0)
p3 = c(1, 5/6)
p4 = c(1, 1)
p5 = c(5/6, 1)
p6 = c(0, 1/6)

ps = cbind(p1, p2, p3, p4, p5, p6)
polygon(ps[1,], ps[2,], col="pink")

text(1/3, 2/3, labels="y = x + 1/6", col="red")
text(2/3, 1/3, labels="y = x - 1/6", col="red")


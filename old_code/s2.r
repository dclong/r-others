Simulation2=function(t,n=10000)
{
   sam=matrix(runif(2*n),nco=2)
   ratio=sam[,1]*sam[,2]
   sum(ratio<=t)/n
}
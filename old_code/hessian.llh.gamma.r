hessian.llh.gamma=function(pars,x)
{#Hessian matrix of the log likelihood function of Gamma distribution
a=pars[1]
b=pars[2]
n=length(x)
result=matrix(0,nrow=2,ncol=2)
result[1,1]=-n*trigamma(a)
result[1,2]=-n/b
result[2,1]=result[1,2]
result[2,2]=n*a/b^2-2*sum(x)/b^3
return(result)
}
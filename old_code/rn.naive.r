
rn.exp = function(n,d){
  d2 = d*d
  left = n
  result = double(n)
  while(left>0){
    sam.exp = rexp(2*left,rate=0.5)
    sam.z = sqrt(sam.exp + d2)
    sam.unif = runif(2*left)
    sam.z = sam.z[sam.unif<=d/sam.z]
    z.length = length(sam.z)
    if(z.length>0){
      index = n - left
      if(z.length<left){
        result[(index+1):(index+z.length)] = sam.z
        left = left - z.length
      }else{
        sam.z = sam.z[1:left]
        result[(index+1):n] = sam.z
        return(result)
      }
    }
  }
  return(result)
}

rn.naive = function(n,d){
  left = n
  result = double(n)
  while(left>0){
    sam = rnorm(2*left)
    sam = sam[sam>=d]
    sam.length = length(sam)
    if(sam.length>0){
      index = n-left
      if(sam.length<left){
        result[(index+1):(index+sam.length)] = sam
        left = left - sam.length
      }else{
        sam = sam[1:left]
        result[(index+1):n] = sam
        return(result)
      }
    }
  }
  return(result)
}


log.pn = function(x){
  -0.5*x^2
}

dlog.pn = function(x){
  -x
}



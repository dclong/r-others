ExpTrunc=function(lambda=1,mu=1,n=10)
{#Consider the following:
#pdf(X)=1/lambda exp(-x/lambda)
#pdf(Y)=1/mu exp(-x/mu)
#Z=min(X,Y)
#W=I(Z=X)
#This functon simulate the MLEs of lambda and mu use the distribution of Z and W
samX=rexp(n,rate=1/lambda)
samY=rexp(n,rate=1/mu)
samZ=apply(cbind(samX,samY),1,min)
samW=(samZ==samX)
MLElambda=sum(samZ)/sum(samW)
ErrorLambda=abs(MLElambda-lambda)/lambda
MLEmu=sum(samZ)/(n-sum(samW))
ErrorMu=abs(MLEmu-mu)/mu
result=matrix(c(lambda,MLElambda,ErrorLambda,mu,MLEmu,ErrorMu),nrow=1)
colnames(result)=c("Lambda","MLELambda","ErrorLambda","Mu","MLEMu","ErrorMu")
result
}
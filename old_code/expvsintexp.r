ExpVSIntExp=function(theta=1,n=0)
{#As the definition If we lose some information when we use IntExp distribuiton instead of Exp distribution
#This function is used to simulate and compare MLEs of the two distribution
#Here the Exp distribution has pdf theta*exp(-theta*x)
#Parameter theta is the paramter for the Exp and IntExp distribution and has a default value 1
#Parameter n is the sample size we use to simulate the result and has a default value 0
#When n=0, this program gives the comparison of sample size 10, 100, ..., 100000
#When n is not 0, this program gives the comparison of sample size n
if(!n)
{
   result=matrix(theta,nrow=5,ncol=6)
   for(i in 1:5)
   {
      sam=rexp(10^i,rate=theta)
      ExpMLE=mean(sam)^-1
      ExpError=abs(ExpMLE-theta)/theta
      IntExpMLE=log(mean(floor(sam))^-1+1)
      IntExpError=abs(IntExpMLE-theta)/theta
      result[i,1]=10^i
      result[i,3]=ExpMLE
      result[i,5]=ExpError
      result[i,4]=IntExpMLE
      result[i,6]=IntExpError
   }
}
else
{
   result=matrix(theta,nrow=1,ncol=6)
   sam=rexp(n,rate=theta)
   ExpMLE=mean(sam)^-1
   ExpError=abs(ExpMLE-theta)/theta
   IntExpMLE=log(mean(floor(sam))^-1+1)
   IntExpError=abs(IntExpMLE-theta)/theta
   result[1,1]=n
   result[1,3]=ExpMLE
   result[1,5]=ExpError
   result[1,4]=IntExpMLE
   result[1,6]=IntExpError
}
colnames(result)=c("SamSize","Theta","ExpMLE","IntExpMLE","ExpError","IntExpError")
result
}
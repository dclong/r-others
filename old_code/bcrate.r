bcrate=function(lambda,RN,ON,BN)
{#this function is used to calculate the power of corrected estimate
   LambdaMatrix=NULL
   for(i in 1:RN)
   {
      temp=boot(lambda,ON,BN)
      LambdaMatrix=rbind(LambdaMatrix,temp)
   }
   LambdaHat.Mean= mean(LambdaMatrix[,2])
   LambdaHat.BC.Mean=mean(LambdaMatrix[,3])
   LambdaHat.Bias=LambdaHat.Mean-lambda
   LambdaHat.BC.Bias=LambdaHat.BC.Mean-lambda
   LambdaHat.Var=var(LambdaMatrix[,2])
   LambdaHat.BC.Var=var(LambdaMatrix[,3])
   Pbc=mean(abs(LambdaMatrix[,2]-LambdaMatrix[,1])>abs(LambdaMatrix[,3]-LambdaMatrix[,1]))
   result=c(lambda,LambdaHat.Mean,LambdaHat.BC.Mean,LambdaHat.Bias,LambdaHat.BC.Bias,LambdaHat.Var,LambdaHat.BC.Var,Pbc)
   names(result)=c("lambda","LambdaHat.Mean","LambdaHat.BC.Mean","LambdaHat.Bias","LambdaHat.BC.Bias","LambdaHat.Var","LambdaHat.BC.Var","Pbc")
   return(result)
}
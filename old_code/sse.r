SSE=function(mat,anova=T,type=0)
{#This fucntion calculates all kinds of sums of squares
#Parameter mat can be a data frame or a design matrix the last column is treated
#as the response variable
#Parameter type is the type of sums of squares:
#0: the default value which gives out all the four kind of sums of squres
#1: Type I sums of squares
#2: Type II sums of squares
#3: Type III sums of squares
#4: Type IV sums of squares
#Parameter anova is logical. If it's ture then this function preforms the anova
#analysis, else it performs the common linear regression
#Warning: there are some reqest if you want to use this function to calculate the
#sums of squares for you! You'd better give names to each
#column, which can be treated as the name of the predict variables
#Warning: I don't how to calculate Type IV sums of squares now, so this function
#won't give out Type IV sums of squares for instance.
#Warning: there can't be any missing data. If you want this function to deal with
#the case when there is missing data, you might need to use the rank of the projction
#matrices
#######################################################################################
 variables=colnames(mat)
 len=ncol(mat)
 y=as.vector(mat[,len])
 len=len-1
 if(anova)
 {#do the anova analysis
    #################build up the design matrix#################################
    mat=as.data.frame(mat)
    myformula=""
    for(i in 1:len)
    {
       mat[,i]=as.factor(mat[,i])
       myformula=paste(myformula,variables[i],sep="*")
    }
    myformula=sub("[[:punct:]]","",myformula)
    myformula=paste("~",myformula,sep="")
    DesignMatrix=model.matrix(as.formula(myformula),data=mat)
    FactCombs=colnames(DesignMatrix)
    DM.cols=ncol(DesignMatrix)
    n=nrow(DesignMatrix)
    ##############build a data frame#######################################
    items.num=2^len
    result1=matrix(0,nrow=items.num,ncol=5)
    result1=as.data.frame(result1)
    #initial the names of the rows
    flag=1#used to indicate which row is being dealed with
    for(i in 1:len)
    {#effect of i factors
       counter=choose(len,i)
       for(j in 1:counter)
       {
          fact=combinations(len,i,j)
          row.name=connect(variables[fact],sep="*")
          rownames(result1)[flag]=row.name
          flag=flag+1
       }
    }
    rownames(result1)[items.num]="Error"
    #initial the names of the columns
    colnames(result1)=c("Df","Sum-Sq","Mean-Sq","F-value","P-values")
    result1=as.data.frame(result1)
    result1[items.num,4]=""
    result1[items.num,5]=""
    #############calculate all kinds of sums of squares
    if(type==1)
    {#calculate the Type I sums of squares
       Mat.part1=DesignMatrix[,"(Intercept)"]
       P1=project(Mat.part1)
       flag=1#indicate which row is being dealed with
       for(i in 1:len)
       {
          counter=choose(len,i)
          for(j in 1:counter)
          {
             #find factors that are in and out
             fact=combinations(len,i,j)
             #calculate the degrees of freedom
             Df=1
             for(k in 1:i)
                Df=Df*(length(levels(mat[,fact[k]]))-1)
             result1[flag,1]=Df
             #build up the partial design matrix
             fact.in=variables[fact]
             fact.out=setdiff(1:len,fact)
             fact.out=variables[fact.out]
             location=1:DM.cols
             for(k in 1:i)
                location=intersect(location,grep(fact.in[k],FactCombs))
             #drop these columns which should not be in
             steps=len-i
             if(steps>0)
                for(k in 1:steps)
                   location=setdiff(location,grep(fact.out[k],FactCombs))
             Mat.part2=cbind(Mat.part1,DesignMatrix[,location])
             #claculate the Type I sums of squares
             P2=project(Mat.part2)
             sse=y%*%(P2-P1)%*%y
             result1[flag,2]=sse
             result1[flag,3]=sse/Df
             Mat.part1=Mat.part2
             P1=P2
             flag=flag+1
          }
       }
       #deal with the error
       Df=n-DM.cols
       result1[flag,1]=Df
       sse=y%*%(diag(1,n)-P1)%*%y
       result1[flag,2]=sse
       result1[flag,3]=sse/Df
       #calculate F-value and P-value
       for(i in 1:(items.num-1))
       {
           Fstat=result1[i,3]/result1[items.num,3]
           result1[i,4]=Fstat
           result1[i,5]=pf(Fstat,result1[i,1],result1[items.num,1],lower.tail=F)
       }
       return(result1)
    }
    if(type==2)
    {#calculate the Type II sums of squares
       return()
    }
    if(type==3)
    {#calculate the Type III sums of squares
       return()
    }
    if(type==4)
    {#calculate the Type IV sums of squares
       print("This function cannot calculate Type IV sums of squares for instance!")
       return(FALSE)
    }
    #calculate all types of sums of squares
 
 }
 else
 {#do the common linear regression
 
 }

}
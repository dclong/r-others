
#' Create a Skeleton for a New Source Package
#' \code{mypkg()} is a variation of $package.skeletion$.
#' @param name the name of the new package
#' @param environment
#' @param path
#' @param force
#' @param namespace
#' @param code.files
#' @return
#' @note
#' @author Chuanlong Bejamin Du
#' @seealso
#' @references
#' @keywords package
#' @examples
#'
#'
package.skeleton <-
function (name, environment = .GlobalEnv,
    path = getwd(), force = FALSE, namespace = FALSE, code.files = character())
{
  name=dclong.String::sym2str(substitute(name))
  path=dclong.String::sym2str(substitute(path))
  code.files=dclont.String::sym2str(substitute(code.files))
  list=ls(pos=1)
  list=list[list!='currentworkspace.name']
  if(length(list)>0)
    utils::package.skeleton(name=name,list=list,environment=environment,path=path,force=force,namespace=namespace,code_files=code.files)
  else
    utils::package.skeleton(name=name,environment=environment,path=path,force=force,namespace=namespace,code_files=code.files)
  library(roxygen)
}
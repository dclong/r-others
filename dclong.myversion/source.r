
#' Source R Code
#' This function sources all given type of files in a given folder into R environment.
#' If title is not specified, then all files of the right type will be sourced into R.
#' @param ...  title of files to be sourced into R.
#' @param path the path of a folder.
#' @param extension the type of file; default ``.r''.
#' @return
#' @note
#' @author Chuanlong Benjamin Du
#' @seealso
#' @references
#' @keywords source file
#' @examples
#'
#' #source R code from current work directory of R
#' #mysource(path=getwd())
#'
source.adu <-
function(..., flist=NULL, path=getwd(), dp=NULL, extension=".r")
{
  #title=match.call(expand.dots=FALSE)$...
  #extract file information fort dots
  title=list(substitute(...))
  title=sapply(title,sym2str)
  if(!length(title))
    title=character(0)
  #extract information from file.list
  if(!is.null(file.list))
    title=c(title,file.list)
  #decide path
  if(is.null(dp))
    path=trim(sym2str(substitute(path)))
  else
    path=ddget(dp)
  #check file extension
  if(grepl(pattern='^\\.{1}[[:alpha:]]+$',x=extension))
      stop("the extension specified cannot be recognized.")
  if(length(title)==0)
    stop("no file is specified.")
  #source in files
  result=NULL
  for(file in title){
    fullFilePath=path.comb(path,file)
    if(myfile.exists(fullFilePath))
    {
      base::source(fullFilePath)
      result=c(result,TRUE)
    }
    else{
      #add extension
      fullFilePath=path.comb(fullFilePath,extension)
      if(myfile.exists(fullFilePath)){
          base::source(fllFilePath)
          result=c(result,TRUE)
      }
      else{
          result=c(reuslt,FALSE)
      }
    }
  }
}


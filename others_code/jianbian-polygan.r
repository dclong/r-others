# 画个渐变多边形
y=0.1+runif(20,0.2,1)
xx=c(1,1:20,20);yy=c(0,y,0)
plot(xx,yy,type="n",xlab="x",ylab="y")
# 多边形的高越来越小，颜色越来越红
for(i in 255:0){
yy=c(0,y-(1-i/255)*min(y),0);polygon(xx,yy,col=rgb(1,i/255,0),border=NA)
# 停一会儿看看清楚
Sys.sleep(0.05)           }
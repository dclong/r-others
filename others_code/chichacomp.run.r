
image(getCharMatrix('ͳ'))

image(getCharMatrix('ͳ'), col = c('yellow', 'red'))


shapeSimilarity <- function(c1, c2)
    sum(getCharMatrix(c1) == getCharMatrix(c2)) / 256

shapeSimilarity('��', 'ʿ')
#[1] 0.9609375

shapeSimilarity('��', '��')
#[1] 0.6992188

shapeSimilarity('��', '��')
#[1] 0.6835938

showShapeSimilarity <- function(c1, c2)
    image(getCharMatrix(c1) * 2 + getCharMatrix(c2),
    col = c('white', 'red', 'green', 'yellow'))

showShapeSimilarity('��', 'ʿ')
readLines(n = 1)

showShapeSimilarity('��', '��')
readLines(n = 1)

showShapeSimilarity('��', '��')
# Romberg's method  
# f - integrand function  
# a - left integration interval endpoint  
# b - right integration interval endpoint  
# n - maximum level in Romberg table  
# return - Romberg table  
  
romberg <- function(f, a, b, n) {  
  r <- matrix(0, n + 1, n + 1);  
    
  r[1, 1] <- (b - a) * (f(a) + f(b)) / 2;  
    
  for (i in 1:n) {  
    h <- (b - a) / (2 ^ i);  
    s <- 0;  
    for (k in 1:(2 ^ (i - 1))) {  
      s <- s + f(a + (2 * k - 1) * h);  
    }  
    r[i + 1, 1] <- r[i, 1] / 2 + h * s;  
  }  
    
  for (i in 1:n) {  
    for (j in 1:i) {  
      r[i + 1, j + 1] <- r[i + 1, j] + (r[i + 1, j] - r[i, j]) / (4 ^ j - 1);  
    }  
  }  
  r;  
}  
  
f <- function(x) 2 / sqrt(pi) * exp(-x)  
romberg(f, 0, 1, 5)  
plot.tri <- function(n = 1000, col ="blue", ani=FALSE, cex=1.2){
  p <- runif(n); 
  X <- rbind(rep(0, n), rep(0, n))
  B <- cbind(c(0,0),c(0.25,0.433),c(0.5,0))
  if(ani) plot(0,0,xlim=c(0,1),ylim=c(0,0.85),type="n",xlab="",ylab="")
  for(i in 2:n){ 
        pp <- p[i]; 
        ind <- rank(c(c(1/3,2/3,1), pp), ties.method="min")[4]
        X[,i] <- 0.5*X[,i-1] + B[,ind]
        if(ani) points(X[1,i], X[2,i],pch = ".", cex = 1, col = col)
    }
  if(!ani) plot(X[1,], X[2,],pch = ".", cex = cex, col = col, xlab="", ylab="") 
}

### example
plot.tri(100000, ani=TRUE)
getCharMatrix <- function(text) {
    encoding <- gsub('LC_CTYPE=[^.]*[.]([^;]*);.*', '\\1', Sys.getlocale())
    code <- as.integer(charToRaw(iconv(text, encoding, 'gb2312')))
    if ((length(code) != 2) || (code[1] < 161) || (code[2] < 161))
        stop('First parameter text must be a Chinese character!')
    code <- code - 161
    f <- file('HZK16', 'rb')
    seek(f, (code[1] * 94 + code[2]) * 32)
    shape <- readBin(f, raw(), 32)
    close(f)
    matrix(as.integer(rev(rawToBits(shape))), 16, 16)[c(9:16,1:8),]
}
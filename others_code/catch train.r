prb = replicate(100, {
x = sample(c(0, 5, 10), 1, prob = c(0.7, 0.2, 0.1))
y = sample(c(28, 30, 32, 34), 1, prob = c(0.3, 0.4, 0.2, 0.1))
plot(0:40, rep(1, 41), type = "n", xlab = "time", ylab = "",
axes = FALSE)
axis(1, 0:40)
r = rnorm(1, 30, 2)
points(x, 1, pch = 15)
i = 0
while (i <= r) {
i = i + 1
segments(x, 1, x + i, 1)
if (x + i >= y)
points(y, 1, pch = 19)
Sys.sleep(0.1)
}
points(y, 1, pch = 19)
title(ifelse(x + r <= y, "poor... missed the train!", "Bingo! catched the train!"))
Sys.sleep(4)
x + r > y
})
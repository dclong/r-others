library(diagram)
# 7 ×4 矩阵
pos <- coordinates(pos = c(4,4,4,4,4,4,4))
# plow.new
openplotmat(main="")
# 直箭头
for(i in c(1,9,17)) straightarrow(from = pos[i,] , to = pos[i + 4,],arr.pos=0.6)
for(i in c(5,13,21)) straightarrow(from = pos[i,] , to = pos[i + 4,],arr.pos=0.7)
for(i in 13:15) straightarrow(from = pos[i,] , to = pos[i + 1,])
# 直角箭头  V 垂直 H 水平
bentarrow(from = pos[15,], to = pos[1,],path = 'V')
bentarrow(from = pos[5,], to = pos[14,],path = 'H')
bentarrow(from = pos[21,], to = pos[14,],path = 'H')
# 绘制矩形
textrect(pos[1,],radx=0.06,rady=0.035,lab = c("阶段 I 试验"),cex = 0.8)
textrect(pos[9,],radx=0.06,rady=0.035,lab = c("阶段 II 试验"),cex = 0.8)
textrect(pos[17,],radx=0.06,rady=0.035,lab = c("阶段 III 试验"),cex = 0.8)
textrect(pos[14,],radx=0.06,rady=0.035,lab = c("返回研究"),cex = 0.8)
# 绘制菱形(判断)
for(i in c(5,13,21))textdiamond(pos[i,],radx = 0.07,rady = 0.055,lab = c("是否达","到目的?"),cex = 0.8)
textdiamond(pos[15,],radx = 0.07,rady = 0.055,lab = c("继续"),cex = 0.8)
# 圆形
textround(pos[25,],radx=0.04,rady=0.04,lab = c("删除审批提交"),cex = 0.8)
textround(pos[16,],radx=0.04,rady=0.04,lab = c("取消项目"),cex = 0.8)
# 添加文本
for(i in c(5,11,13,21)) text(pos[i,1] + 0.035,(pos[i,2]+pos[i+4,2])/2,"是")
for(i in c(5,13,15,21)) text((pos[i,1]+pos[i+1,1])/2,pos[i,2]+0.035,"否")
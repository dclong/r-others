THXDaily.Stk <- function(fl, path='THXpath/history/shase/day'){
    dat <- readBin(file.path(path, fl), 'raw', n=100000000)
    mat <- matrix(as.integer(dat[-c(1:72)]), byrow=T, ncol=56)
    mat <- mat[, 1:28, drop=FALSE]
    .tran <- function(x){
        flag <- x[4]
        flag <- flag %/% 16
        x[4] <- x[4] - flag*16
        if(flag > 7){
            flag <- (8 - flag)
        }
        return((10^flag) * sum(x * 256^(0:3)))
    }
    dt <- as.Date(as.character(apply(mat[, 1:4, drop=FALSE], 1, function(x) sum(x * 256^(0:3)))), '%Y%m%d')
    op <- apply(mat[, 5:7, drop=FALSE], 1, function(x) sum(x * 256^(0:2)))/1000
    hi <- apply(mat[, 9:11, drop=FALSE], 1, function(x) sum(x * 256^(0:2)))/1000
    lo <- apply(mat[, 13:15, drop=FALSE], 1, function(x) sum(x * 256^(0:2)))/1000
    cl <- apply(mat[, 17:19, drop=FALSE], 1, function(x) sum(x * 256^(0:2)))/1000
    am <- apply(mat[, 21:24, drop=FALSE], 1, .tran)
    vo <- apply(mat[, 25:28, drop=FALSE], 1, .tran)
    return(data.frame(dt, op, hi, lo, cl, am, vo))
}


library(quantmod)
getSymbols()


��������

library(RODBC)
z<-odbcConnectExcel("e:/my documents/wanke.xls")
wanke<-sqlFetch(z,"Sheet1")
close(z)

#��ͼ

library(playwith)
a<-factor(wanke$combine)
playwith(xyplot(price~tu|a,data=as.data.frame(wanke),
       panel=panel.barchart,box.ratio=0.002,
       xlab = NULL,ylab = NULL,layout = c(length(levels(a)),1),
       scales = list( x = list(draw = FALSE),tick.number=10)))


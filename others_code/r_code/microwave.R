


#  Code for creating probability plots in R
#  or S-Plus.  It is stored in the file 
#
#                     microwave.R  
#
#  Any line preceded with a pound sign is a 
#  comment that is ignored by the program.  
#  Data are read into a data frame from the file
#
#                     microwave.closed.dat
 
  mdat<-read.table("c:/stat501/microwave.closed.dat",header=F)
 
#  Assign labels to the variables (columns 
#  of the data frame)

  m.var<-c('Oven','Radiation')
  names(mdat)<-m.var

  mdat
 
#  Compute the sample mean and the
#  sample cvariance matrix and print 
#  the results

  mmean<-mean(mdat$Radiation)
  mmean
  mvar<-var(mdat$Radiation)
  mvar


# Create a normal probability plot. Note that
# this function does not avearge normal quantiles
# for tied data values

  par(pch=5)		
    qqnorm(mdat$Radiation,  main="Normal Probability Plot") 

#  Compute the Shapir-Willk test statistic

   shapiro.test(mdat$Radiation)


# Compute natural logarithm of radiation values
	
   mdat$logRad<-log(mdat$Radiation)	
  
# Compute Q-Q plot and value of Shapiro-Wilk test
# for the natural logaritm of the radiation values
		
    qqnorm(mdat$logRad,  main="Normal Probability Plot") 

    shapiro.test(mdat$logRad)















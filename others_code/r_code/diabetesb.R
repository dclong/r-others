



#  This program performs cluster analysis on the Stanford diabetes
#  data considered in class.  This code is posted as  diabetes.R

#  Enter the data and assign variable names.
  
  diabetes<-read.table("c:/documents and settings/kkoehler.IASTATE/my documents/courses/st501/data/diabetes.dat",
    header=F, col.names=c("Patient", "x1", "x2", "x3", "x4", "x5", "c1", "c2"))

  diabetes


#  Create a matrix of variables to be used in the cluster analysis
#  and a vector of patient id numbers
	
  patient.id<-diabetes[ ,1]	
  diabetes.x<-diabetes[ ,2:6]

#  Standardize the data

  diabetes.mean<-apply(diabetes.x,2,mean)
  diabetes.std<-sqrt(apply(diabetes.x,2,var))
  diabetes.sx<-sweep(diabetes.x,2,diabetes.mean,FUN="-")
  diabetes.sx<-sweep(diabetes.sx,2,diabetes.std,FUN="/")
 


#  Use complete linkage, this is also the default method

	hc<-hclust(dist(diabetes.sx),method="compact")
        plclust(hc,label=patient.id)
        title("Complete Linkage Cluster Analysis: Stanford Diabetes Data") 
	
#  Enter the following to display the tree without labels, then save
#	  the tree structure without plotting it, and provide the
#         capability of pointing at a leaf to it identified
#	
#	plclust(hc,label=FALSE)	
#	xy<-plclust(hc,plot=FALSE)
#	identify(xy)
	
#  Create a vector of cluster labels.  Here 7 clusters are requested
	 	
	cutree(hc, 7)

#  Use canonical discriminants to display the clusters.  First source
#  in the functions   lda   and  eqscplot  stored in the file
#               /home/kkoehler/st501/lda.spl
#  The first function computes linear canonical discriminants and
#  the second function is used to plot the computed scores.
	
 source("/home/kkoehler/st501/lda.spl")

#  Compute canonical dsicriminant scores and display 2-dimensional
#  projections of the clusters

 a<-lda(diabetes.sx, cutree(hc, 7))  
 a$svd^2/sum(a$svd^2)

#  Print the coefficients for the canonical discriminants

 a$scaling

#  Compute the scores for the first two canonical discriminants

 scores<-diabetes.sx %*% a$scaling[ ,1:2]

#  Plot the scores for the first two canonical discriminants

 eqscplot(scores, type="n", xlab="first canonical discriminant",
     ylab="second canonical discriminant")
 text(scores, cutree(hc, 7))
 title("Complete Linkage Cluster Analysis: Stanford Diabetes Data")





#  Use average linkage

	hc<-hclust(dist(diabetes.sx),method="average")
        plclust(hc,label=patient.id)
        title("Average Linkage Cluster Analysis: Stanford Diabetes Data") 

 a<-lda(diabetes.sx, cutree(hc, 7))  
 a$svd^2/sum(a$svd^2)
 a$scaling
 scores<-diabetes.sx %*% a$scaling[ ,1:2]
 eqscplot(scores, type="n", xlab="first canonical discriminant",
     ylab="second canonical discriminant")
 text(scores, cutree(hc, 7))
 title("Average Linkage Cluster Analysis: Stanford Diabetes Data")



#  Use single linkage

	hc<-hclust(dist(diabetes.sx),method="connected")
        plclust(hc,label=patient.id)
        title("Single Linkage Cluster Analysis: Stanford Diabetes Data") 

 a<-lda(diabetes.sx, cutree(hc, 7))  
 a$svd^2/sum(a$svd^2)
 a$scaling
 scores<-diabetes.sx %*% a$scaling[ ,1:2]
 eqscplot(scores, type="n", xlab="first canonical discriminant",
     ylab="second canonical discriminant")
 text(scores, cutree(hc, 7))
 title("Single Linkage Cluster Analysis: Stanford Diabetes Data")




#  Compute K-means cluster analysis starting with the results from
#  an average linkage cluster analysis.  The centroids from seven
#  aveargae linkage clusters are stored as rows in the matrix  "initial"

 	hc<-hclust(dist(diabetes.sx),method="average") 
          initial<-tapply(diabetes.sx, list(rep(cutree(hc,7),
             ncol(diabetes.sx)), col(diabetes.sx)), mean)
        km<-kmeans(diabetes.sx, initial)
           a<-lda(diabetes.sx, km$cluster)  
           a$svd^2/sum(a$svd^2)
           a$scaling
           scores<-diabetes.sx %*% a$scaling[ ,1:2]
        eqscplot(scores, type="n", xlab="first canonical discriminant",
          ylab="second canonical discriminant")
          text(scores, km$cluster)
          title("K-means Cluster Analysis: Stanford Diabetes Data")        



#  Model based clustering based on the assumption that 
#  the data were sampled from a mixture of multivariate normal
#  distributions with common covariance matrix 

#  First do hierarchical clustering

     hm<-mclust(diabetes.sx, method="S*")
       plclust(clorder(hm$tree, cutree(hm$tree, 7)))
         title("Model Based Cluster Analysis: Stanford Diabetes Data")
           a<-lda(diabetes.sx, cutree(hm$tree, 7))  
           a$svd^2/sum(a$svd^2)
           a$scaling
           scores<-diabetes.sx %*% a$scaling[ ,1:2]
        eqscplot(scores, type="n", xlab="first canonical discriminant",
           ylab="second canonical discriminant")
           text(scores, cutree(hm$tree, 7))
           title("Model Based Cluster Analysis: Stanford Diabetes Data")

#  Optimize further using a 7 cluster solution for a K-means type
#  of reallocation using Mahalonobis distance based on a homogeneous
#  covariance matrix.  Using a scalar multiple of the identity matrix
#  for the covariance matrix would result in K-means clustering
 
     hm<-mclust(diabetes.sx, method="S*", noise=T)
     hclass<-mclass(hm, 7)
     hclass$class
     mreloc(hclass, diabetes.sx, method="S*", noise=T)
           a<-lda(diabetes.sx, hclass$class)  
           a$svd^2/sum(a$svd^2)
           a$scaling
           scores<-diabetes.sx %*% a$scaling[ ,1:2]
        eqscplot(scores, type="n", xlab="first canonical discriminant",
          ylab="second canonical discriminant")
          text(scores, hclass$class)
          title("Model Based Cluster Analysis: Stanford Diabetes Data")    




# This code creates scatter plot matrices and does
# factor analysis for 100 consecutive weeks of gains
# in prices for five stocks. This code is posted as
#
# stocks.R
#
# The data are posted as stocks.dat
#
# There is one line of data for each week and the
# weekly gains are represented as
# x1 = ALLIED CHEMICAL
# x2 = DUPONT
# x3 = UNION CARBIDE
# x4 = EXXON
# x5 = TEXACO
stocks <- read.table("http://streaming.stat.iastate.edu/~stat501/data/stocks.dat", header=F,col.names=c("x1","x2","x3","x4","x5"))
# Print the first six rows of the data file
stocks[1:6, ]
# Create a scatter plot matrix of the standardized data
stockss <- scale(stocks, center=T, scale=T)
pairs(stockss,labels=c("Allied","Dupont","Carbide", "Exxon","Texaco"), panel=function(x,y){panel.smooth(x,y) abline(lsfit(x,y),lty=2)})
# Compute principal components from the correlation matrix
s.cor <- var(stockss)
s.pc <- prcomp(stocks, scale=T, center=T)
# List component coefficients
# Estimate proportion of variation explained by each
# principal component,and cummulative proportions
s <- var(s.pc$x)
pvar<-round(diag(s)/sum(diag(s)), digits=6)
pvar
cpvar <- round(cumsum(diag(s))/sum(diag(s)), digits=6)
cpvar
# Plot component scores
par(fin=c(5,5))
plot(s.pc$x[,1],s.pc$x[,2],xlab="PC1: Overall Market",ylab="PC2: Oil vs. Chemical",type="p")
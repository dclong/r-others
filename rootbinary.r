#' This function uses bianry search to find a root.
#' @param f a monotone function.
#' @parma lower the lower limit of the interval. 
#' @param upper the upper limit of the interval. 
#' @param dx the threshold for independent variable for convergence.
#' @param dy the threshold for function value for convergence.
rootBinary = function(f,lower,upper,dx=1e-8,dy=dx){
  flower = f(lower)
  fupper = f(upper)
  if(flower>0&&fupper>0||flower<0&&fupper<0){
    stop('The monotone function f does not have a root in the specified interval.')
  }
  delta.y = fupper - flower
  if(delta.y>0){
    delta.x = abs(upper - lower)
    delta.y = abs(delta.y)
    while(!(delta.x<=dx&&delta.y<=dy)){
      middle = 0.5*(lower+upper)
      fmiddle = f(middle)
      if(fmiddle>0){
        upper = middle
        fupper = fmiddle
      }else if(fmiddle<0){
        lower = middle
        flower = fmiddle
      }else{
        return(middle)
      }
      delta.x = abs(upper-lower)
      delta.y = abs(fupper-flower)
    }
    return(0.5*(lower+upper))
  }
  g(x) = function(x){-f(x)}
  rootBinary(f=g,lower=lower,upper=upper,dx=dx,dy=dy)
}



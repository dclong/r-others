
#' Linear Algebra
#' Get row and column indeces of elements in a matrix in descending or ascending order.
#' @param x a vector or matrix.
#' @param na.last for controlling the treatment of NAs. If TRUE, missing values in the data are put last; if FALSE, they are put first; if NA, they are removed.
#' @param desc logical. If TRUE the sort order is decreasing, o.w. it's increasing.
#' @return a matrix of two columns, where the first column contains the row indeces and the second column contains the column indeces.
#' @author Chuanlong Benjamin Du.
#' @seealso \code{\link[base]{order}}.
#' @keywords element matrix index
#' @examples
#'
#' #generate a matrix
#' A = matrix(rnorm(12),nrow=3)
#' #get the indeces of ordered elements
#' orderedElementIndex(A)
#'
orderedElementIndex <-
function(x,na.last=TRUE,desc=FALSE)
{
  x=as.matrix(x)
  rownum=nrow(x)
  indices=order(x,na.last=na.last,decreasing=desc)
  row.indices=indices%%rownum
  row.indices=ifelse(row.indices==0,rownum,row.indices)
  col.indices=ceiling(indices/rownum)
  cbind(row.indices,col.indices)
}

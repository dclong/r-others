#' Directory Manipulation
#' Swap elements in a given vector.
#' @alias vector.swap
#' @param index1 an integer vector.
#' @param index2 another integer vector with the same length as index1.
#' @author Chuanlong Benjamin Du
#' @seealso \code{\link{vector.move}}.
#' @keywords vector swap switch
#' @examples
#' x=c(10,2,3)
#' swap(x,1,3)
#'
vector.swap=function(x,index1,index2){
    temp = x[index1]
  x[index1] = x[index2]
  x[index2] = temp
}
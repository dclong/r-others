
#' Matrices
#' returns a matrix from rotating a vector. %% ~~ A concise (1-5 lines)
#' description of what the function does. ~~
#'
#' @param x a vector used to generate the matrix.
#' @param direction a string specifying which direction to rotate the vector.
#'   %% ~~Describe \code{direction} here~~
#' @param nrow number of rows of the matrix to be returned.
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords rotate matrix
#' @examples
#'
#' #generate a rotate matrix
#'
shiftMatrix <-
function(x=numeric(0),direction='left',nrow=NULL)
{
  len=length(x)
  if(len==0)
    stop('x is not specified.')
  if(len==1)
  {
    if(x>1)
      x=1:x
    else
      if(x<-1)
        x=-1:x
      else
        stop('x require a vector or a number with absolute value greater than 1.')
  }
  if(is.null(nrow))
    nrow=length(x)
  else
    if(nrow==1)
      return(x)
  result=x
  for(i in 2:nrow)
  {
    x=shift(x,1,direction)
    result=rbind(result,x)
  }
  rownames(result)=NULL
  return(result)
}


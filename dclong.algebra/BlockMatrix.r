
#' Linear Algebra
#' Construct a block diagonal matrix from given matrices.
#' @param \dots vectors or matrices.
#' @param list a list of vector or matrices.
#' @param p.tr padding values for the top right block of the block diagonal matrix.
#' @param p.bl paddding values for the bottom left block of the block diagonal matrix.
#' @param p.tl padding values for the top left block for the block anti-diagonal matrix.
#' @param p.br padding values for the bottom right block for the block anti-diagonal matrix.
#' @return a block diagonal or anti-diagonal matrix with the given matrices as diagonal or anti-diagonal block elements.
#' @author Chuanlong Benjamin Du
#' @keywords block diagonal anti-diagonal
#' @examples
#'
#' #generate matrices
#' A=matrix(1,nrow=2,ncol=3)
#' B=matrix(2,nrow=3,ncol=4)
#' C=matrix(3,nrow=4,ncol=5)
#' blockDiag(A,B,C)
#' blockAntiDiag(A,B,C)
#'
blockAntiDiag <-
function(...,list=numeric(0L),p.tl=0,p.br=0){
  dots = list(...)
  if(length(dots) && !all(sapply(dots,function(x) is.matrix(x)||is.vector(x))))
    stop("... must be matrices or vectors.")
  dots=lapply(dots,as.matrix)
  if(length(list) && !all(sapply(list,function(x) is.matrix(x)||is.vector(x))))
    stop("argument list must be matrices or vectors.")
  list=lapply(list,as.matrix)
  matrices=c(dots,list)
  n=length(matrices)
  if(!n)
    stop("at least one matrix is required.")
  if(n==1)
    return(matrices[[1]])
  dimension=sapply(matrices,dim)
  TotRowNum=sum(dimension[1,])
  TotColNum=sum(dimension[2,])
  result=matrix(p.br,nrow=TotRowNum,ncol=TotColNum)
  RowStart=1
  ColEnd=TotColNum
  for(i in 1:(n-1))
  {
    RowEnd = RowStart + dimension[1,i]
    ColStart = ColEnd - dimension[2,i]
    result[RowStart:(RowEnd-1),(ColStart+1):ColEnd] = matrices[[i]]
    result[RowStart:(RowEnd-1),1:ColStart]=p.tl
    RowStart = RowEnd
    ColEnd = ColStart
  }
  result[RowStart:TotRowNum,1:ColEnd]=matrices[[n]]
  result
}

blockDiag <-
function(...,list=numeric(0L),p.tr=0,p.bl=0)
{
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  dots=list(...)
  if(length(dots) && !all(sapply(dots,function(x) is.matrix(x)||is.vector(x))))
    stop("... must be matrices or vectors.")
  dots=lapply(dots,as.matrix)
  if(length(list) && !all(sapply(list,function(x) is.matrix(x)||is.vector(x))))
    stop("argument list must be matrices or vectors.")
  list=lapply(list,as.matrix)
  matrices=c(dots,list)
  n=length(matrices)
  if(!n)
    stop("at least one matrix is required.")
  if(n==1)
    return(matrices[[1]])
  dimension=sapply(matrices,dim)
  TotRowNum=sum(dimension[1,])
  TotColNum=sum(dimension[2,])
  result=matrix(p.bl,nrow=TotRowNum,ncol=TotColNum)
  RowStart=1
  ColStart=1
  for(i in 1:(n-1))
  {
    RowEnd=RowStart+dimension[1,i]
    ColEnd=ColStart+dimension[2,i]
    result[RowStart:(RowEnd-1),ColStart:(ColEnd-1)]=matrices[[i]]
    result[RowStart:(RowEnd-1),ColEnd:TotColNum]=p.tr
    RowStart=RowEnd
    ColStart=ColEnd
  }
  result[RowStart:TotRowNum,ColStart:TotColNum]=matrices[[n]]
  result
}



#' Linear Algebra
#' Product of matrices.
#' @param \dots matrices that are compatible %% ~~Describe \code{\dots} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords matrix product
#' @examples
#'
#' #matrix product
#' matprod(matrix(rnorm(6),nrow=2),matrix(rnorm(9),nrow=3),matrix(rnorm(12),nrow=3))
#'
matprod <-
function(...)
{#This function calculate the matrix product of given matrices
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  mats=list(...)
  len=length(mats)
  if(len<1)
    stop("at least one matrix is required!")
  if(len<2)
    return(mats[[1]])
  return(mats[[1]]%*%do.call(matprod,mats[-1]))
}

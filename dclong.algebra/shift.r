
#' Vectors
#' shift a vector left or right. %% ~~ A concise (1-5 lines) description of
#' what the function does. ~~
#'
#' %% ~~ If necessary, more details than the description above ~~
#'
#' @param x %% ~~Describe \code{x} here~~
#' @param n %% ~~Describe \code{n} here~~
#' @param direction %% ~~Describe \code{direction} here~~
#' @return %% ~Describe the value returned %% If it is a LIST, use %%
#'   \item{comp1 }{Description of 'comp1'} %% \item{comp2 }{Description of
#'   'comp2'} %% ...
#' @note %% ~~further notes~~
#' @author Chuanlong Benjamin Du %% ~~who you are~~
#' @seealso %% ~~objects to See Also as \code{\link{help}}, ~~~
#' @references %% ~put references to the literature/web site here ~
#' @keywords shift vector
#' @examples
#'
#' #rotate a vector left
#' shift(1:4)
#'
shiftVector <-
function(x,n=1,direction=c("left","right")[1])
{#This function rotate a given vector left or right by n
#Parameter x is the vector to be rotated
#Parameter direction specifies the direction to the vector to be moved
#    "left" means move the vector to left, which is the default value
#    "right" means move the vector to right
#Parameter n is the length the vector to be moved, which has default value 1
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  if(is.character(direction))
    direction=tolower(trim(direction))
  xlen=length(x)
  if(xlen==1)
    return(x)
  if(direction=="left" || direction=='l' || direction<0)
  {
    for(i in 1:n)
    {
      temp=x[1]
      for(j in 1:(xlen-1))
         x[j]=x[j+1]
      x[xlen]=temp
    }
    return(x)
  }
  if(direction=="right" || direction=='r' || direction>0)
  {
    for(i in 1:n)
    {
      temp=x[xlen]
      for(j in xlen:2)
        x[j]=x[j-1]
      x[1]=temp
    }
    return(x)
  }
  stop("unrecognized  direction.")
}


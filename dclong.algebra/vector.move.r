vector.move=function(x,index,up,down=-up){
  #index cannot be a vector
 # #calculate new index
#    newIndex=index+down
#    if(newIndex<1)
#      newIndex=1
#    else
#      if(newIndex>n.dpath)
#        newIndex=n.dpath
#    #move the specified path
#    temp=dpath[index]
#    dpath[index]=dpath[newIndex]
#    dpath[newIndex]=temp
  if(down>0){
    newIndex = index + down
    xLength = length(x)
    if(newIndex>xLength){
      newIndex = xLength
    }
    temp = x[index]
    x[index:(newIndex-1)] = x[(index+1):newIndex]
    x[newIndex] = temp
    return(x)
  }
  if(down<0){
    newIndex = index + down
    if(newIndex<1){
      newIndex = 1
    }
    temp = x[index]
    x[(newIndex+1):index] = x[newIndex:(index-1)]
    x[newIndex] = temp
    return(x)
  }
  return(x)
}
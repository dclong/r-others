
#' Linear Algebra
#' Power of a square matrix.
#' @param x a square matrix.
#' @param power numerical. The power of the matrix.
#' @param method the method used to calculate the matrix power. "diag" stands for method of diagonalization; "bf" stands for the brute force method.
#' @return the power of the given square matrix.
#' @author Chuanlong Benjamin Du.
#' @seealso \code{\link[base]{exp}}.
#' @keywords matrix power
#' @examples
#'
#' #create a nonnegative definite symmetric matrix
#' A = matrix(rnorm(9,nrow=3))
#' A = A %*% A'
#' #calculate the power of A to 0.5
#' matrixPower(A,0.5)
#'
matrixPower <-
function(x,power,method=c("diag","bf")[1])
{
#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  method=tolower(method)
  if(method=="diag")
  {
    xeigen=eigen(x)
    x.values=xeigen$values
    x.vectors=xeigen$vectors
    return(x.vectors%*%diag(x.values^power)%*%solve(x.vectors))
  }
  if(as.integer(power)!=power||power<0)
    stop("the power must be a nonnegative integer.")
  if(power>1)
  {
    result=x
    for(i in 1:(power-1))
      result=result%*%x
    return(result)
  }
  if(nrow(x)!=ncol(x))
    stop("the matrix must a square matrix.")
  if(power==0)
    return(diag(nrow(x)))
  return(x)
}

